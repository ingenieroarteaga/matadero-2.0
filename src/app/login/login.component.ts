import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { AuthService } from '../services/auth/auth.service';
import { TokenService } from '../services/token/token.service';
import { UserService } from '../@core/data/users.service';
import { Usuario} from '../@theme/components/header/usuario';
// para mostrar error
import { InvalidFormComponent } from '../pages/personas/matarifes/agregar-matarife/invalid-form/invalid-form.component';
@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthService, TokenService],
})

export class LoginComponent {

  username: string;
  password: string;
  public userInfo: Usuario = new Usuario();
  public users: any[];

  constructor(private router: Router, private authService: AuthService, private dialogService: NbDialogService,
              private token: TokenService, private userService: UserService) {

                const el = document.getElementById('nb-global-spinner');
                  if (el) {
                    el.style['display'] = 'none';
                }
  }

  ingresar(): void {
    this.authService.attemptAuth(this.username, this.password).subscribe(
      data => {
        this.token.saveToken(data.token);
        console.log('apreto el boton');
        this.userService.getUsuarioByUsername(this.username);
      },
      error => {
        this.dialogService.open( InvalidFormComponent, {
          context: {
            title: 'Usuario o Contraseña incorrectos',
          },
          closeOnEsc: false,
          closeOnBackdropClick: true,
        });
        this.username = null;
        this.password = null;
      },
    );
  }
}
// 1- AGREGAR ERRROR CUANDO USER O PASS ESTÁ MAL
