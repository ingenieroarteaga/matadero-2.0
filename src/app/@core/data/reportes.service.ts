import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';



import { Global } from '../../../app/global';

@Injectable()
export class ReportesService {

  path: string = Global.urlBase;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(public http: HttpClient) {

   }

   imprimirTropa(ingresoId: number) {
     const httpOptions = {
       'responseType': 'arraybuffer' as 'json',
     };
     return this.http.post<any>(this.path + '/imprime-romaneos-ingreso', ingresoId, httpOptions);
   }

}
