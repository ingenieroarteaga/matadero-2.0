import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
// CLASIFICADORES
import {Pais} from '../../../app/pages/clasificadores/paises/pais';
import {Provincia} from '../../../app/pages/clasificadores/provincias/provincia';
import {Localidad} from '../../../app/pages/clasificadores/localidades/localidad';
import {Distrito} from '../../../app/pages/clasificadores/distritos/distrito';
import {TipoDomicilio} from '../../../app/pages/clasificadores/tipos-domicilio/tipo-domicilio';
import {TipoDocumento} from '../../../app/pages/clasificadores/tipos-documento/tipo-documento';
import {TipoPersona} from '../../../app/pages/clasificadores/tipos-persona/tipo-persona';
import {TipoAnimal} from '../../../app/pages/clasificadores/tipos-animales/tipo-animal';
import {SubTipoAnimal} from '../../../app/pages/clasificadores/subtipos-animales/subtipo-animal';
import {TipoDestino} from '../../../app/pages/clasificadores/tipos-destinos/tipo-destino';
import {TipoProcedencia} from '../../../app/pages/clasificadores/tipos-procedencias/tipo-procedencia';
import {TipoCamara} from '../../../app/pages/clasificadores/tipos-camaras/tipo-camara';
import {NumeroSenasa} from '../../../app/pages/clasificadores/numeros-senasa/numero-senasa';
// HERRRAMIENTAS
import {Corral} from '../../../app/pages/herramientas/corrales/corral';
import {Camara} from '../../../app/pages/herramientas/camaras/camara';
import {Transporte} from '../../../app/pages/herramientas/transportes/transporte';
// PERSONAS
import {Productor} from '../../../app/pages/personas/productores/productor';
import {Matarife} from '../../../app/pages/personas/matarifes/matarife';
import {Transportista} from '../../../app/pages/personas/transportistas/transportista';
import {Administrativo} from '../../../app/pages/personas/administrativos/administrativo';
import {Operario} from '../../../app/pages/personas/operarios/operario';
import {Corralero} from '../../../app/pages/personas/corraleros/corralero';
// OPERACIONES
import {RangoTropa} from '../../../app/pages/operaciones/rangos-tropas/rango-tropa';
import {Ingreso} from '../../../app/pages/operaciones/ingresos/ingreso';
import {Tropa} from '../../../app/pages/operaciones/ingresos/tropa';
import {TropaDetalle} from '../../../app/pages/operaciones/ingresos/tropa-detalle';
import {ListaMatanza} from '../../../app/pages/operaciones/listas-matanza/lista-matanza';
import {Faena} from '../../../app/pages/operaciones/romaneos/faena';
import {Romaneo} from '../../../app/pages/operaciones/romaneos/romaneo';
import {MovimientoCamara} from '../../../app/pages/operaciones/movimientos-camara/movimiento-camara';
import {Venta} from '../../../app/pages/operaciones/movimientos-camara/venta';

import { Global } from '../../../app/global';

@Injectable()
export class SmartTableService {

  paises: any = [];
  source: any = [];
  path: string = Global.urlBase;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(public http: HttpClient) {

   }

  // Servicios para PAIS

  getPaises() {
    return this.http.get<Pais[]>(this.path + '/paises');
  }

  createPais(pais: Pais): Observable<Pais> {
    return this.http.post<Pais>(this.path + '/pais', pais, { headers: this.httpHeaders });
  }

  updatePais(pais: Pais): Observable<Pais> {
    return this.http.put<Pais>(this.path + '/pais', pais, { headers: this.httpHeaders });
  }

  deletePais(id: number): Observable<Pais> {
    return this.http.delete<Pais>(`${this.path + '/pais'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para PROVINCIA

  getProvincias() {
    return this.http.get<Provincia[]>(this.path + '/provincias');
  }

  createProvincia(provincia: Provincia): Observable<Provincia> {
    return this.http.post<Provincia>(this.path + '/provincia', provincia, { headers: this.httpHeaders });
  }

  updateProvincia(provincia: Provincia): Observable<Provincia> {
    return this.http.put<Provincia>(this.path + '/provincia', provincia, { headers: this.httpHeaders });
  }

  deleteProvincia(id: number): Observable<Provincia> {
    return this.http.delete<Provincia>(`${this.path + '/provincia'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para LOCALIDADES

  getLocalidades() {
    return this.http.get<Localidad[]>(this.path + '/localidades');
  }

  createLocalidad(localidad: Localidad): Observable<Localidad> {
    return this.http.post<Localidad>(this.path + '/localidad', localidad, { headers: this.httpHeaders });
  }

  updateLocalidad(localidad: Localidad): Observable<Localidad> {
    return this.http.put<Localidad>(this.path + '/localidad', localidad, { headers: this.httpHeaders });
  }

  deleteLocalidad(id: number): Observable<Localidad> {
    return this.http.delete<Localidad>(`${this.path + '/localidad'}/${id}`, { headers: this.httpHeaders });
  }
  // Servicios para Distritos

  getDistritos() {
    return this.http.get<Distrito[]>(this.path + '/distritos');
  }

  createDistrito(localidad: Distrito): Observable<Distrito> {
    return this.http.post<Distrito>(this.path + '/distrito', localidad, { headers: this.httpHeaders });
  }

  updateDistrito(localidad: Distrito): Observable<Distrito> {
    return this.http.put<Distrito>(this.path + '/distrito', localidad, { headers: this.httpHeaders });
  }

  deleteDistrito(id: number): Observable<Distrito> {
    return this.http.delete<Distrito>(`${this.path + '/distrito'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO DE DOMICILIO

  getTipoDomicilios() {
    return this.http.get<TipoDomicilio[]>(this.path + '/tipos-domicilios');
  }

  createTipoDomicilio(tipoDomicilio: TipoDomicilio): Observable<TipoDomicilio> {
    return this.http.post<TipoDomicilio>(this.path + '/tipo-domicilio', tipoDomicilio, { headers: this.httpHeaders });
  }

  updateTipoDomicilio(tipoDomicilio: TipoDomicilio): Observable<TipoDomicilio> {
    return this.http.put<TipoDomicilio>(this.path + '/tipo-domicilio', tipoDomicilio, { headers: this.httpHeaders });
  }

  deleteTipoDomicilio(id: number): Observable<TipoDomicilio> {
    return this.http.delete<TipoDomicilio>(`${this.path + '/tipo-domicilio'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO DOCUMENTO

  getTipoDocumentos() {
    return this.http.get<TipoDocumento[]>(this.path + '/tipos-documentos');
  }

  createTipoDocumento(documento: TipoDocumento): Observable<TipoDocumento> {
    return this.http.post<TipoDocumento>(this.path + '/tipo-documento', documento, { headers: this.httpHeaders });
  }

  updateTipoDocumento(documento: TipoDocumento): Observable<TipoDocumento> {
    return this.http.put<TipoDocumento>(this.path + '/tipo-documento', documento, { headers: this.httpHeaders });
  }

  deleteTipoDocumento(id: number): Observable<TipoDocumento> {
    return this.http.delete<TipoDocumento>(`${this.path + '/tipo-documento'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO PERSONAS

  getTipoPersonas() {
    return this.http.get<TipoPersona[]>(this.path + '/tipos-personas');
  }

  createTipoPersona(persona: TipoPersona): Observable<TipoPersona> {
    return this.http.post<TipoPersona>(this.path + '/tipo-persona', persona, { headers: this.httpHeaders });
  }

  updateTipoPersona(persona: TipoPersona): Observable<TipoPersona> {
    return this.http.put<TipoPersona>(this.path + '/tipo-persona', persona, { headers: this.httpHeaders });
  }

  deleteTipoPersona(id: number): Observable<TipoPersona> {
    return this.http.delete<TipoPersona>(`${this.path + '/tipo-persona'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO ANIMAL

  getTiposAnimales() {
    return this.http.get<TipoAnimal[]>(this.path + '/tipos-animales');
  }

  createTipoAnimal(tipoAnimal: TipoAnimal): Observable<TipoAnimal> {
    return this.http.post<TipoAnimal>(this.path + '/tipo-animal', tipoAnimal, { headers: this.httpHeaders });
  }

  updateTipoAnimal(tipoAnimal: TipoAnimal): Observable<TipoAnimal> {
    return this.http.put<TipoAnimal>(this.path + '/tipo-animal', tipoAnimal, { headers: this.httpHeaders });
  }

  deleteTipoAnimal(id: number): Observable<TipoAnimal> {
    return this.http.delete<TipoAnimal>(`${this.path + '/tipo-animal'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para SUB TIPO ANIMAL

  getSubTiposAnimales() {
    return this.http.get<SubTipoAnimal[]>(this.path + '/subtipos-animales');
  }

  createSubTipoAnimal(subTipoAnimal: SubTipoAnimal): Observable<SubTipoAnimal> {
    return this.http.post<SubTipoAnimal>(this.path + '/subtipo-animal', subTipoAnimal, { headers: this.httpHeaders });
  }

  updateSubTipoAnimal(subTipoAnimal: SubTipoAnimal): Observable<SubTipoAnimal> {
    return this.http.put<SubTipoAnimal>(this.path + '/subtipo-animal', subTipoAnimal, { headers: this.httpHeaders });
  }

  deleteSubTipoAnimal(id: number): Observable<SubTipoAnimal> {
    return this.http.delete<SubTipoAnimal>(`${this.path + '/subtipo-animal'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO DESTINO

  getTiposDestinos() {
    return this.http.get<TipoDestino[]>(this.path + '/destinos');
  }

  createTipoDestino(tipoDestino: TipoDestino): Observable<TipoDestino> {
    return this.http.post<TipoDestino>(this.path + '/destino', tipoDestino, { headers: this.httpHeaders });
  }

  updateTipoDestino(tipoDestino: TipoDestino): Observable<TipoDestino> {
    return this.http.put<TipoDestino>(this.path + '/destino', tipoDestino, { headers: this.httpHeaders });
  }

  deleteTipoDestino(id: number): Observable<TipoDestino> {
    return this.http.delete<TipoDestino>(`${this.path + '/destino'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO PROCEDENCIA

  getTiposProcedencias() {
    return this.http.get<TipoProcedencia[]>(this.path + '/procedencias');
  }

  createTipoProcedencia(procedencias: TipoProcedencia): Observable<TipoProcedencia> {
    return this.http.post<TipoProcedencia>(this.path + '/procedencias', procedencias, { headers: this.httpHeaders });
  }

  updateTipoProcedencia(procedencias: TipoProcedencia): Observable<TipoProcedencia> {
    return this.http.put<TipoProcedencia>(this.path + '/procedencias', procedencias, { headers: this.httpHeaders });
  }

  deleteTipoProcedencia(id: number): Observable<TipoProcedencia> {
    return this.http.delete<TipoProcedencia>(`${this.path + '/procedencias'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO CAMARAS

  getTiposCamaras() {
    return this.http.get<TipoCamara[]>(this.path + '/tipos-camaras');
  }

  createTipoCamara(localidad: TipoCamara): Observable<TipoCamara> {
    return this.http.post<TipoCamara>(this.path + '/tipo-camara', localidad, { headers: this.httpHeaders });
  }

  updateTipoCamara(localidad: TipoCamara): Observable<TipoCamara> {
    return this.http.put<TipoCamara>(this.path + '/tipo-camara', localidad, { headers: this.httpHeaders });
  }

  deleteTipoCamara(id: number): Observable<TipoCamara> {
    return this.http.delete<TipoCamara>(`${this.path + '/tipo-camara'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para NUMEROS SENASA

  getNumerosSenasa() {
    return this.http.get<NumeroSenasa[]>(this.path + '/senasas');
  }

  createNumeroSenasa(numero: NumeroSenasa): Observable<NumeroSenasa> {
    return this.http.post<NumeroSenasa>(this.path + '/senasa', numero, { headers: this.httpHeaders });
  }

  updateNumeroSenasa(numero: NumeroSenasa): Observable<NumeroSenasa> {
    return this.http.put<NumeroSenasa>(this.path + '/senasa', numero, { headers: this.httpHeaders });
  }

  deleteNumeroSenasa(id: number): Observable<NumeroSenasa> {
    return this.http.delete<NumeroSenasa>(`${this.path + '/senasa'}/${id}`, { headers: this.httpHeaders });
  }

  // OPERACIONES
  // Servicios para RANGO TROPAS

  getRangosTropas() {
    return this.http.get<RangoTropa[]>(this.path + '/rangos-tropas');
  }

  createRangoTropa(rango: RangoTropa): Observable<RangoTropa> {
    return this.http.post<RangoTropa>(this.path + '/rango-tropa', rango, { headers: this.httpHeaders });
  }

  updateRangoTropa(rango: RangoTropa): Observable<RangoTropa> {
    return this.http.put<RangoTropa>(this.path + '/rango-tropa', rango, { headers: this.httpHeaders });
  }

  deleteRangoTropa(id: number): Observable<RangoTropa> {
    return this.http.delete<RangoTropa>(`${this.path + '/rango-tropa'}/${id}`, { headers: this.httpHeaders });
  }
  getNumeroRangoTropa(rango: RangoTropa): Observable<number> {
    return this.http.post<number>(this.path + '/rango-tropa-numero', rango, { headers: this.httpHeaders });
  }

  // Servicios para INGRESOS

  getIngresos() {
    return this.http.get<Ingreso[]>(this.path + '/ingresos');
  }
  getIngresosPaginado(search: any, page: number, size: number, order: string, asc: boolean): Observable<any> {
    return this.http.post<any>(this.path + '/ingresos-search-page?' +
      `page=${page}&size=${size}&order=${order}&asc=${asc}`, search, { headers: this.httpHeaders });
  }
  getIngresosSinMatanza() {
    return this.http.get<Ingreso[]>(this.path + '/ingresos-sin-matanza');
  }
  getIngresosByFaena(faenaId: number) {
    return this.http.get<Ingreso[]>(`${this.path + '/ingresos-disp-faena'}/${faenaId}`);
  }
  getIngresosEnCamara(camaraId: number) {
    return this.http.get<Ingreso[]>(`${this.path + '/ingresos-disp-camara'}/${camaraId}`);
  }
  createIngreso(ingreso: Ingreso): Observable<Ingreso> {
    return this.http.post<Ingreso>(this.path + '/ingreso', ingreso, { headers: this.httpHeaders });
  }

  updateIngreso(ingreso: Ingreso): Observable<Ingreso> {
    return this.http.put<Ingreso>(this.path + '/ingreso', ingreso, { headers: this.httpHeaders });
  }

  deleteIngreso(id: number): Observable<Ingreso> {
    return this.http.delete<Ingreso>(`${this.path + '/ingreso'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TROPAS

  getTropas() {
    return this.http.get<Tropa[]>(this.path + '/tropas');
  }

  createTropa(tropa: Tropa): Observable<Tropa> {
    return this.http.post<Tropa>(this.path + '/tropa', tropa, { headers: this.httpHeaders });
  }

  updateTropa(tropa: Tropa): Observable<Tropa> {
    return this.http.put<Tropa>(this.path + '/tropa', tropa, { headers: this.httpHeaders });
  }

  deleteTropa(id: number): Observable<Tropa> {
    return this.http.delete<Tropa>(`${this.path + '/tropa'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicio para TropaDetalle solo tiene update
  getTropasDetalles() {
    return this.http.get<TropaDetalle[]>(this.path + '/tropas-detalles');
  }
  getTropasDetallesByTropa(tropadId: number) {
    return this.http.get<TropaDetalle[]>(`${this.path + '/tropas-detalles-tropa'}/${tropadId}`);
  }
  createTropaDetalle(tropaDetalle: TropaDetalle): Observable<TropaDetalle> {
    return this.http.post<TropaDetalle>(this.path + '/tropa-detalle', tropaDetalle, { headers: this.httpHeaders });
  }

  updateTropaDetalle(tropaDetalle: TropaDetalle): Observable<TropaDetalle> {
    return this.http.put<TropaDetalle>(this.path + '/tropa-detalle', tropaDetalle, { headers: this.httpHeaders });
  }

  // Servicios para Listas Matanza

  getMatanzas() {
    return this.http.get<ListaMatanza[]>(this.path + '/matanzas');
  }
  getMatanzasPaginado(search: any, page: number, size: number, order: string, asc: boolean): Observable<any> {
    return this.http.post<any>(this.path + '/matanzas-search-page?' +
      `page=${page}&size=${size}&order=${order}&asc=${asc}`, search, { headers: this.httpHeaders });
  }

  createMatanza(matanza: ListaMatanza): Observable<ListaMatanza> {
    return this.http.post<ListaMatanza>(this.path + '/matanza', matanza, { headers: this.httpHeaders });
  }

  updateMatanza(matanza: ListaMatanza): Observable<ListaMatanza> {
    return this.http.put<ListaMatanza>(this.path + '/matanza', matanza, { headers: this.httpHeaders });
  }

  deleteMatanza(id: number): Observable<ListaMatanza> {
    return this.http.delete<ListaMatanza>(`${this.path + '/matanza'}/${id}`, { headers: this.httpHeaders });
  }

// Servicios para Romaneos
  getRomaneos() {
    return this.http.get<Romaneo[]>(this.path + '/romaneos');
  }
  getRomaneosByIngreso(ingresoId: number) {
    return this.http.get<Romaneo[]>(`${this.path + '/romaneos-ingreso'}/${ingresoId}`);
  }
  getRomaneosByIngresoCount(ingresoId: number) {
    return this.http.get<number>(`${this.path + '/romaneos-ingreso-cant'}/${ingresoId}`);
  }
  getRomaneosByFaena(faenaId: number) {
    return this.http.get<Romaneo[]>(`${this.path + '/romaneos-faena'}/${faenaId}`);
  }
  getRomaneosDisponibles() {
    return this.http.get<Romaneo[]>(this.path + '/romaneos-disponibles');
  }
  getRomaneosIngresoPesoByIngreso(ingresoId: number) {
    return this.http.get<number>(`${this.path + '/romaneos-ingreso-peso'}/${ingresoId}`);
  }
  getRomaneosIngresoCantByIngreso(ingresoId: number) {
    return this.http.get<number>(`${this.path + '/romaneos-ingreso-cant'}/${ingresoId}`);
  }
  createRomaneo(romaneo: Romaneo): Observable<Romaneo> {
    return this.http.post<Romaneo>(this.path + '/romaneo', romaneo, { headers: this.httpHeaders });
  }
  // creo romaneos por lista
  createRomaneoList(romaneo: Romaneo[]): Observable<Romaneo[]> {
    return this.http.post<Romaneo[]>(this.path + '/romaneo-varios', romaneo, { headers: this.httpHeaders });
  }
  updateRomaneo(romaneo: Romaneo): Observable<Romaneo> {
    return this.http.put<Romaneo>(this.path + '/romaneo', romaneo, { headers: this.httpHeaders });
  }

  deleteRomaneo(id: number): Observable<Romaneo> {
    return this.http.delete<Romaneo>(`${this.path + '/romaneo'}/${id}`, { headers: this.httpHeaders });
  }


// Servicios para FAENAa
  getFaenas() {
    return this.http.get<Faena[]>(this.path + '/faenas');
  }
  getFaenasPaginado(search: any, page: number, size: number, order: string, asc: boolean): Observable<any> {
    return this.http.post<any>(this.path + '/faenas-search-page?' +
      `page=${page}&size=${size}&order=${order}&asc=${asc}`, search, { headers: this.httpHeaders });
  }
  // para listar romaneos de la faena de forma paginagada
  getRomaneosFaenasPaginado(ingreso: number, page: number, size: number, order: string, asc: boolean): Observable<any> {
    return this.http.post<any>(this.path + '/romaneos-ingreso-page?' +
      `page=${page}&size=${size}&order=${order}&asc=${asc}`, ingreso, { headers: this.httpHeaders });
  }
  createFaena(faena: Faena): Observable<Faena> {
    return this.http.post<Faena>(this.path + '/faena', faena, { headers: this.httpHeaders });
  }

  updateFaena(faena: Faena): Observable<Faena> {
    return this.http.put<Faena>(this.path + '/faena', faena, { headers: this.httpHeaders });
  }

  deleteFaena(id: number): Observable<Faena> {
    return this.http.delete<Faena>(`${this.path + '/faena'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para Movimiento de camara
    getMovimientosCamaras() {
      return this.http.get<MovimientoCamara[]>(this.path + '/movimientos-camaras');
    }

    createMovimientoCamara(movimiento: MovimientoCamara): Observable<MovimientoCamara> {
      return this.http.post<MovimientoCamara>(this.path + '/movimiento-camara', movimiento,
      { headers: this.httpHeaders });
    }

    updateMovimientoCamara(movimiento: MovimientoCamara): Observable<MovimientoCamara> {
      return this.http.put<MovimientoCamara>(this.path + '/movimiento-camara', movimiento,
      { headers: this.httpHeaders });
    }

    deleteMovimientoCamara(id: number): Observable<MovimientoCamara> {
     return this.http.delete<MovimientoCamara>(`${this.path + '/movimiento-camara'}/${id}`,
     { headers: this.httpHeaders });
    }


  // PERSONAS

  // Servicios para PRODUCTORES

  getProductores() {
    return this.http.get<Productor[]>(this.path + '/productores');
  }
  getProductoresPaginado(p_search: string): Observable<any> {
    return this.http.post<any>(this.path + '/productores-lista', p_search, { headers: this.httpHeaders });
  }

  createProductor(productor: Productor): Observable<Productor> {
    return this.http.post<Productor>(this.path + '/productor', productor, { headers: this.httpHeaders });
  }

  updateProductor(productor: Productor): Observable<Productor> {
    return this.http.put<Productor>(this.path + '/productor', productor, { headers: this.httpHeaders });
  }

  deleteProductor(id: number): Observable<Productor> {
    return this.http.delete<Productor>(`${this.path + '/productor'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para MATARIFES

  getMatarifes() {
    return this.http.get<Matarife[]>(this.path + '/clientes');
  }

  createMatarife(matarife: Matarife): Observable<Matarife> {
    return this.http.post<Matarife>(this.path + '/cliente', matarife, { headers: this.httpHeaders });
  }

  updateMatarife(matarife: Matarife): Observable<Matarife> {
    return this.http.put<Matarife>(this.path + '/cliente', matarife, { headers: this.httpHeaders });
  }

  deleteMatarife(id: number): Observable<Matarife> {
    return this.http.delete<Matarife>(`${this.path + '/cliente'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TRANPOSRTISTAS

  getTransportistas() {
    return this.http.get<Transportista[]>(this.path + '/choferes');
  }

  createTransportista(chofer: Transportista): Observable<Transportista> {
    return this.http.post<Transportista>(this.path + '/chofer', chofer, { headers: this.httpHeaders });
  }

  updateTransportista(chofer: Transportista): Observable<Transportista> {
    return this.http.put<Transportista>(this.path + '/chofer', chofer, { headers: this.httpHeaders });
  }

  deleteTransportista(id: number): Observable<Transportista> {
    return this.http.delete<Transportista>(`${this.path + '/chofer'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para Adminisrtativos

  getAdministrativos() {
    return this.http.get<Administrativo[]>(this.path + '/administrativos');
  }

  createAdministrativo(administrativo: Administrativo): Observable<Administrativo> {
    return this.http.post<Administrativo>(this.path + '/administrativo', administrativo, { headers: this.httpHeaders });
  }

  updateAdministrativo(administrativo: Administrativo): Observable<Administrativo> {
    return this.http.put<Administrativo>(this.path + '/administrativo', administrativo, { headers: this.httpHeaders });
  }

  deleteAdministrativo(id: number): Observable<Administrativo> {
    return this.http.delete<Administrativo>(`${this.path + '/administrativo'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para Operarios

  getOperarios() {
    return this.http.get<Operario[]>(this.path + '/operarios');
  }

  createOperario(operario: Operario): Observable<Operario> {
    return this.http.post<Operario>(this.path + '/operario', operario, { headers: this.httpHeaders });
  }

  updateOperario(operario: Operario): Observable<Operario> {
    return this.http.put<Operario>(this.path + '/operario', operario, { headers: this.httpHeaders });
  }

  deleteOperario(id: number): Observable<Operario> {
    return this.http.delete<Operario>(`${this.path + '/operario'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para Corraleros

  getCorraleros() {
    return this.http.get<Corralero[]>(this.path + '/corraleros');
  }

  createCorralero(corralero: Corralero): Observable<Corralero> {
    return this.http.post<Corralero>(this.path + '/corralero', corralero, { headers: this.httpHeaders });
  }

  updateCorralero(corralero: Corralero): Observable<Corralero> {
    return this.http.put<Corralero>(this.path + '/corralero', corralero, { headers: this.httpHeaders });
  }

  deleteCorralero(id: number): Observable<Corralero> {
    return this.http.delete<Corralero>(`${this.path + '/corralero'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para Corrales

  getCorrales() {
    return this.http.get<Corral[]>(this.path + '/corrales');
  }

  createCorral(corral: Corral): Observable<Corral> {
    return this.http.post<Corral>(this.path + '/corral', corral, { headers: this.httpHeaders });
  }

  updateCorral(corral: Corral): Observable<Corral> {
    return this.http.put<Corral>(this.path + '/corral', corral, { headers: this.httpHeaders });
  }

  deleteCorral(id: number): Observable<Corral> {
    return this.http.delete<Corral>(`${this.path + '/corral'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para Camaras

  getCamaras() {
    return this.http.get<Camara[]>(this.path + '/camaras');
  }

  createCamara(camara: Camara): Observable<Camara> {
    return this.http.post<Camara>(this.path + '/camara', camara, { headers: this.httpHeaders });
  }

  updateCamara(camara: Camara): Observable<Camara> {
    return this.http.put<Camara>(this.path + '/camara', camara, { headers: this.httpHeaders });
  }

  deleteCamara(id: number): Observable<Camara> {
    return this.http.delete<Camara>(`${this.path + '/camara'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para transportes

  getTransportes() {
    return this.http.get<Transporte[]>(this.path + '/transportes');
  }

  createTransporte(transporte: Transporte): Observable<Transporte> {
    return this.http.post<Transporte>(this.path + '/transporte', transporte, { headers: this.httpHeaders });
  }

  updateTransporte(transporte: Transporte): Observable<Transporte> {
    return this.http.put<Transporte>(this.path + '/transporte', transporte, { headers: this.httpHeaders });
  }

  deleteTransporte(id: number): Observable<Transporte> {
    return this.http.delete<Transporte>(`${this.path + '/transporte'}/${id}`, { headers: this.httpHeaders });
  }


  // Servicios para VENTAS

  getVentas() {
    return this.http.get<Venta[]>(this.path + '/ventas');
  }

  createVenta(venta: Venta): Observable<Venta> {
    return this.http.post<Venta>(this.path + '/venta', venta, { headers: this.httpHeaders });
  }

  updateVenta(venta: Venta): Observable<Venta> {
    return this.http.put<Venta>(this.path + '/venta', venta, { headers: this.httpHeaders });
  }

  deleteVenta(id: number): Observable<Venta> {
    return this.http.delete<Venta>(`${this.path + '/venta'}/${id}`, { headers: this.httpHeaders });
  }


  // Reportes
  imprimirTropa(ingresoId: Number) {
     const httpOptions = {
       'responseType': 'arraybuffer' as 'json',
     };
     return this.http.get<any>(`${this.path + '/imprime-romaneos-ingreso'}/${ingresoId}`, httpOptions);
  }
  // imprimirRomaneo(romaneoId: Number) {
  //    const httpOptions = {
  //      'responseType': 'arraybuffer' as 'json',
  //    };
  //    return this.http.get<any>(`${this.path + '/imprime-romaneo'}/${romaneoId}`, httpOptions);
  // }

  imprimirRomaneo(listaRomaneo: Romaneo[]): Observable<any> {
    // return this.http.post<any>(this.path + '/imprime-romaneo', listaRomaneo, { headers: this.httpHeaders });
    const httpOptions = {
      'responseType': 'arraybuffer' as 'json',
    };
    return this.http.post<any>(this.path + '/imprime-romaneo', listaRomaneo, httpOptions);
  }
  imprimirRomaneoCuarteo(listaRomaneo: Romaneo[]): Observable<any> {
    // return this.http.post<any>(this.path + '/imprime-romaneo', listaRomaneo, { headers: this.httpHeaders });
    const httpOptions = {
      'responseType': 'arraybuffer' as 'json',
    };
    return this.http.post<any>(this.path + '/imprime-romaneo-cuarteo', listaRomaneo, httpOptions);
  }

}
