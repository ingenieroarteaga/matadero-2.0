import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable} from 'rxjs';

import { Global } from '../../../app/global';
import { Usuario } from '../../@theme/components/header/usuario';

// Constantes para guardar en storage
const userName =  'userName';
const userApellido = 'userApellido';
const userNick = 'userNick';
// let counter = 0;

@Injectable()
export class UserService {

  // Varible para asginar nombre
  public userName;
  path: string = Global.urlBase;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(public http: HttpClient, private router: Router ) {

  }


  private userArray: any[];

  // getUsers(): Observable<any> {
  //   return observableOf(this.users);
  // }
  getUsers() {
    return this.http.get<Usuario[]>(this.path + '/usuarios');
  }
  getUser() {
     this.userName = sessionStorage.getItem(userName);
     return this.userName;
  }
  getUserNick() {
      return sessionStorage.getItem(userNick);
  }
  getUsuarioByUsername(username: string) {
     this.http.get<Usuario>(`${this.path + '/usuario-username'}/${username}`).subscribe(
      infoUser => {
        console.log('el usuario es', infoUser);
        window.sessionStorage.removeItem(userName);
        window.sessionStorage.setItem(userName, infoUser.nombre);
        window.sessionStorage.removeItem(userApellido);
        window.sessionStorage.setItem(userApellido, infoUser.apellido);
        window.sessionStorage.removeItem(userNick);
        window.sessionStorage.setItem(userNick, infoUser.username);
        this.router.navigate(['pages/operaciones/ingresos']);
      },
    );
  }
  updateUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>(this.path + '/usuario', usuario, { headers: this.httpHeaders });
  }
  // getUserArray(): Observable<any[]> {
  //   return observableOf(this.userArray);
  // }
  //
  // getUser(): Observable<any> {
  //   counter = (counter + 1) % this.userArray.length;
  //   return observableOf(this.userArray[counter]);
  // }
}
