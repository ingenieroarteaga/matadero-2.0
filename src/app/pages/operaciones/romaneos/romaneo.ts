import { Ingreso } from '../ingresos/ingreso';
import { TropaDetalle } from '../ingresos/tropa-detalle';
import {Faena} from './faena';
import {Camara} from '../../herramientas/camaras/camara';

export class Romaneo {
        public id: number;
        public fecha: Date;
        public garron: number;
        public peso: number;
        public pesoVivo: number;
        public observarciones: string;
        public ingreso: Ingreso;
        public ingresoCamara: any;
        public tropaDetalle: TropaDetalle;
        public faena: Faena;
        public fechaCamara: Date;
        public fechaVenc: Date;
        public codigoBarra: string;
        public camara: Camara;
}
