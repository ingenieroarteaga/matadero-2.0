import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { NbDialogService } from '@nebular/theme';
import { InvalidFormComponent } from '../../personas/matarifes/agregar-matarife/invalid-form/invalid-form.component';
// Necesarioas para el mensaje toaster
import { ToasterConfig } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Romaneo } from './romaneo';
import { MovimientoCamara } from '../movimientos-camara/movimiento-camara';
import { Faena } from './faena';
import { Ingreso } from '../ingresos/ingreso';
import { Operario } from '../../personas/operarios/operario';
import { Camara } from '../../herramientas/camaras/camara';
import {ListaMatanza} from '../listas-matanza/lista-matanza';
import {TropaDetalle} from '../ingresos/tropa-detalle';

@Component({
  selector: 'ngx-romaneos',
  templateUrl: './romaneos.component.html',
  styleUrls: ['./romaneos.component.scss'],
})


export class RomaneosComponent implements OnDestroy {
    romaneos: any[];
   // para cargar las las lista de matanzas
   listaCargador: ListaMatanza = new ListaMatanza();
   listasPendientes: ListaMatanza[] = [];
   listaSeleccionada: ListaMatanza = new ListaMatanza ();
   // Si ya tienen faena uso update y no create
   listasConFaena: ListaMatanza[] = [];
   // para cargar las TropaDetalle
   tropaDetalleCargador: TropaDetalle = new TropaDetalle();
   tropasDetallesList: TropaDetalle[] = [];
   tropasDetallesElegidas: TropaDetalle[] = [];
   ingresosElegidos: Ingreso[] = [];
   // para cargar romaneos a faenar
   romaneoCargador: Romaneo = new Romaneo;
   romaneoListGeneral = [];
   // para carga faena
   operarios: Operario[] = [];
   camaras: Camara[] = [];
   camarasRefrigerado: Camara[] = [];
   camaraElegida: number;
   romaneoDatos = {
     peso: [] ,
   };
   // para cargar movimiento de camara
   movimientoCamara: MovimientoCamara = new MovimientoCamara();
   // para ocultar ingresos ya cargados
   hideIngresos = {
     id: [],
   };
   faenaFinal: Faena = new Faena;
   // Habilitación de boton de terminado
   btnTerminado: boolean = true;

   // Datos para mostrar notificaciones
   config: ToasterConfig;
   index = 1;
   destroyByClick = true;
   duration = 3500;
   hasIcon = true;
   position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
   preventDuplicates = false;
   status: NbToastStatus;
   titleToast: string;
   contentToat: string;
   // para cortar el servicio cuando cambio da pantalla
   getMatanzaSubscribe: Subscription;
   ///////// Buscar
  search: any = {};
  totalPages: Array<number>;

  page = 0;
  size = 5;
  order = 'id';
  asc = false;

  isFirst = false;
  isLast = false;



   constructor(public service: SmartTableService,
              private toastrService: NbToastrService,
              private dialogService: NbDialogService) {

              // Obtengo las lista de matanza
              this.getMatanzaSubscribe = service.getMatanzasPaginado(this.search, this.page, this.size,
                this.order, this.asc ).subscribe(
                 data => {
                   this.listasPendientes  = data.content;
                   this.isFirst = data.first;
                   this.isLast = data.last;
                   this.totalPages = new Array(data.totalPages);
                   console.log(data);
                   // cargo la lista de matanzas con faena
                   data.content.forEach(lista => {
                     if (lista.faena !== null) {
                       this.listaCargador = lista;
                       console.log(this.listaCargador);
                       this.listasConFaena.push(this.listaCargador);
                     }
                   });
                   console.log('Listas con faena actualizada');
                  },
                );
              // Obtengo los operarios
            this.service.getOperarios().subscribe(
              data => {
                  this.operarios = data;
                  console.log(this.operarios);
                  console.log('Se hizo el traspaso de operarios');
                },
              );

              // Obtengo las camaras
            this.service.getCamaras().subscribe(
              data => {
                  this.camaras = data;
                  console.log(this.camaras);
                  this.camaras.forEach(camara => {
                    if (camara.tipoCamara.id === 1) {
                        this.camarasRefrigerado.push(camara);
                    }
                  });
                  console.log('Se hizo el traspaso de camaras');
                },
              );
  }

  /////// Corto la carga de registros
   ngOnDestroy() {
     if (typeof this.getMatanzaSubscribe !== 'undefined') {
       this.getMatanzaSubscribe.unsubscribe();
     }
   }


  seleccionarLista(lista) {
    this.listaSeleccionada = lista;
    console.log(lista);
    const listTropasId = [];
    this.tropasDetallesElegidas = [];
    this.ingresosElegidos = [];
    // recorro los ingresos para obtener el id de tropa
    let i = 1;
    lista.ingresoList.forEach (ingreso => {
      // primero filtro los ingresos que no tienen faena
      this.service.getRomaneosByIngresoCount(ingreso.id).subscribe(
      romaneosByIngreso => {
             console.log(romaneosByIngreso);
             if (romaneosByIngreso >= 1) {
                 console.log('el ingreso ya fue faenado');
                 // aca debeo poner una bander de que en esta lista
                 // hay un ingreso que ya fue faenado
             } else {
               // si el ingreso no está faenada cargo el id de tropa
               listTropasId.push(ingreso.tropa.id);
               this.ingresosElegidos.push(ingreso);
               console.log('se cargo un ingreso');
             }
             if (lista.ingresoList.length === i) {
               // si no hay ningun ingreso sin faena, doy aviso
               if (listTropasId.length === 0) {
                 this.dialogService.open(InvalidFormComponent, {
                   context: {
                     title: 'La lista seleccionada tiene todas las tropas faenadas',
                   },
                   closeOnEsc: false,
                   closeOnBackdropClick: true,
                 });
               } else {
                 // envio los id de las tropas que no tienen faena
                 this.asignarTropas(listTropasId);
                 console.log('llame a asignar tropa');
               }
             }
             i = i + 1;
         },
       );
    });
  }
  asignarTropas(listTropasId) {

    listTropasId.forEach( tropaId => {
        // Obtengo las tropasDetalle que coinciden con el id de tropa
      this.service.getTropasDetallesByTropa(tropaId).subscribe( data => {
        for (let i = 0; i < data.length; i++) {
          this.tropasDetallesElegidas.push(data[i]);
        }
        console.log('agregue tropa detalle');
        console.log(this.tropasDetallesElegidas, this.ingresosElegidos);
        this.armoFaena();
      });
    });
  }


  armoFaena() {
    this.romaneoListGeneral = [];
    let numGarron: number = 0;
    // Si la lista ya tiene algun ingreso faenado
    // obtengo el ultimo número de garron
    if (this.listaSeleccionada.faena !== null) {
      numGarron = this.listaSeleccionada.faena.cantidad;
    }
    // ordeno para que la faena se arme por id
    this.ingresosElegidos.sort((a, b) => 0 - (a.id > b.id ? -1 : 1));
    console.log(this.ingresosElegidos);
    this.ingresosElegidos.forEach( ingreso => {
      const romaneoList: Romaneo [] = [];
      // orden las tropa-detalle de las que menor cantidad tiene a las de mayor
      this.tropasDetallesElegidas.sort((a, b) => 0 - (a.cantidad > b.cantidad ? -1 : 1));
      this.tropasDetallesElegidas.forEach(tropaDetalle => {
        if (tropaDetalle.tropa.id === ingreso.tropa.id) {
          // agrego garron por garron
          for (let i = 1; i <= tropaDetalle.cantidad; i++) {
            numGarron =  numGarron + 1;
            this.romaneoCargador = {
                id: null,
                fecha: new Date(),
                garron: numGarron,
                peso: 0,
                pesoVivo: 0,
                observarciones: 'obs',
                ingreso: ingreso,
                ingresoCamara: null,
                tropaDetalle: tropaDetalle,
                faena: null,
                fechaCamara: new Date(),
                fechaVenc: new Date(),
                codigoBarra: null,
                camara: null,
            };
            this.romaneoCargador.fechaVenc.setDate(this.romaneoCargador.fechaVenc.getDate() + 7);
            romaneoList.push(this.romaneoCargador);
          }
        }
      });
      this.romaneoListGeneral.push(romaneoList);
    });
    console.log(this.romaneoListGeneral);
    this.btnTerminado = false;
  }

  decomizar (romaneo) {
    this.dialogService.open(InvalidFormComponent, {
      context: {
        title: 'Decomizar está desactivado',
      },
      closeOnEsc: false,
      closeOnBackdropClick: true,
    });
  }

  terminar (romaneoList, idRomaneoList) {
    let error: string = '';
    if (window.confirm('Confirma la faena de esta tropa?' )) {
      console.log(romaneoList);
      let tienePeso = 0;
      // recorro romaneos para ver si todos tienen peso y si no superan el limite
      romaneoList.forEach( romaneo => {
          romaneo.peso = this.romaneoDatos.peso[romaneo.garron];
          if (this.romaneoDatos.peso[romaneo.garron] === undefined) {
              tienePeso = 1;
              error = 'El garron ' + romaneo.garron + ' no tiene peso asignado';
              console.log( 'el garron ', romaneo.garron , ' no tiene peso asignado');
          } else if (this.romaneoDatos.peso[romaneo.garron] > 60) {
            tienePeso = 1;
            error = 'El garron ' + romaneo.garron + ' pesa más de 60kg';
            console.log( 'el garron', romaneo.garron , 'pesa más de 60kg');
          } else if (this.romaneoDatos.peso[romaneo.garron] < 3) {
            tienePeso = 1;
            error = 'El garron ' + romaneo.garron + ' pesa menos 3kg';
            console.log( 'el garron', romaneo.garron , 'pesa meno de 3kg');
          }
      });
      // compruebo que haya seleccionado una camara
      if (this.camaraElegida === null) {
          tienePeso = 1;
          error = 'Debe elegir camara';
          console.log( 'debe elegir camara');
      } else if (this.camaraElegida === undefined) {
        tienePeso = 1;
        error = 'Debe elegir camara';
        console.log( 'debe elegir camara');
      }
      if (tienePeso === 0) {
        // Chequeo si las lista con la que estoy trabajando ya tienen una faena asiganda
        console.log(this.listasConFaena);
        const bandera = this.listasConFaena.find(
                        listaConFaena => listaConFaena.id === this.listaSeleccionada.id);
        // Si la lista no tiene faena creo una, si ya tiene actualizo
        console.log(bandera);
        if ( bandera === undefined ) {
          this.faenaFinal = {'id': null,
                          'fecha': new Date(),
                          'numero': null,
                          'cantidad': romaneoList.length,
                          'operario': null,
                          'mantanzaList': this.faenaFinal.mantanzaList = [],
                          'tipoFaena': null,
                          'romaneoList': null,
                        };
          this.faenaFinal.operario = this.operarios.find(operario =>
                                                           operario.id === 1 );
          // this.faenaFinal.mantanzaList.push(this.listaSeleccionada);
          this.faenaFinal.romaneoList = romaneoList;
          console.log(this.listaSeleccionada);
          console.log(this.faenaFinal);
          this.service.createFaena(this.faenaFinal).subscribe(datas => {
            console.log(datas);
            // agrego id de faena en las listas de matanza
            this.listaSeleccionada.faena = datas;
            this.listaSeleccionada.ingresoList = [];
            // asigno faena y camara a cada romaneo de la lista que voy a enviar
            romaneoList.forEach( romaneo => {
                romaneo.faena = datas;
                romaneo.camara = this.romaneoCargador.camara = this.camaras.find(camara =>
                                                                   camara.id === this.camaraElegida);
            });
            this.service.updateMatanza(this.listaSeleccionada).subscribe( matanza => {
                console.log(matanza);
                // envio la lista de romaneos a crear
                this.service.createRomaneoList(romaneoList).subscribe( romaneoCargado => {
                  console.log(romaneoCargado);
                  // Renuevo la lista de matanzas con faena para que la nueva aparezca
                  this.listasConFaenaRefresh();
                  this.status = NbToastStatus.SUCCESS;
                  this.titleToast = 'Romaneo exitoso';
                  // cuando falle
                  // this.status =  NbToastStatus.WARNING;
                  // this.titleToast =  'No se pudo cargar la Faena'
                  this.showToast(this.status, this.titleToast, this.contentToat);
                });
             },
                errorUpadteMatanza => {
                console.log(errorUpadteMatanza);
                this.dialogService.open( InvalidFormComponent, {
                  context: {
                    title: 'No se pudo actualizar la matanza',
                  },
                  closeOnEsc: false,
                  closeOnBackdropClick: true,
                });
              },
              );
          },
            errorFaena => {
            console.log(errorFaena);
            this.dialogService.open( InvalidFormComponent, {
              context: {
                title: 'No se pudo crear la Faena',
              },
              closeOnEsc: false,
              closeOnBackdropClick: true,
            });
          },
        );
        this.hideIngresos.id[idRomaneoList] = true;
        } else {
          this.faenaFinal = {'id': bandera.faena.id,
                          'fecha': bandera.faena.fecha,
                          'numero': bandera.faena.numero,
                          'cantidad': bandera.faena.cantidad + romaneoList.length,
                          'operario': bandera.faena.operario,
                          'mantanzaList': this.faenaFinal.mantanzaList = [],
                          'tipoFaena': bandera.faena.tipoFaena,
                          'romaneoList': [],
                        };
          console.log(this.faenaFinal);
          this.service.updateFaena(this.faenaFinal).subscribe(datas => {
            console.log(datas);
            // agrego id de faena en las listas de matanza
            this.listaSeleccionada.faena = datas;
            this.listaSeleccionada.ingresoList = [];
            // asigno faena y camara a cada romaneo de la lista que voy a enviar
            romaneoList.forEach( romaneo => {
                romaneo.faena = datas;
                romaneo.camara = this.romaneoCargador.camara = this.camaras.find(camara =>
                                                                   camara.id === this.camaraElegida);
            });
            this.service.updateMatanza(this.listaSeleccionada).subscribe( matanza => {
                console.log(matanza);
                // envio la lista de romaneos a crear
                this.service.createRomaneoList(romaneoList).subscribe( romaneoCargado => {
                  console.log(romaneoCargado);
                  // Renuevo la lista de matanzas con faena para que la nueva aparezca
                  this.listasConFaenaRefresh();
                  this.status = NbToastStatus.SUCCESS;
                  this.titleToast = 'Romaneo exitoso';
                  // cuando falle
                  // this.status =  NbToastStatus.WARNING;
                  // this.titleToast =  'No se pudo cargar la Faena'
                  this.showToast(this.status, this.titleToast, this.contentToat);
                });
             },
                errorUpadteMatanza => {
                console.log(errorUpadteMatanza);
                this.dialogService.open( InvalidFormComponent, {
                  context: {
                    title: 'No se pudo actualizar la matanza',
                  },
                  closeOnEsc: false,
                  closeOnBackdropClick: true,
                });
              },
              );
          },
            errorUpadteFaena => {
            console.log(errorUpadteFaena);
            this.dialogService.open( InvalidFormComponent, {
              context: {
                title: 'No se pudo actualizar la Faena',
              },
              closeOnEsc: false,
              closeOnBackdropClick: true,
            });
          },
        );
        this.hideIngresos.id[idRomaneoList] = true;
        }
      } else {
        // muestro la variable error que puede ser por falta de peso
        // peso bajo o alto y por no elegir camara
        this.dialogService.open(InvalidFormComponent, {
          context: {
            title: error,
          },
          closeOnEsc: false,
          closeOnBackdropClick: true,
        });
      }
    } else {
      return;
    }
  }

  listasConFaenaRefresh () {
    // Obtengo las lista de matanza
    this.getMatanzaSubscribe = this.service.getMatanzasPaginado(this.search, this.page, this.size,
      this.order, this.asc ).subscribe(
       data => {
         this.listasPendientes  = data.content;
         this.isFirst = data.first;
         this.isLast = data.last;
         this.totalPages = new Array(data.totalPages);
         console.log(data);
         // primero vacio la anterior y luego cargo la lista de matanzas con faena actualizada
         this.listasConFaena = [];
         data.content.forEach(lista => {
           if (lista.faena !== null) {
             this.listaCargador = lista;
             console.log(this.listaCargador);
             this.listasConFaena.push(this.listaCargador);
           }
         });
         console.log('Listas con faena actualizada');
        },
      );
  }

  // Funciones para mostrar Toastear
  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  /////////////////////////////////////
 buscar() {
   this.listasPendientes = [];

   const v_search: any = {};
   v_search.numero = this.search.numero;

   if (typeof this.search.desde !== 'undefined' && this.search.desde !== null) {
     v_search.desde = this.search.desde;
   } else {
     v_search.desde = null;
   }
   if (typeof this.search.hasta !== 'undefined' && this.search.hasta !== null) {
     v_search.hasta = this.search.hasta;
   } else {
     v_search.hasta = null;
   }

   if (typeof this.search.cliente !== 'undefined' && this.search.cliente !== null) {
     v_search.cliente = this.search.cliente.id;
   } else {
     v_search.cliente = null;
   }

   if (typeof this.search.sucursal !== 'undefined' && this.search.sucursal !== null) {
     v_search.sucursal = this.search.sucursal.id;
   } else {
     v_search.sucursal = null;
   }

   if (typeof this.search.puntoVenta !== 'undefined' && this.search.puntoVenta !== null) {
     v_search.puntoVenta = this.search.puntoVenta.id;
   } else {
     v_search.puntoVenta = null;
   }

   if (typeof this.search.tipoFactura !== 'undefined' && this.search.tipoFactura !== null) {
     v_search.tipoFactura = this.search.tipoFactura.id;
   } else {
     v_search.tipoFactura = null;
   }

   this.service.getMatanzasPaginado(
     v_search, this.page, this.size, this.order, this.asc).subscribe(
       data => {
         this.listasPendientes = data.content;

         this.isFirst = data.first;
         this.isLast = data.last;
         this.totalPages = new Array(data.totalPages);

       },
       err => {
         console.log(err.error);
       },
     );
 }
 ////////////////////////////////////

  /////////////// Paginacion //////////////////////////
  sort(): void {
    this.asc = !this.asc;
    this.buscar();
  }

  rewind(): void {
    if (!this.isFirst) {
      this.page--;
      this.buscar();
    }
  }

  forward(): void {
    if (!this.isLast) {
      this.page++;
      this.buscar();
    }
  }

  setPage(page: number): void {
    this.page = page;
    this.buscar();
  }

  setOrder(order: string): void {
    this.order = order;
    this.buscar();
  }
  //////////////////// Fin Paginacion ////////////////////////////

}
// FALTA
// 1- Agregar el operario que realiza la faena
