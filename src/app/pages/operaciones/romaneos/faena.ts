import { Romaneo } from './romaneo';
import {ListaMatanza} from '../listas-matanza/lista-matanza';
import {Operario} from '../../personas/operarios/operario';


export class Faena {
        public id: number;
        public fecha: Date;
        public numero: number;
        public cantidad: number;
        public operario: Operario;
        public mantanzaList: ListaMatanza[];
        public tipoFaena: any;
        public romaneoList: Romaneo[];
}
