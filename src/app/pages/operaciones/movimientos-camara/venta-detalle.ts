import { Romaneo } from '../romaneos/romaneo';
import { Venta } from './venta';


export class VentaDetalle {
        public id: number;
        public precio: number;
        public venta: Venta;
        public romaneo: Romaneo;
}
