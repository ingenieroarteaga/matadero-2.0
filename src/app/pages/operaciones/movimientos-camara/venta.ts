import { VentaDetalle } from './venta-detalle';


export class Venta {
        public id: number;
        public fecha: Date;
        public monto: number;
        public ventaDetalleList: VentaDetalle[];
}
