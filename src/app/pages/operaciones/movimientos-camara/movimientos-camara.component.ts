// tslint:disable-next-line:max-line-length
import { Component, OnDestroy } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { NbDialogService } from '@nebular/theme';
import { InvalidFormComponent } from '../../personas/matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { FormsModule } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
// Necesarios par alas notificaciones
// Necesarioas para el mensaje toaster
import { ToasterConfig } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
// Datos para el ingreso
import {Ingreso} from '../ingresos/ingreso';
import {Romaneo} from '../romaneos/romaneo';
import {Camara} from '../../herramientas/camaras/camara';
import { MovimientoCamara } from '../movimientos-camara/movimiento-camara';
import { Venta } from './venta';
import { VentaDetalle } from './venta-detalle';
import { Subscription } from 'rxjs';

@Component({
selector: 'ngx-movimientos-camara',
templateUrl: './movimientos-camara.component.html',
styleUrls: ['./movimientos-camara.component.scss'],
})


export class MovimientosCamaraComponent  implements OnDestroy {

// guardo los ingresos por camara
ingresosRefrigerados: any = [];
ingresosTunel: any = [];
ingresosOreo: any = [];
ingresosCongelados: any = [];
// para guardar los romaneos que deseo mover
romaneosParaMover: any = [];
// para guardar las camaras
camaras: any = [];

// para listar  los ingresos ya cargados
ingresos: any = [];

camaraInicial: string = 'Refrigeración 1';

// para realizar MOVIMIENTOS POR INGRESO
ingresoMov: Ingreso = new Ingreso();
camaraFormRef: any = [];
camaraFormTun: any = [];
camaraFormCon: any = [];
camaraMov: Camara = new Camara();
romaneoMov: Romaneo = new Romaneo();
movimientoCamara: MovimientoCamara = new MovimientoCamara();
banderaExito: number = 0;
cantidadGarrones: number = 0;
// bandera para contar repiticiones de update
procesoCompleto: number = 0;

// para realizar MOVIMIENTOS POR GARRON
garronesMovGarron: any = [];
// MOSTRAR NOTIFICACIONES
config: ToasterConfig;
index = 1;
destroyByClick = true;
duration = 4500;
hasIcon = true;
position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
preventDuplicates = false;
status: NbToastStatus;
titleToast: string;
contentToat: string;
// Para mosrtar subtabla por garrones dependiendo la camara
public subTableGarron1: boolean = false;
public subTableGarron2: boolean = false;
public subTableGarron3: boolean = false;

// Para realizar retiros (ventas)
venta: Venta;
ventaDetalleCargador: VentaDetalle;
ventaDetalleCompleto: VentaDetalle[] = [];

// para cortar el servicio cuando cambio da pantalla
getIngresosEnCongeSubscribe: Subscription;
getCamaraSubscribe: Subscription;
getIngresosEnRefriSubscribe: Subscription;

constructor(public service: SmartTableService,
          private dialogService: NbDialogService,
          private toastrService: NbToastrService) {

            // Obtengo los ingresos que estan en camaras refrigerado id 1
            this.getIngresosEnRefriSubscribe = service.getIngresosEnCamara(1).subscribe ( ingresosEnRefrigerado =>
              this.ingresosRefrigerados = ingresosEnRefrigerado,
            );
            console.log(this.ingresosRefrigerados);
            // Obtengo los ingresos que estan en camaras Tunel id 2
            service.getIngresosEnCamara(2).subscribe ( ingresosEnTunel =>
              this.ingresosTunel = ingresosEnTunel,
            );
            console.log(this.ingresosTunel);
            // Obtengo los ingresos que estan en camaras Oreo id 3
            service.getIngresosEnCamara(3).subscribe ( ingresosEnOreo =>
              this.ingresosOreo = ingresosEnOreo,
            );
            console.log(this.ingresosOreo);
            // Obtengo los ingresos que estan en camaras Congelado id 4
              this.getIngresosEnCongeSubscribe = service.getIngresosEnCamara(4).subscribe ( ingresosEnCongelado =>
              this.ingresosCongelados = ingresosEnCongelado,
            );
            console.log(this.ingresosCongelados);
            // Obtengo las camaras
              this.getCamaraSubscribe = service.getCamaras().subscribe(
              data => {
                     this.camaras = data;
                     console.log(this.camaras);
            });
}

/////// Corto la carga de registros
 ngOnDestroy() {
   if (typeof this.getIngresosEnRefriSubscribe !== 'undefined') {
     this.getIngresosEnRefriSubscribe.unsubscribe();
   }
   if (typeof this.getIngresosEnCongeSubscribe !== 'undefined') {
     this.getIngresosEnCongeSubscribe.unsubscribe();
   }
   if (typeof this.getCamaraSubscribe !== 'undefined') {
     this.getCamaraSubscribe.unsubscribe();
   }
 }

// METODO PARA INICIAR EL MOVIMIENTO POR INGRESO
// Recibo del formulario el ingreso completo que quiero moverIngreso
// la variable actual que me indica en que tipo de camara está el ingresos
// y la variable i que me indica el id de la camara donde tengo que mover el ingreso
moverIngreso(ingreso, i, actual) {
  if (window.confirm('Estás seguro que quieres realizar el movimiento?')) {
    this.ingresoMov = ingreso;
    console.log(this.ingresoMov);
    const camaraActual = actual;
    // Obtengo los romaneos del ingreso que quiero mover
    this.service.getRomaneosByIngreso(this.ingresoMov.id).subscribe( data => {
      // tengo que filtrar este resultado para mostrar unicamente
      // los romaneos que coinciden en id de ingreso y de camara
      this.romaneosParaMover = data,
      console.log(this.romaneosParaMover);
      this.romaneosParaMover.forEach( romaneoParaMover => {
        this.romaneoMov = romaneoParaMover;
        // envio el romaneo, el id de camara nueva y la camara actual
        this.updateRomaneo(this.romaneoMov, i, camaraActual);
      });
    });
   // En caso de no aceptar el movimiento
  } else {
    return;
  }
}

// METODO PARA HACER EL UPADTE DE ROMANEOS
updateRomaneo(romaneoCargador, i, estoy) {
  const romaneoUpate = romaneoCargador;
  let estoyForm = 0;
  console.log(i);
  // bandera para indicar si encontre camara
  this.banderaExito = 0;
  // Con la variable estoy indico en que camara está
  // el ingreso que quiero mover. Identifico la camara
  // y asigno en que varbile form esta el dato de la camara
  // a la cual quiere mover.
  if (estoy === 1) {
     estoyForm = this.camaraFormRef;
  } else if ( estoy === 2) {
     estoyForm = this.camaraFormTun;
  } else if ( estoy === 3) {
     estoyForm = this.camaraFormCon;
  }
  // Busco la nueva camara que quiero asignar
  // a partir del [i] recibido en la variable i
  // sino encuento camara me fijo si es un retiro
  this.camaras.find(camara => {
  if (camara.id == estoyForm[i] ) {
      // si encuento camara cambio la bandera a 1 para evitar show error
      this.banderaExito = 1;
      console.log('es un movimiento');
      this.camaraMov = camara;
      console.log(this.camaraMov);
      // armo el romaneo para actualizarlo con los datos del actual
      // y le sumo los datos de la nueva camara
      this.romaneoMov = {
        'id': romaneoUpate.id,
        'fecha': romaneoUpate.fecha,
        'garron': romaneoUpate.garron,
        'peso': romaneoUpate.peso,
        'pesoVivo': romaneoUpate.pesoVivo,
        'observarciones': romaneoUpate.observarciones,
        'ingreso': romaneoUpate.ingreso,
        'ingresoCamara': romaneoUpate.ingresoCamara,
        'tropaDetalle': romaneoUpate.tropaDetalle,
        'faena': romaneoUpate.faena,
        'fechaCamara': romaneoUpate.fechaCamara,
        'fechaVenc': new Date(),
        'codigoBarra': romaneoUpate.codigoBarra,
        'camara': this.camaraMov,
      };
      // si el movimiento es hacia una camara de congelado le agrego
      // 365 días a la fecha de vencimiento.
      if (this.camaraMov.tipoCamara.id === 4) {
        this.romaneoMov.fechaVenc = new Date(romaneoUpate.fecha);
        this.romaneoMov.fechaVenc.setDate(this.romaneoMov.fechaVenc.getDate() + 365);
      }
      console.log(romaneoUpate);
      console.log(this.romaneoMov);
      // envio el romaneo para ser actualizado
      this.service.updateRomaneo(this.romaneoMov).subscribe(romaneoRefreshed => {
        console.log('se hizo el refresh del romeneo ' + romaneoRefreshed.id);
        // creo movimiento de camara
        this.movimientoCamara = {'id': null,
                        'fecha': new Date(),
                        'camara': romaneoRefreshed.camara,
                        'romaneo': romaneoRefreshed,
                      };
        this.service.createMovimientoCamara(this.movimientoCamara)
        .subscribe(movimientoCargado => {
          console.log(movimientoCargado);
          console.log('se hizo movimiento de camara del romaneo' + romaneoRefreshed.id);
          this.procesoCompleto = this.procesoCompleto + 1;
          console.log(this.procesoCompleto);
          // Si ya actualice todo los romaneos de romaneosParaMover hago refresh
          if (this.procesoCompleto === this.romaneosParaMover.length) {
            console.log(this.procesoCompleto);
            // si se terminaron las repeticiones pido el refresh;
            this.refreshTablas();
          }
        });
      });
  } else if (estoyForm[i] === 'retiro') {
    // si es un retiro cambio la bandera a 1 para evitar mostrar error
      this.banderaExito = 1;
      console.log('es un retiro');
      // armo el objeto ventaDetalle es uno por garron
      this.ventaDetalleCargador = new VentaDetalle();
      this.ventaDetalleCargador = {
        'id': null,
        'precio': 0, // actulamente no asignamos precio
        'venta': null,
        'romaneo': romaneoUpate, // inserto el romaneo actual
      };
      console.log(this.ventaDetalleCargador);
      // Agrego la ventaDetalle de este garron a la ventaDetalle General
      this.ventaDetalleCompleto.push(this.ventaDetalleCargador);
      this.procesoCompleto = this.procesoCompleto + 1;
      console.log(this.procesoCompleto);
      // Esta cóncidición me indica si ya cargue todos los garrones
      // a la VentaDetalleCompleto, sino sigue cargando
      if (this.procesoCompleto === this.romaneosParaMover.length) {
        console.log(this.procesoCompleto);
        // armo el objeto venta con sus propiedades y la lista ventaDetalleList
        this.venta = new Venta();
        this.venta = {
          'id': null,
          'fecha': new Date(),
          'monto': 0, // Debería ser la sumatoria de precios de VentaDetalle
          'ventaDetalleList': this.ventaDetalleCompleto,
        };
        // envio el objeto venta para armado
        this.service.createVenta(this.venta).subscribe (ventaCreada => {
          console.log('se realizo el retiro', ventaCreada);
          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Se realizo el retiro con exito',
            },
            closeOnEsc: false,
            closeOnBackdropClick: true,
          });
          this.refreshTablas();
        });
        // this.dialogService.open(InvalidFormComponent, {
        //   context: {
        //     title: 'El usuario no está autorizado a realizar retiros',
        //   },
        //   closeOnEsc: false,
        //   closeOnBackdropClick: true,
        // });
      }
  }
});
  if (this.banderaExito === 0) {
    this.dialogService.open(InvalidFormComponent, {
      context: {
        title: 'No se encontro la camara donde quiere asignar el garron',
      },
      closeOnEsc: false,
      closeOnBackdropClick: true,
    });
  }
}


// METODO PARA MOSTRAR LA TABLA DE MOVIMIENTOS INDIVUALES
// Recibo del formulario el ingreso del que debo mostrar los garrones
// la variable actual que me indica en que tipo de camara está los garrones del ingreso
movimientoGarronShow(ingreso, actual) {
  this.dialogService.open(InvalidFormComponent, {
    context: {
      title: 'El usuario no está autorizado a realizar movimientos individuales',
    },
    closeOnEsc: false,
    closeOnBackdropClick: true,
  });
  // muestro la subtable dependiendo la camara
 //  if (actual === 1) {
 //    this.subTableGarron1 =  true;
 //    this.subTableGarron2 =  false;
 //    this.subTableGarron3 =  false;
 //  } else if (actual === 2) {
 //    this.subTableGarron1 =  false;
 //    this.subTableGarron2 =  true;
 //    this.subTableGarron3 =  false;
 //  } else {
 //    this.subTableGarron1 =  false;
 //    this.subTableGarron2 =  false;
 //    this.subTableGarron3 =  true;
 //  }
 //
 //  this.ingresoMov = ingreso;
 //  console.log(this.ingresoMov);
 //  const camaraActual = actual;
 //  // vacio el array de gaarrones a mostrara para cargar los nuevos
 //  this.garronesMovGarron = [];
 //  // camaraActual = 1 es Refrigerado, 2 Tunel y 3 congelado
 //  if (camaraActual === 1) {
 //    // Recorro la camara elegida y cargo todo los garrones
 //    // del ingreso en el array para mostrarlos
 //    this.romaneosRefrigerados.forEach(romaneosAmostrar => {
 //      if (romaneosAmostrar.ingreso.id === this.ingresoMov.id) {
 //          this.garronesMovGarron.push(romaneosAmostrar);
 //          console.log('el array de romanoes a mostrar es:' , this.garronesMovGarron);
 //      }
 //    });
 //  } else if (camaraActual === 2) {
 //    // Recorro la camara elegida y cargo todo los garrones
 //    // del ingreso en el array para mostrarlos
 //    this.romaneosTunel.forEach(romaneosAmostrar => {
 //      if (romaneosAmostrar.ingreso.id === this.ingresoMov.id) {
 //          this.garronesMovGarron.push(romaneosAmostrar);
 //          console.log('el array de romanoes a mostrar es:' , this.garronesMovGarron);
 //      }
 //    });
 // } else {
 //   // Recorro la camara elegida y cargo todo los garrones
 //   // del ingreso en el array para mostrarlos
 //   this.romaneosCongelados.forEach(romaneosAmostrar => {
 //     if (romaneosAmostrar.ingreso.id === this.ingresoMov.id) {
 //         this.garronesMovGarron.push(romaneosAmostrar);
 //         console.log('el array de romanoes a mostrar es:' , this.garronesMovGarron);
 //     }
 //   });
 // }
 // // ejecuto sort para mostrar los gaarrones en orden
 // this.garronesMovGarron.sort((a, b) => 0 - (a.garron > b.garron ? -1 : 1));
}



// METODO PARA HACER REFRESH DE LA SECCIÓN LUEGO DE MOVIMIENTOS
refreshTablas () {
  this.status = NbToastStatus.SUCCESS;
  this.titleToast = 'El movimiento fue realizado con exito';
  // cuando falle
  // this.status =  NbToastStatus.WARNING;
  // this.titleToast =  'No se pudo cargar Lista de Matanza'
  this.showToast(this.status, this.titleToast, this.contentToat);
  // Reseteo variables
  this.ingresosRefrigerados = [];
  this.ingresosCongelados = [];
  this.ingresosOreo = [];
  this.ingresosTunel = [];
  this.romaneosParaMover = [];
  this.ingresos = [];
  this.banderaExito = 0;
  this.cantidadGarrones = 0;
  this.procesoCompleto = 0;
  this.ventaDetalleCompleto = [];
  // Obtengo los ingresos que estan en camaras refrigerado id 1
  this.getIngresosEnRefriSubscribe = this.service.getIngresosEnCamara(1).subscribe ( ingresosEnRefrigerado =>
    this.ingresosRefrigerados = ingresosEnRefrigerado,
  );
  // Obtengo los ingresos que estan en camaras Tunel id 2
  this.service.getIngresosEnCamara(2).subscribe ( ingresosEnTunel =>
    this.ingresosTunel = ingresosEnTunel,
  );
  // Obtengo los ingresos que estan en camaras Oreo id 3
  this.service.getIngresosEnCamara(3).subscribe ( ingresosEnOreo =>
    this.ingresosOreo = ingresosEnOreo,
  );
  // Obtengo los ingresos que estan en camaras Congelado id 4
    this.getIngresosEnCongeSubscribe = this.service.getIngresosEnCamara(4).subscribe ( ingresosEnCongelado =>
    this.ingresosCongelados = ingresosEnCongelado,
  );
}


// METODO PARA MOSTRAR TOSTED
  private showToast(type: NbToastStatus, title: string, body: string) {
  const config = {
    status: type,
    destroyByClick: this.destroyByClick,
    duration: this.duration,
    hasIcon: this.hasIcon,
    position: this.position,
    preventDuplicates: this.preventDuplicates,
  };
  const titleContent = title ? `${title}` : '';

  this.index += 1;
  this.toastrService.show(
    body,
    `${titleContent}`,
    config);
  }


} // FIN
