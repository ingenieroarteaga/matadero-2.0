import { Romaneo } from '../romaneos/romaneo';
import { Camara } from '../../herramientas/camaras/camara';


export class MovimientoCamara {
        public id: number;
        public fecha: Date;
        public camara: Camara;
        public romaneo: Romaneo;
}
