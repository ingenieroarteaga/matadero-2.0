  // tslint:disable-next-line:max-line-length
import { Component, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { NbDialogService } from '@nebular/theme';
import { InvalidFormComponent } from '../../personas/matarifes/agregar-matarife/invalid-form/invalid-form.component';
// Necesarios par alas notificaciones
// Necesarioas para el mensaje toaster
import { ToasterConfig } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import { NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
// Datos para el ingreso
import {Faena} from './faena';
import {Tropa} from '../ingresos/tropa';
import {Ingreso} from '../ingresos/ingreso';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'ngx-faenas',
  templateUrl: './faenas.component.html',
  styleUrls: ['./faenas.component.scss'],
})
export class FaenasComponent implements OnDestroy {
  @ViewChild ('aPrint') aPrint: ElementRef;

  // para listar  las faenas
  faenas: any = [];
  faenaCargado: Faena =  new Faena();
  faenasCargados: Faena [] = [];
  ingresos: any = [];
  ingresoFaenado: Ingreso =  new Ingreso();
  ingresosFaenados:  Ingreso[] = [];
  // para listar  los ingresos ya cargados
  romaneosSelect: any = [];
  tropa: Tropa = new Tropa();
  ingreso: Ingreso = new Ingreso();
  // para mostrar la tabla seleccinoada
  showSelect: boolean = true;

  // Datos para mostrar notificaciones
  config: ToasterConfig;
  index = 1;
  destroyByClick = true;
  duration = 3500;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus;
  titleToast: string;
  contentToat: string;

  // Obtener el ingreso de la faena elegido para imprimir
  faenasListadas = {
    ingresoElegido: [],
    pesoTotal: 0,
    idIngreso: 1,
    promedio: 0,
  };
  printIndividual =  {
    checkbox: [],
    idRomaneo: [],
  };
  listaRomaneosImprimir: any = [];
  checkAll:  boolean = false;
  // para cortar el servicio cuando cambio da pantalla
  getFaenasSubscribe: Subscription;

  /////////// Paginacion
  ///////// Buscar faenas
 search: any = {};
 totalPages: Array<number>;

// paginado faenas
 page = 0;
 size = 5;
 order = 'id';
 asc = false;

 isFirst = false;
 isLast = false;


 ///////// Buscar romaneos de faenas
ingresoId: any = {};
totalPagesRomaneo: Array<number>;

 // paginado romaneos de faenas
  pageRomaneo = 0;
  sizeRomaneo = 50;
  orderRomaneo = 'id';
  ascRomaneo = true ;

  isFirstRomaneo = false;
  isLastRomaneo = false;

// exportacion senasa
fileCsv: any = [];
options = {
   fieldSeparator: ',',
   quoteStrings: '"',
   decimalseparator: '.',
   showLabels: false,
   headers: [],
   showTitle: false,
   title: '',
   useBom: false,
   removeNewLines: true,
   keys: [],
};


  constructor(public service: SmartTableService,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private datePipe: DatePipe) {

                // Obtengo las faenas
               this.getFaenasSubscribe = service.getFaenasPaginado(this.search, this.page, this.size,
                 this.order, this.asc ).subscribe(
                  data => {

                    this.faenas = data.content;
                    this.isFirst = data.first;
                    this.isLast = data.last;
                    this.totalPages = new Array(data.totalPages);
                    console.log(data);
                    console.log(this.faenas);
                    // para cada Faena busco sus ingresos
                    this.faenas.forEach(faena => {
                      service.getIngresosByFaena(faena.id).subscribe (
                        dataIngresos => {
                          this.ingresosFaenados = [];
                          this.ingresosFaenados = dataIngresos;
                          console.log(dataIngresos);
                          // armo la faena incorporando sus ingresos
                          this.faenaCargado = {
                                    'id': faena.id,
                                    'fecha': faena.fecha,
                                    'cantidad': faena.cantidad,
                                    'numero': faena.numero,
                                    'operario': faena.operario,
                                    'tipoFaena': faena.tipoFaena,
                                    'listIngresos': this.ingresosFaenados,
                                  };
                          // agrego la faena a la lista de faenas que estoy mostrando
                         this.faenasCargados.push(this.faenaCargado);
                         // ejecuto sort para mostrarlos por fecha
                         this.faenasCargados.sort((a, b) => new Date(b.fecha).getTime() - new Date(a.fecha).getTime());
                       },
                      );
                    });
                   },
                 );

  }
  /////// Corto la carga de registros
   ngOnDestroy() {
     if (typeof this.getFaenasSubscribe !== 'undefined') {
       this.getFaenasSubscribe.unsubscribe();
     }
   }

  selectFaena(faena) {
    this.romaneosSelect = [];
    // envio el id de faena y me devuelve la lista de romaneos
    this.showSelect = false;
    // confirmo que haya alegido un ingreso de la faena seleccionada
    if (this.faenasListadas.ingresoElegido[faena.id] !== undefined) {
      // Obento los pesos totales antes de paginar
      this.service.getRomaneosIngresoPesoByIngreso(this.faenasListadas.ingresoElegido[faena.id]).subscribe (
        data => {
          this.faenasListadas.pesoTotal = data;
          this.service.getRomaneosIngresoCantByIngreso(this.faenasListadas.ingresoElegido[faena.id]).subscribe (
            cantidad => {
              this.faenasListadas.promedio = this.faenasListadas.pesoTotal / cantidad;
            },
          );
        },
      );
      // envio el id del ingreso elegido de la faena elegida para obenter los romaneos
      console.log(this.faenasListadas.ingresoElegido[faena.id]);
      this.ingresoId = this.faenasListadas.ingresoElegido[faena.id];
      this.service.getRomaneosFaenasPaginado(this.ingresoId, this.pageRomaneo, this.sizeRomaneo,
        this.orderRomaneo, this.ascRomaneo ).subscribe(
      // this.service.getRomaneosByIngreso(this.faenasListadas.ingresoElegido[faena.id]).subscribe(
        data => {
            console.log(data.content);
            this.faenasListadas.idIngreso = this.faenasListadas.ingresoElegido[faena.id];
            this.isFirstRomaneo = data.first;
            this.isLastRomaneo = data.last;
            this.totalPagesRomaneo = new Array(data.totalPages);
            this.romaneosSelect = data.content;
            console.log(this.romaneosSelect);
            // ejecuto sort para mostrar los gaarrones en orden
            this.romaneosSelect.sort((a, b) => 0 - (a.garron > b.garron ? -1 : 1));
            // limpio y asigno valor false a todos los checkbox de print printIndividual
            this.printIndividual.checkbox = [];
            for (let i = 0; i < this.romaneosSelect.length; i++) {
              console.log(i);
              this.printIndividual.checkbox[i] = false;
            }
            // limpio y Guardo en un arreglo los id de Garron con el mismo index que los checkbox
            // para pode envir el id del garron que quiero imprimir
            this.printIndividual.idRomaneo = [];
            let contadorRom = 0;
            this.romaneosSelect.forEach (romaneo => {
              this.printIndividual.idRomaneo[contadorRom] = romaneo.id;
              contadorRom = contadorRom + 1;
            });
            console.log(this.printIndividual);
         },
       );
    } else {
      this.dialogService.open(InvalidFormComponent, {
        context: {
          title: 'Debe elegir una Tropa de la Faena',
        },
        closeOnEsc: false,
        closeOnBackdropClick: true,
      });
    }
    this.romaneosSelect.sort((a, b) => 0 - (a.garron > b.garron ? -1 : 1));
  }


  // imprimo datos de un romaneo individual
  imprimirUno() {
    // vacio lista romaneos a imprimirRomaneo
    // recorro el arreglo de checkbo para ver cual está seleccionado
    // si no hay ninguno selccionado la bandera quedara en 0
    this.listaRomaneosImprimir = [];
    let bandera = 0;
    for (let i = 0; i < this.printIndividual.checkbox.length; i++) {
      if (this.printIndividual.checkbox[i]) {
        const romaneoImprimirId = this.printIndividual.idRomaneo[i];
        this.romaneosSelect.forEach( romaneo => {
          if (romaneoImprimirId === romaneo.id) {
            this.listaRomaneosImprimir.push(romaneo);
          }
        });
        console.log(this.printIndividual.checkbox[i]);
        console.log(this.printIndividual.idRomaneo[i]);
        console.log(this.listaRomaneosImprimir);
        bandera = 1;
      }
    }
    // si la bandera quedo en 0 no hay romaneo seleccionado, sino mando la listaRomaneosImprimir
    if (bandera === 0) {
      this.dialogService.open(InvalidFormComponent, {
        context: {
          title: 'Debe seleccionar por lo menos un garron',
        },
        closeOnEsc: false,
        closeOnBackdropClick: true,
      });
    } else {
       this.service.imprimirRomaneo(this.listaRomaneosImprimir).subscribe((response) => {
         const file = new Blob([response], { type: 'application/pdf' });
         const fileURL = URL.createObjectURL(file);
         window.open(fileURL);
       });
    }
  }

  // imprimo datos de un romaneo cuarteado (provisorio)
  imprimirCuarteo() {
    // vacio lista romaneos a imprimirRomaneo
    // recorro el arreglo de checkbo para ver cual está seleccionado
    // si no hay ninguno selccionado la bandera quedara en 0
    this.listaRomaneosImprimir = [];
    let bandera = 0;
    for (let i = 0; i < this.printIndividual.checkbox.length; i++) {
      if (this.printIndividual.checkbox[i]) {
        const romaneoImprimirId = this.printIndividual.idRomaneo[i];
        this.romaneosSelect.forEach( romaneo => {
          if (romaneoImprimirId === romaneo.id) {
            this.listaRomaneosImprimir.push(romaneo);
          }
        });
        console.log(this.printIndividual.checkbox[i]);
        console.log(this.printIndividual.idRomaneo[i]);
        console.log(this.listaRomaneosImprimir);
        bandera = 1;
      }
    }
    // si la bandera quedo en 0 no hay romaneo seleccionado, sino mando la listaRomaneosImprimir
    if (bandera === 0) {
      this.dialogService.open(InvalidFormComponent, {
        context: {
          title: 'Debe seleccionar por lo menos un garron',
        },
        closeOnEsc: false,
        closeOnBackdropClick: true,
      });
    } else {
       this.service.imprimirRomaneoCuarteo(this.listaRomaneosImprimir).subscribe((response) => {
         const file = new Blob([response], { type: 'application/pdf' });
         const fileURL = URL.createObjectURL(file);
         window.open(fileURL);
       });
    }
  }
  // imprimo datos de una tropa
  imprimir() {

     this.service.imprimirTropa(this.faenasListadas.idIngreso).subscribe((response) => {
       const file = new Blob([response], { type: 'application/pdf' });
       const fileURL = URL.createObjectURL(file);
       window.open(fileURL);
     });
     // const doc = new jsPDF('p' , 'pt', 'a4');
     // const specialElementHandlers = {
     //   '#editor': function (element, renderer) {
     //     return true;
     //   },
     // };
     // const print = this.aPrint.nativeElement;
     // doc.fromHTML(print.innerHTML, 15, 15, {
     //   'width': 522,
     //   'elementHandlers': specialElementHandlers ,
     // });
     // // agregar para que se imprima con el número de tropa
     // // doc.save('tropa' + this.faenasListadas.idIngreso + '.pdf');
     // doc.save('tropa.pdf');
  }

  btnSelectAll() {
    // asigno valor true o false a todos los checkbox de print printIndividual
    if (this.checkAll) {
      for (let i = 0; i < this.romaneosSelect.length; i++) {
        console.log(i);
        this.printIndividual.checkbox[i] = true;
      }
    } else {
      for (let i = 0; i < this.romaneosSelect.length; i++) {
        console.log(i);
        this.printIndividual.checkbox[i] = false;
      }
    }
  }


  exportarSenasa() {
    this.service.getRomaneosByIngreso(this.ingresoId).subscribe(
      data => {
        console.log(data);
        // vacio el contenido para armar uno nuevo
        this.fileCsv = [];
        // recorro el json para extraer los datos para armar el csv
        data.forEach( romaneo => {
          let especie: number = 0 ;
          let despecie: number = 0 ;
          let destino: number = 0 ;
          // busco el codigo de tipo de animal y agrego el correspondiente al codigo SENASA
          // En sistema SENASA tipo de animal es denominado especie
          // también asigno despecie que es las parte en lasque esta dividida el animal
          // en el matadero malargüe siempre va entero por eso asigno ese codigo
          // También asigno codigo de destino, esta dado por el tipo de animal y
          // el destino elegido
          if (romaneo.tropaDetalle.subtipoAnimal.tipoAnimal.id === 1 ) {
            especie = 2; // Ovino
            despecie = 32; // Ovino entero
            if (romaneo.ingreso.tropa.destino.id === 2 ) {
              destino = 106; // Ovino consumo inerno
            } else {
              destino = 105; // Ovino otros paises
            }
          } else if (romaneo.tropaDetalle.subtipoAnimal.tipoAnimal.id === 2 ) {
            especie = 3; // Porcinos
            despecie = 26; // Porcino entero
            destino = 106; // Porcino consumo inerno
          } else {
            especie = 4; // Caprinos
            despecie = 9; // Caprino entero
            if (romaneo.ingreso.tropa.destino.id === 2 ) {
              destino = 106; // Caprinos consumo inerno
            } else {
              destino = 105; // Caprinos otros paises
            }
          }
          // obtengo día mes y año de la fecha en el formato indicado
          const fecha = this.datePipe.transform( romaneo.fecha, 'dd/MM/yyyy'); //
          const year = this.datePipe.transform( romaneo.fecha, 'yyyy'); //

          // Recorro mis subtipos de animal para asiganar el codigo correspondiente de senasa
          // en sistema SENASA los subtipos son denominadas clasificacion
          let clasificacion = '' ;
          if (romaneo.tropaDetalle.subtipoAnimal.id === 1 ) {
            clasificacion = '04.03' ; // Cabrito
          } else if (romaneo.tropaDetalle.subtipoAnimal.id === 2 ) {
            clasificacion = '04.05'; // Cabrilla o chivito
          } else if (romaneo.tropaDetalle.subtipoAnimal.id === 3 ) {
            clasificacion = '04.02'; // Cabra o chiva
          } else if (romaneo.tropaDetalle.subtipoAnimal.id === 7 ) {
            clasificacion = '04.04'; // Capon
          } else if (romaneo.tropaDetalle.subtipoAnimal.id === 8 ) {
            clasificacion = '04.01'; // Chivo o chivato
          } else if (romaneo.tropaDetalle.subtipoAnimal.id === 9 ) {
            clasificacion = '02.05'; // Cordero
          } else if (romaneo.tropaDetalle.subtipoAnimal.id === 10 ) {
            clasificacion = '02.03'; // Borrego
          } else if (romaneo.tropaDetalle.subtipoAnimal.id === 11 ) {
            clasificacion = '02.04'; // Capon ovino
          } else if (romaneo.tropaDetalle.subtipoAnimal.id === 12 ) {
            clasificacion = '02.02'; // Oveja
          } else {
            clasificacion = '02.01'; // Carnero
          }
          // Armo una fila por cada garron cargando los datos que pide SENASA
          const itemCsv = {
            tropa : romaneo.ingreso.tropa.numero ,
            especie: especie ,
            fecha: fecha ,
            periodo: year ,
            clasificacion: clasificacion ,
            despecie: despecie ,
            garron: romaneo.garron ,
            kilos: romaneo.peso ,
            camara: 1 , // es el codigo de camara cargado en senasa
            destino: destino ,
            tipificador: '', // dejo vacio porque son datos que no tenemos y no son obligatorios
            tipificacion: '', // dejo vacio porque son datos que no tenemos y no son obligatorios
            contusion: '', // dejo vacio porque son datos que no tenemos y no son obligatorios
          };
          this.fileCsv.push(itemCsv);
          console.log(this.fileCsv);
        });
        new Angular2Csv(this.fileCsv, 'FaenaSenasa' , this.options);
      },
    );
  }

  // Funcion para mostrar Toastear
  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  // Borra Ingreso de la listar
  borrarFaena(faena) {

    console.log(faena);
    if (window.confirm('Estás seguro que quieres borrar la faena?')) {
      const idFaena: number = faena.id;
      this.service.deleteFaena(idFaena).subscribe(datas => {
        console.log('se borro la faena');
        console.log(datas);
      });
    } else {
      return;
    }
  }

  /////////////////////////////////////
 buscar() {
   this.faenas = [];
   this.faenasCargados = [];

   // Obtengo las faenas
  this.getFaenasSubscribe = this.service.getFaenasPaginado(this.search, this.page, this.size,
    this.order, this.asc ).subscribe(
     data => {

       this.faenas = data.content;
       this.isFirst = data.first;
       this.isLast = data.last;
       this.totalPages = new Array(data.totalPages);
       console.log(data);
       console.log(this.faenas);
       // para cada Faena busco sus ingresos
       this.faenas.forEach(faena => {
         this.service.getIngresosByFaena(faena.id).subscribe (
           dataIngresos => {
             this.ingresosFaenados = [];
             this.ingresosFaenados = dataIngresos;
             console.log(dataIngresos);
             // argo la faena incorporando sus ingresos
             this.faenaCargado = {
                       'id': faena.id,
                       'fecha': faena.fecha,
                       'cantidad': faena.cantidad,
                       'numero': faena.numero,
                       'operario': faena.operario,
                       'tipoFaena': faena.tipoFaena,
                       'listIngresos': this.ingresosFaenados,
                     };
             // agrego la faena a la lista de faenas que estoy mostrando
            this.faenasCargados.push(this.faenaCargado);
            // ejecuto sort para mostrarlos por fecha
            this.faenasCargados.sort((a, b) => new Date(b.fecha).getTime() - new Date(a.fecha).getTime());
          },
         );
       });
      },
    );
 }
 ////////////////////////////////////

 //////////////// buscar de romaneos por faena /////////////////////
buscarRomaneos() {
  this.romaneosSelect = [];
  // envio el id de faena y me devuelve la lista de romaneos
  this.showSelect = false;
    // envio el id del ingreso elegido de la faena elegida para obenter los romaneos
    console.log(this.faenasListadas.idIngreso);
    this.service.getRomaneosFaenasPaginado(this.ingresoId, this.pageRomaneo, this.sizeRomaneo,
      this.orderRomaneo, this.ascRomaneo ).subscribe  (
    // this.service.getRomaneosByIngreso(this.faenasListadas.ingresoElegido[faena.id]).subscribe(
      data => {
          console.log(data.content);
          this.romaneosSelect = data.content;
          console.log(this.romaneosSelect);
          // ejecuto sort para mostrar los gaarrones en orden
          this.romaneosSelect.sort((a, b) => 0 - (a.garron > b.garron ? -1 : 1));
          // limpio y asigno valor false a todos los checkbox de print printIndividual
          this.printIndividual.checkbox = [];
          for (let i = 0; i < this.romaneosSelect.length; i++) {
            console.log(i);
            this.printIndividual.checkbox[i] = false;
          }
          // limpio y Guardo en un arreglo los id de Garron con el mismo index que los checkbox
          // para pode envir el id del garron que quiero imprimir
          this.printIndividual.idRomaneo = [];
          let contadorRom = 0;
          this.romaneosSelect.forEach (romaneo => {
            this.printIndividual.idRomaneo[contadorRom] = romaneo.id;
            contadorRom = contadorRom + 1;
          });
          console.log(this.printIndividual);
       },
     );
  this.romaneosSelect.sort((a, b) => 0 - (a.garron > b.garron ? -1 : 1));
}

  /////////////// Paginacion Ingresos //////////////////////////
  sort(): void {
    this.asc = !this.asc;
    this.buscar();
  }

  rewind(): void {
    if (!this.isFirst) {
      this.page--;
      this.buscar();
    }
  }

  forward(): void {
    if (!this.isLast) {
      this.page++;
      this.buscar();
    }
  }

  setPage(page: number): void {
    this.page = page;
    this.buscar();
  }

  setOrder(order: string): void {
    this.order = order;
    this.buscar();
  }
  //////////////////// Fin Paginacion faenas ////////////////////////////

  /////////////// Paginacion Romaneos de Faena //////////////////////////
  sortRomaneos(): void {
    this.ascRomaneo = !this.asc;
    this.buscarRomaneos();
  }

  rewindRomaneos(): void {
    if (!this.isFirst) {
      this.pageRomaneo--;
      this.buscarRomaneos();
    }
  }

  forwardRomaneos(): void {
    if (!this.isLast) {
      this.pageRomaneo++;
      this.buscarRomaneos();
    }
  }

  setPageRomaneos(page: number): void {
    this.pageRomaneo = page;
    this.buscarRomaneos();
  }

  setOrderRomaneos(order: string): void {
    this.orderRomaneo = order;
    this.buscarRomaneos();
  }
  //////////////////// Fin Paginacion faenas ////////////////////////////
}
// PAGINAR LA MUESTRA DE ROMANEOS
