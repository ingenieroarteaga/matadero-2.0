import {Operario} from '../../personas/operarios/operario';
import {Ingreso} from '../ingresos/ingreso';

export class Faena {
    public id: number;
    public fecha: Date;
    public cantidad: number;
    public numero: number;
    public operario: Operario;
    public tipoFaena: any;
    public listIngresos: Ingreso [];
}
