import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { OperacionesRoutingModule, routedComponents } from './operaciones-routing.module';
import { SmartTableService } from '../../@core/data/smart-table.service';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { IngresosComponent } from './ingresos/ingresos.component';
import { ListasMatanzaComponent } from './listas-matanza/listas-matanza.component';
import { RomaneosComponent } from './romaneos/romaneos.component';
import { MovimientosCamaraComponent } from './movimientos-camara/movimientos-camara.component';
import { FaenasComponent } from './faenas/faenas.component';
import { RangosTropasComponent } from './rangos-tropas/rangos-tropas.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    OperacionesRoutingModule,
    Ng2SmartTableModule,
    AutocompleteModule.forRoot(),
    Ng2AutoCompleteModule,
    NgSelectModule,
    FormsModule,

  ],
  declarations: [
    ...routedComponents,
    IngresosComponent,
    RangosTropasComponent,
    ListasMatanzaComponent,
    RomaneosComponent,
    MovimientosCamaraComponent,
    FaenasComponent,
  ],
  providers: [
    SmartTableService,
  ],
})
export class OperacionesModule { }
