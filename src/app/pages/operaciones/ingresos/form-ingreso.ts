// Lo utilizo para armar los objetos Ingreso y Tropa que tengo que enviar
// a partir del formulario
// import {Matarife} from '../../personas/matarifes/matarife';
// import {Productor} from '../../personas/productores/productor';
// import {Transportista} from '../../personas/transportistas/transportista';
// import {Corralero} from '../../personas/corraleros/corralero';
// import {Corral} from '../../herramientas/corrales/corral';
// import {TipoAnimal} from '../../clasificadores/tipos-animales/tipo-animal';
// import {SubTipoAnimal} from '../../clasificadores/subtipos-animales/subtipo-animal';

export class FormIngreso {
    public fechaIngreso: Date;
    public libroNumero: string;
    public libroFolio: string;
    public guia: string;
    public fechaGuia: Date;
    public fechaDte: Date;
    public numeroDte: string;
    public matarife: string;
    public productor: string;
    public procedencia: any;
    public destino: any;
    public transportista: string;
    public transporte: string;
    public corralero: string;
    public corral: string;
    public numeroTropa: number;
    public tipoAnimal: string;
    public subTipoAnimal: string;
    // no debería ir public ruca: any;
    public cantidad: number;
    public peso: number;
    public cantidadMuertos: number;
    public pesoMuertos: number;
    public observaciones: string;
}
