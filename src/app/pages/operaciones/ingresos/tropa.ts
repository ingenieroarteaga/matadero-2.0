import {Matarife} from '../../personas/matarifes/matarife';
import {Transportista} from '../../personas/transportistas/transportista';
import {Transporte} from '../../herramientas/transportes/transporte';
import {Productor} from '../../personas/productores/productor';
import {TipoDestino} from '../../clasificadores/tipos-destinos/tipo-destino';
import {TropaDetalle} from './tropa-detalle';


export class Tropa {
    public id: number;
    public numero: number;
    public guia: string;
    public fechaGuia: Date;
    public dte: string;
    public fechaDte: Date;
    public cantidad: number; // Cantidad total de animales vivos
    public peso: number; // Peso total de animales vivos
    public cantidadMuerto: number; // Cantidad total de animales muertos
    public pesoMuerto: number; // Peso total de animales muertos
    public libroNumero: string;
    public libroFolio: string;
    public cliente: Matarife;
    public chofer: Transportista;
    public transporte: Transporte;
    public productor: Productor;
    public destino: TipoDestino;
    public procedencia: any;
    public tropaDetalleList: TropaDetalle[];
    // public procedencia: Distrito;

  }


// ruca id siglas (string), cant_ovino, cant_caprino, desde, hasta, cliente id,
// destino id y descripcion
