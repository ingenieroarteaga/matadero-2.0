import {Corralero} from '../../personas/corraleros/corralero';
import {Corral} from '../../herramientas/corrales/corral';
import {Tropa} from './tropa';
import {ListaMatanza} from '../listas-matanza/lista-matanza';

export class Ingreso {
    public id: number;
    public fecha: Date;
    public numero: number;
    public cantidad: number;
    public fechaEgreso: Date;
    public corral: Corral;
    public corralero: Corralero;
    public matanza: ListaMatanza;
    public tropa: Tropa;
    public romaneoList: any;
    public observaciones: string;
}


// [16:16, 2/3/2019] Chaca Guevara: romaneo list y fecha egreso van nulos
// [16:16, 2/3/2019] Chaca Guevara: matanza tb

