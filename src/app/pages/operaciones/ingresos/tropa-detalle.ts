import {Tropa} from './tropa';
import {SubTipoAnimal} from '../../clasificadores/subtipos-animales/subtipo-animal';
export class TropaDetalle {
    public id: number;
    public fecha: Date;
    public cantidad: number;
    public cantidadMuerto: number;
    public cantidadDecomizado: number;
    public estadoAnimalId: number;
    public subtipoAnimal: SubTipoAnimal;
    public tropa: Tropa;
}
