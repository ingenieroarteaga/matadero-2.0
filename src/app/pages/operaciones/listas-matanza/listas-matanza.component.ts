import { Component, OnDestroy } from '@angular/core';
// Necesarioas para el mensaje toaster
import { ToasterConfig } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbDialogService } from '@nebular/theme';
import { InvalidFormComponent } from '../../personas/matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { UserService } from '../../../@core/data/users.service';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { ListaMatanza } from './lista-matanza';
import { Ingreso } from '../ingresos/ingreso';
import { Administrativo } from '../../personas/administrativos/administrativo';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ngx-listas-matanza',
  templateUrl: './listas-matanza.component.html',
  styleUrls: ['./listas-matanza.component.scss'],
})
export class ListasMatanzaComponent implements OnDestroy {
  // Para guardad el usuarios
  userNick: string = 'rgomez';
  // Para listar las listas de matanza
  listasMatanza: any = [];

  // Para listar los Romaneos
  romaneos: any[];

  // Para mostrar los ingresos sin matanza
  listIngresos: Ingreso[] = [];
  // Para insertar el ingreso en la lista;
  ingresoElegido: Ingreso = new Ingreso();
  // Para guardar los ingresos elegidos
  ingresosElegidos: Ingreso[] = [];
  // para crear la lista de Matanza
  listaMatanza: ListaMatanza = new ListaMatanza();
  // confirmar lista de matanza btn
  btnConfirmar: boolean = true;
  btnConfirmarShow: boolean = false;

  // Adminisrtativos para asignar a la lista
  administrativos: any [];
  // Libro y folio para asignar a lista
  libroNro: number = 0;
  libroFolio: string;
  // Datos para mostrar notificaciones
  config: ToasterConfig;
  index = 1;
  destroyByClick = true;
  duration = 3500;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus;
  titleToast: string;
  contentToat: string;
  tituloMatanza: string = 'Nueva Lista de Matanza';
  // Para Editar listas
  // Para guardar la lista a editar
  listaEdit: ListaMatanza = new ListaMatanza();
  // confirmar lista de matanza btn
  btnEditar: boolean = true;
  btnEditarShow: boolean = true;
  btnCancelarShow: boolean = true;
  // para cortar el servicio cuando cambio da pantalla
  getMatanzasSubscribe: Subscription;
  /////////// Paginacion
  ///////// Buscar
 search: any = {};
 totalPages: Array<number>;

 page = 0;
 size = 4;
 order = 'id';
 asc = false;

 isFirst = false;
 isLast = false;



  constructor(public service: SmartTableService,
                private toastrService: NbToastrService,
                private dialogService: NbDialogService,
                private userService: UserService,
                ) {
                  // obtengo el user logueado
                  this.userNick = this.userService.getUserNick();
                  console.log(this.userNick);

                  // Obtengo las lista de matanza
                  this.getMatanzasSubscribe = service.getMatanzasPaginado(this.search, this.page, this.size,
                    this.order, this.asc ).subscribe(
                     data => {
                       this.listasMatanza = data.content;
                       this.isFirst = data.first;
                       this.isLast = data.last;
                       this.totalPages = new Array(data.totalPages);
                       console.log(data);
                      },
                    );

                    // Obtengo los ingresos sin matanza
                  this.service.getIngresosSinMatanza().subscribe(
                    data => {
                        this.listIngresos = data;
                        console.log(this.listIngresos);
                        console.log('Se hizo el traspaso de ingresos');
                      },
                    );

                    // Obtengo los administrativo
                  this.service.getAdministrativos().subscribe(
                    data => {
                        this.administrativos = data;
                        console.log(this.administrativos);
                        console.log('Se hizo el traspaso de administrativos');
                      },
                    );
    }
    /////// Corto la carga de registros
     ngOnDestroy() {
       if (typeof this.getMatanzasSubscribe !== 'undefined') {
         this.getMatanzasSubscribe.unsubscribe();
       }
     }
    // para sacar ingreso de la lista
    remove(tropa , id) {

      console.log(tropa);
      this.ingresoElegido = tropa;
      console.log(this.ingresoElegido);
      this.listIngresos.push(this.ingresoElegido);
      this.ingresosElegidos.splice(id, 1);
      console.log(this.ingresosElegidos);
      if (this.ingresosElegidos.length === 0) {
        this.btnConfirmar = true;
      }

    }
    // para agregar ingreso a la lista
    add(tropa, id) {
      console.log(tropa);
      this.ingresoElegido = tropa;
      console.log(this.ingresoElegido);
      this.ingresosElegidos.push(this.ingresoElegido);
      this.listIngresos.splice(id, 1);
      console.log(this.listIngresos);
      this.btnConfirmar = false;
      this.btnEditar = false;
      // if (this.ingresosElegidos.length < 1) {
      //   console.log(tropa);
      //   this.ingresoElegido = tropa;
      //   console.log(this.ingresoElegido);
      //   this.ingresosElegidos.push(this.ingresoElegido);
      //   this.listIngresos.splice(id, 1);
      //   console.log(this.listIngresos);
      //   this.btnConfirmar = false;
      //
      // } else {
      //   // Deberia recorrer ingresosElegidos y compar con tropa el id de tipoAnimal
      //   // Si el id de tipoAnimal es igual agrega el ingreso, sino avisar que no se puede mezclar
      // }
    }

    // Cargo la lista de matanza
    confirmar() {
      // Valido que haya cargado un Libro Nro
      if (this.libroNro !== 0) {
        // armo el objeto listaMatanza para enviar
        this.listaMatanza = {'id': null,
                             'administrativo': this.listaMatanza.administrativo = new Administrativo(),
                             'numero': this.libroNro,
                             'ingresoList': this.listaMatanza.ingresoList = [] ,
                             'fecha': this.listaMatanza.fecha = new Date(),
                             'observaciones': 'observaciones',
                             'faena': null,
                           };
        // asigno administrativo
        // this.listaMatanza.administrativo = this.administrativos.find(administrativo =>
        //                                    administrativo.id === 1 );
        // PARA GUARDA LISTA CON EL USUARIO LOGUEADO, EL USUARIO USER ADMIN NO EXISTE
        // HAY QUE CORREGIR PORQUE SINO GUARDO SIEMPRE CON EL ID 1
        this.listaMatanza.administrativo = this.administrativos.find(administrativo =>
                                           administrativo.usuario.username === this.userNick);

        // asigno lista de ingresos
        // this.ingresosElegidos.forEach(ingreso => {
        //     this.listaMatanza.ingresoList.push(ingreso);
        // });
        console.log(this.listaMatanza);
        this.service.createMatanza(this.listaMatanza).subscribe(
        data => {
          this.showToast(this.status, this.titleToast, this.contentToat);
          // Asigno lista de matanza a cada ingreso de la lista de matanza
          let i = 0;
          this.ingresosElegidos.forEach(ingreso => {
            ingreso.matanza = data;
            this.service.updateIngreso(ingreso).subscribe(
            info => {
              console.log('id matanza asignado', info );
              // Si el recorrido está completo, hago refresh de la lista de matanzas
              i = i + 1;
              if (this.ingresosElegidos.length === i) {
                this.refreshListas ();
              }
            });
          });
          console.log('Se cargo la lista de matanza', data);
          this.status = NbToastStatus.SUCCESS;
          this.titleToast = 'Lista de Matanza creada';
          // cuando falle
          // this.status =  NbToastStatus.WARNING;
          // this.titleToast =  'No se pudo cargar Lista de Matanza'
          },
        );
      } else {
        this.dialogService.open(InvalidFormComponent, {
          context: {
            title: 'Debe ingresar un Libro Nro',
          },
          closeOnEsc: false,
          closeOnBackdropClick: false,
        });
      }
    }


    // Editar ingreso
    editMatanza(lista) {
      this.listaEdit = lista;
      this.ingresosElegidos = [];
      // Los ingresos ya faenados no se permite editar, solo los que no tienen faena
        lista.ingresoList.forEach(ingreso => {
        // envio id de ingreso para ver si tiene romaneo
        this.service.getRomaneosByIngresoCount(ingreso.id).subscribe(
          data => {
               if (data >= 1) {
                   console.log( ingreso.tropa.numero , 'ingreso faenado');
               } else {
                 this.ingresosElegidos.push(ingreso);
               }
           },
         );
      });
      this.libroNro = lista.numero;
      // this.libroFolio = lista.libroFolio;
      // cambio el titulo del formulario
      this.tituloMatanza = 'Editar lista de Matanza';
      // muestro los botones de editar y oculta el de crear
      this.btnEditarShow = false;
      this.btnCancelarShow = false;
      this.btnConfirmarShow = true;
      this.btnEditar = false;
    }
    // Si confirmo la edición
     editar() {
       console.log(this.listaEdit);
       // Valido que haya cargado un Libro Nro
       if (this.libroNro !== 0) {
         // armo el objeto listaMatanza para enviar
         this.listaMatanza = {'id': this.listaEdit.id ,
                              'administrativo': this.listaEdit.administrativo,
                              'numero': this.libroNro,
                              'ingresoList': this.listaMatanza.ingresoList = [] ,
                              'fecha': this.listaEdit.fecha,
                              'observaciones': this.listaEdit.observaciones,
                              'faena': this.listaEdit.faena,
                            };
         console.log(this.listaMatanza);
         this.service.updateMatanza(this.listaMatanza).subscribe(
         data => {
             console.log(this.ingresosElegidos);
             console.log(this.listaEdit);
             // Comparo con la lista que edite, si borre algun ingreso
             // marco como null el campo ingreso.matanza
             // la variable data me la devuelve el dialogService tiene todavia
             // todos los ingresos asignados
             data.ingresoList.forEach (ingresoEdit => {
               if (this.ingresosElegidos.find(ingresoElegido =>
                                         ingresoElegido.id === ingresoEdit.id)) {
                 console.log('el ingreso sigue estando');
               } else {
                 // compruebo si no está porque ya está faenado
                 this.service.getRomaneosByIngreso(ingresoEdit.id).subscribe(
                   ingresoVerificado => {
                        if (ingresoVerificado.length >= 1) {
                            console.log( 'El ingreso no está porque fue faenado');
                        } else {
                          ingresoEdit.matanza = null;
                          this.service.updateIngreso(ingresoEdit).subscribe(
                          info => {
                            console.log('ingreso retirado de la lista de matanza', info );
                          });
                        }
                    },
                  );
               }
             });
             // Asigno lista de matanza a cada ingreso de la lista de matanza
             let i = 0;
             this.ingresosElegidos.forEach(ingreso => {
               data.ingresoList = null;
               ingreso.matanza = data;
               this.service.updateIngreso(ingreso).subscribe(
               info => {
                 console.log('matanza asiganada a ingreso', info );
                 // Si el recorrido está completo, hago refresh de la lista de matanzas
                 i = i + 1;
                 if (this.ingresosElegidos.length === i) {
                   this.refreshListas ();
                 }
               });
             });
             console.log('Se edito la lista de matanza', data);
             this.status = NbToastStatus.SUCCESS;
             this.titleToast = 'Lista de Matanza Editada';
             // cuando falle
             // this.status =  NbToastStatus.WARNING;
             // this.titleToast =  'No se pudo cargar Lista de Matanza'
             this.showToast(this.status, this.titleToast, this.contentToat);
           },
         );
       } else {
         this.dialogService.open(InvalidFormComponent, {
           context: {
             title: 'Debe ingresar un Libro Nro',
           },
           closeOnEsc: false,
           closeOnBackdropClick: false,
         });
       }
     }
    // Si cancelo la Edición
    cancelar() {

      this.refreshListas();
      // this.listaEdit = new ListaMatanza ();
      // this.ingresosElegidos = [];
      // this.libroNro = 0;
      // this.tituloMatanza = 'Nueva Lista de Matanza';
      // // oculto los botones de editar y muestro el de crear
      // this.btnEditarShow = true;
      // this.btnCancelarShow = true;
      // this.btnConfirmarShow = false;
      // this.btnEditar = true;
    }

    // Borra listas
    eliminarList(lista) {
      // Necesito evitar que borren listas que tengas ingresos cargados
      // antes de permitirles eliminar
      // this.dialogService.open(InvalidFormComponent, {
      //   context: {
      //     title: 'La función eliminar está deshabilitada',
      //   },
      //   closeOnEsc: false,
      //   closeOnBackdropClick: true,
      // });
      console.log(lista);
      if (window.confirm('Estás seguro que quieres borrar la lista?')) {
        const idLista: number = lista.id;
        this.service.deleteMatanza(idLista).subscribe(datas => {
          console.log('se borro la lista');
          this.refreshListas ();
        });
      } else {
        return;
      }
    }

    // Actualiza la lista de listas de matanzas
    // Actualizado la lsita de ingresos disponibles
    refreshListas () {
      this.listasMatanza = [];
      this.service.getMatanzasPaginado(this.search, this.page, this.size,
        this.order, this.asc ).subscribe(
         data => {
           this.listasMatanza = data.content;
           this.isFirst = data.first;
           this.isLast = data.last;
           this.totalPages = new Array(data.totalPages);
           console.log(data);
           // Obtengo los ingresos sin matanza
         this.service.getIngresosSinMatanza().subscribe(
           dataIngresos => {
               this.listIngresos = dataIngresos;
               console.log(this.listIngresos);
               console.log('Se hizo el traspaso de ingresos');
             },
           );
          },
      );
      this.listIngresos = [];
        // deshabilito el boton de confirmar
        this.btnConfirmar = true;
        // limpio la lista de ingresos ingresosElegidos
        this.ingresosElegidos = [];
        this.libroNro = 0;
        this.listaEdit = new ListaMatanza ();
        this.tituloMatanza = 'Nueva Lista de Matanza';
        // oculto los botones de editar y muestro el de crear
        this.btnEditarShow = true;
        this.btnCancelarShow = true;
        this.btnConfirmarShow = false;
        this.btnEditar = true;
    }
    // Funciones para mostrar Toastear
    private showToast(type: NbToastStatus, title: string, body: string) {
      const config = {
        status: type,
        destroyByClick: this.destroyByClick,
        duration: this.duration,
        hasIcon: this.hasIcon,
        position: this.position,
        preventDuplicates: this.preventDuplicates,
      };
      const titleContent = title ? `${title}` : '';

      this.index += 1;
      this.toastrService.show(
        body,
        `${titleContent}`,
        config);
    }

    /////////////////////////////////////
   buscar() {
     this.listasMatanza = [];

     const v_search: any = {};
     v_search.numero = this.search.numero;

     if (typeof this.search.desde !== 'undefined' && this.search.desde !== null) {
       v_search.desde = this.search.desde;
     } else {
       v_search.desde = null;
     }
     if (typeof this.search.hasta !== 'undefined' && this.search.hasta !== null) {
       v_search.hasta = this.search.hasta;
     } else {
       v_search.hasta = null;
     }

     if (typeof this.search.cliente !== 'undefined' && this.search.cliente !== null) {
       v_search.cliente = this.search.cliente.id;
     } else {
       v_search.cliente = null;
     }

     if (typeof this.search.sucursal !== 'undefined' && this.search.sucursal !== null) {
       v_search.sucursal = this.search.sucursal.id;
     } else {
       v_search.sucursal = null;
     }

     if (typeof this.search.puntoVenta !== 'undefined' && this.search.puntoVenta !== null) {
       v_search.puntoVenta = this.search.puntoVenta.id;
     } else {
       v_search.puntoVenta = null;
     }

     if (typeof this.search.tipoFactura !== 'undefined' && this.search.tipoFactura !== null) {
       v_search.tipoFactura = this.search.tipoFactura.id;
     } else {
       v_search.tipoFactura = null;
     }

     this.service.getMatanzasPaginado(
       v_search, this.page, this.size, this.order, this.asc).subscribe(
         data => {
           this.listasMatanza = data.content;

           this.isFirst = data.first;
           this.isLast = data.last;
           this.totalPages = new Array(data.totalPages);

         },
         err => {
           console.log(err.error);
         },
       );
   }
   ////////////////////////////////////

    /////////////// Paginacion //////////////////////////
    sort(): void {
      this.asc = !this.asc;
      this.buscar();
    }

    rewind(): void {
      if (!this.isFirst) {
        this.page--;
        this.buscar();
      }
    }

    forward(): void {
      if (!this.isLast) {
        this.page++;
        this.buscar();
      }
    }

    setPage(page: number): void {
      this.page = page;
      this.buscar();
    }

    setOrder(order: string): void {
      this.order = order;
      this.buscar();
    }
    //////////////////// Fin Paginacion ////////////////////////////

}
// 1- agregar aviso cuando vamos a editer una ya creada
