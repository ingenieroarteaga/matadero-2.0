import { Ingreso } from '../ingresos/ingreso';
import { Administrativo } from '../../personas/administrativos/administrativo' ;
import { Faena } from '../romaneos/faena';

export class ListaMatanza {
        public id: number;
        public administrativo: Administrativo;
        public numero: number;
        public ingresoList: Ingreso [];
        public fecha: Date;
        public observaciones: string;
        public faena: Faena;
}
