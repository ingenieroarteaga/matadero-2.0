import { TipoAnimal } from '../../clasificadores/tipos-animales/tipo-animal';
import { Matarife } from '../../personas/matarifes/matarife' ;


export class RangoTropa {
        public id: number;
        public desde: Date;
        public hasta: Date;
        public inicio: number;
        public fin: number;
        public numero: number;
        public tipoAnimal: TipoAnimal;
        public cliente: Matarife;
}
