import { Component } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
// import { AgregarMatarifeComponent } from './agregar-matarife/agregar-matarife.component';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import {Matarife} from '../../personas/matarifes/matarife';
import {TipoAnimal} from '../../clasificadores/tipos-animales/tipo-animal';
import {RangoTropa} from './rango-tropa';

@Component({
  selector: 'ngx-rangos-tropas',
  templateUrl: './rangos-tropas.component.html',
  styleUrls: ['./rangos-tropas.component.scss'],
})
export class RangosTropasComponent {

  // Cargo los matarifes
  matarifesCargador;
  matarifes: Matarife[];
  matarifeCargador: Matarife = new Matarife();

  // Cargo los tipo de animales
  tiposAnimalesCargador;
  tiposAnimales: TipoAnimal[];
  tipoAnimalCargador: TipoAnimal = new TipoAnimal();

  // Trabajar con RangoTropa
  public rangoTropa: RangoTropa = new RangoTropa();

  // Cargar rango-tropas en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      cliente: {
        title: 'Cliente',
        type: 'text',
        // valuePrepareFunction: (data) => {
        //                            return data.persona['nombre'];
        //                        },
        editor: {
        type: 'list',
          config: {
          list: this.matarifesCargador,
          },
        },
      },
      inicio: {
        title: 'Inicio',
        type: 'number',
      },
      fin: {
        title: 'Final',
        type: 'number',
      },
      numero: {
        title: 'Ultimo número',
        type: 'number',
      },
      tipoAnimal: {
        title: 'Tipo De Animal',
        type: 'text',
        valuePrepareFunction: (data) => {
                                   return data['descripcion'];
                               },
        editor: {
        type: 'list',
          config: {
          list: this.tiposAnimalesCargador,
          },
        },
      },
    },
  };

  constructor(public service: SmartTableService,
              private dialogService: NbDialogService,
              ) {

              //  Obtengo los Matarifes
              this.service.getMatarifes().subscribe(
                  data => {
                    this.matarifes = data;
                    this.matarifesCargador = data.map( item => {
                    return { title: item.persona.nombre , value : item.id};
                    });
                    // asigno a la variable setting los paises que debe mostrar en editor
                    this.settings.columns.cliente.editor.config.list = this.matarifesCargador;
                    this.settings = Object.assign({}, this.settings);
                    console.log(this.matarifesCargador);
                    console.log(this.matarifes);
                    },
                  );

                  // Obtengo los Tipo de Animal
                this.service.getTiposAnimales().subscribe(
                    data => {
                      this.tiposAnimales = data;
                      this.tiposAnimalesCargador = data.map( item => {
                      return { title: item.descripcion , value : item.id };
                      });
                      // asigno a la variable setting los paises que debe mostrar en editor
                      this.settings.columns.tipoAnimal.editor.config.list = this.tiposAnimalesCargador;
                      this.settings = Object.assign({}, this.settings);
                      console.log(this.tiposAnimalesCargador);
                      console.log(this.tiposAnimales);
                      },
                    );

                    // Obtengo los Rangos
                  this.service.getRangosTropas().subscribe(
                    data => {
                      const info = data.map( item => {
                      return { id: item.id , inicio : item.inicio , fin: item.fin , numero : item.numero ,
                               tipoAnimal: item.tipoAnimal , cliente : item.cliente.persona.nombre +
                               item.cliente.persona.apellido,
                               };
                      });
                        this.source = info;
                        console.log('Se hizo el traspaso de rangos', this.source);
                      },
                    );


  }

  // Agregar rango
  onCreateConfirm(event) {
    this.rangoTropa = {'id': null,
                  'desde': this.rangoTropa.desde = new Date(),
                  'hasta' : null,
                  'inicio': event.newData.inicio,
                  'fin': event.newData.fin,
                  'numero' : event.newData.numero,
                  'tipoAnimal': this.rangoTropa.tipoAnimal = new TipoAnimal(),
                  'cliente': this.rangoTropa.cliente = new Matarife(),
                  };

      this.rangoTropa.tipoAnimal =  this.tiposAnimales.find(tipoAnimal => tipoAnimal.id ==
                                    event.newData.tipoAnimal);
      this.rangoTropa.cliente =  this.matarifes.find(matarife => matarife.id ==
                                    event.newData.cliente);

      console.log(this.rangoTropa);
      this.service.createRangoTropa(this.rangoTropa).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo un rango', datas);
        this.refreshTable();
      });
  }

  // Borrar rango-tropa
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const rangoTropa: number = event.data.id;
      this.service.deleteRangoTropa(rangoTropa).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el rango-tropa', datas);
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar rago tropa
  onSaveConfirm(event) {
    this.rangoTropa = {'id': event.newData.id,
                  'desde': this.rangoTropa.desde = new Date(),
                  'hasta' : null,
                  'inicio': event.newData.inicio,
                  'fin': event.newData.fin,
                  'numero' : event.newData.numero,
                  'tipoAnimal': this.rangoTropa.tipoAnimal = new TipoAnimal(),
                  'cliente': this.rangoTropa.cliente = new Matarife(),
                  };

      this.rangoTropa.tipoAnimal =  this.tiposAnimales.find(tipoAnimal => tipoAnimal.id ==
                                    event.newData.tipoAnimal);
      this.rangoTropa.cliente =  this.matarifes.find(matarife => matarife.id ==
                                event.newData.cliente);
      console.log(this.rangoTropa);
      this.service.updateRangoTropa(this.rangoTropa).subscribe(datas => {
        this.refreshTable();
        event.confirm.resolve();
        console.log('se edito el rango de tropa', datas);
    });
  }

  // funcion para actulizar cambios
  refreshTable() {
    // Obtengo los Rangos
  this.service.getRangosTropas().subscribe(
    data => {
      const info = data.map( item => {
      return { id: item.id , inicio : item.inicio , fin: item.fin , numero : item.numero ,
               tipoAnimal: item.tipoAnimal , cliente : item.cliente.persona.nombre +
               item.cliente.persona.apellido,
               };
      });
        this.source = info;
        console.log('Se hizo el traspaso de rangos', this.source);
      },
    );
    return   console.log('Se hizo el refresh de administrativos');
  }

}
