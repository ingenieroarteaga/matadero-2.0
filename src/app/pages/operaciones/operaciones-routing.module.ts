import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OperacionesComponent } from './operaciones.component';

import { IngresosComponent } from './ingresos/ingresos.component';
import { ListasMatanzaComponent } from './listas-matanza/listas-matanza.component';
import { RangosTropasComponent } from './rangos-tropas/rangos-tropas.component';
import { RomaneosComponent } from './romaneos/romaneos.component';
import { MovimientosCamaraComponent } from './movimientos-camara/movimientos-camara.component';
import { FaenasComponent } from './faenas/faenas.component';

const routes: Routes = [{
  path: '',
  component: OperacionesComponent,
  children: [ {
      path: 'ingresos',
      component: IngresosComponent,
      }, {
      path: 'listas-matanza',
      component: ListasMatanzaComponent,
      }, {
      path: 'romaneos',
      component: RomaneosComponent,
      },
      {
      path: 'movimientos-camara',
      component: MovimientosCamaraComponent,
      },
      {
      path: 'faenas',
      component: FaenasComponent,
      },
      {
      path: 'rangos-tropas',
      component: RangosTropasComponent,
      },
    ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OperacionesRoutingModule { }
export const routedComponents = [
    OperacionesComponent,
    IngresosComponent,
    RangosTropasComponent,
    ListasMatanzaComponent,
    RomaneosComponent,
    MovimientosCamaraComponent,
    FaenasComponent,
];
