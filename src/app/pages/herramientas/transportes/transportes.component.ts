import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import { Transporte } from './transporte';

@Component({
  selector: 'ngx-transportes',
  templateUrl: './transportes.component.html',
  styleUrls: ['./transportes.component.scss'],
})

export class TransportesComponent {

  // Add transporte
  public transporte: Transporte = new Transporte();

  // Cargar transporte en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      dominio: {
        title: 'Dominio',
        type: 'string',
      },
      cenasa: {
        title: 'Cenasa',
        type: 'string',
      },
      capacidad: {
        title: 'Capacidad',
        type: 'number',
      },
    },
  };
  constructor(public service: SmartTableService ) {

    // Obtengo los transportes
  this.service.getTransportes().subscribe(
    data => {

        this.source = data;
        console.log('Se hizo el traspaso de transportes');
      },
    );
  }

  // Agregar transporte
  onCreateConfirm(event) {
    this.transporte = {'id': event.newData.id,
                  'dominio': event.newData.dominio,
                  'cenasa': event.newData.cenasa,
                  'capacidad': event.newData.capacidad,
                  };
      this.service.createTransporte(this.transporte).subscribe(datas => {
        event.confirm.resolve();
        this.refreshTable();
        console.log('se guardo un transporte', datas);
      });
  }

  // Borrar transporte
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar el transporte?')) {
      const idTransporte: number = event.data.id;
      this.service.deleteTransporte(idTransporte).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el transportes', datas);
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar transporte
  onSaveConfirm(event) {
    this.transporte = {'id': event.newData.id,
                  'dominio': event.newData.dominio,
                  'cenasa': event.newData.cenasa,
                  'capacidad': event.newData.capacidad,
                  };
      this.service.updateTransporte(this.transporte).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el transporte', datas);
    });
  }

  // funcion para actulizar cambios
  refreshTable() {
    // Obtengo los corralero
  this.service.getTransportes().subscribe(
    data => {

        this.source = data;
      },
    );
    return   console.log('Se hizo el refresh de transportes');
  }
}
