import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaenasComponent } from './faenas.component';

describe('FaenasComponent', () => {
  let component: FaenasComponent;
  let fixture: ComponentFixture<FaenasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaenasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaenasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
