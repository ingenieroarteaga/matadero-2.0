import {TipoCamara} from '../../clasificadores/tipos-camaras/tipo-camara';

export class Camara {
        public id: number;
        public descripcion: string;
        public capacidad: number;
        public habilitado: string;
        public tipoCamara: TipoCamara;

}
