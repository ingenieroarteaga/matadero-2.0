import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import { Camara } from './camara';
import { TipoCamara } from '../../clasificadores/tipos-camaras/tipo-camara';

@Component({
  selector: 'ngx-camaras',
  templateUrl: './camaras.component.html',
  styleUrls: ['./camaras.component.scss'],
})
export class CamarasComponent {

  // Add camara
  public camara: Camara = new Camara();
  // Cargo los paises disponibles
  tiposCamaraCargador;
  tiposCamara: TipoCamara[];
  TipoCamaraCargado: TipoCamara = new TipoCamara();
  // Cargar camara en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
      capacidad: {
        title: 'Capacidad',
        type: 'number',
      },
      habilitado: {
        title: 'Habilitado',
        type: 'number',
      },
      tipoCamara: {
        title: 'Tipo de Camara',
        valuePrepareFunction: (data) => {
                                   return data['descripcion'];
                               },
        editor: {
          type: 'list',
          config: {
            list: this.tiposCamara,
          },
        },
      },
    },
  };
  constructor(public service: SmartTableService ) {

    // Obtengo los camaras
  this.service.getCamaras().subscribe(
    data => {

        this.source = data;
        console.log(this.source);

      },
    );

    // Obtengo los tipos de camara que muestro en editar
    this.service.getTiposCamaras().subscribe(
      data => {
        this.tiposCamara = data;
        this.tiposCamaraCargador = data.map( item => {
        return { title: item.descripcion , value: item.id };
        });
        // asigno a la variable setting los paises que debe mostrar en editor
        this.settings.columns.tipoCamara.editor.config.list = this.tiposCamaraCargador;
        this.settings = Object.assign({}, this.settings);
        console.log(this.tiposCamaraCargador);
        console.log(this.tiposCamara);
        },
      );
  }

  // Agregar camara
  onCreateConfirm(event) {
    this.camara = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'capacidad': event.newData.capacidad,
                  'habilitado': event.newData.habilitado,
                  'tipoCamara': this.camara.tipoCamara = new TipoCamara(),
                  };

      this.camara.tipoCamara =  this.tiposCamara.find(tipoCamara => tipoCamara.id == event.newData.tipoCamara);
      this.service.createCamara(this.camara).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo una camara', datas);
        this.refreshTable();
      });
  }

  // Borrar camara
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar la camara?')) {
      const idCamara: number = event.data.id;
      this.service.deleteCamara(idCamara).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro la camara', datas);
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar pais
  onSaveConfirm(event) {
    this.camara = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'capacidad': event.newData.capacidad,
                  'habilitado': event.newData.habilitado,
                  'tipoCamara': this.camara.tipoCamara = new TipoCamara(),
                  };

      this.camara.tipoCamara =  this.tiposCamara.find(tipoCamara => tipoCamara.id == event.newData.tipoCamara);
      console.log(this.camara);
      this.service.updateCamara(this.camara).subscribe(datas => {
      event.confirm.resolve();
      this.refreshTable();
      console.log('se edito el camara', datas);
    });
  }

  // funcion para actulizar cambios
  refreshTable() {
    // Obtengo los corralero
  this.service.getCamaras().subscribe(
    data => {

        this.source = data;
      },
    );
    return   console.log('Se hizo el refresh de corrales');
  }

}
