import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import { Corral } from './corral';

@Component({
  selector: 'ngx-corrales',
  templateUrl: './corrales.component.html',
  styleUrls: ['./corrales.component.scss'],
})
export class CorralesComponent {

  // Add corral
  public corral: Corral = new Corral();

  // Cargar corral en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
      capacidad: {
        title: 'Capacidad',
        type: 'number',
      },
      ocupacion: {
        title: 'Ocupación',
        type: 'number',
      },
      habilitado: {
        title: 'Habilitado',
        type: 'number',
      },
    },
  };
  constructor(public service: SmartTableService ) {

    // Obtengo los corrales
  this.service.getCorrales().subscribe(
    data => {

        this.source = data;
        console.log('Se hizo el traspaso de corrales');
      },
    );
  }

  // Agregar corral
  onCreateConfirm(event) {
    this.corral = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'capacidad': event.newData.capacidad,
                  'ocupacion': event.newData.ocupacion,
                  'habilitado': event.newData.habilitado,
                  };
      this.service.createCorral(this.corral).subscribe(datas => {

        console.log('se guardo un corral', datas);
        event.confirm.resolve();
        this.refreshTable();
      });
  }

  // Borrar corral
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar el corral?')) {
      const idCorral: number = event.data.id;
      this.service.deleteCorral(idCorral).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el corral', datas);
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar corral
  onSaveConfirm(event) {
    this.corral = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'capacidad': event.newData.capacidad,
                  'ocupacion': event.newData.ocupacion,
                  'habilitado': event.newData.habilitado,
                  };
      this.service.updateCorral(this.corral).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el corral', datas);
    });
  }

  // funcion para actulizar cambios
  refreshTable() {
    // Obtengo los corralero
  this.service.getCorrales().subscribe(
    data => {

        this.source = data;
      },
    );
    return   console.log('Se hizo el refresh de corrales');
  }

}
