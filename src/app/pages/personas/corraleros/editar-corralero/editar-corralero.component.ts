import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Usuario } from '../../usuario';
import { Corralero } from '../corralero';
// uso la clase que ya cree en agregar
import { FormUsuario } from './../agregar-corralero/form-usuario';

@Component({
  selector: 'ngx-editar-corralero',
  templateUrl: 'editar-corralero.component.html',
  styleUrls: ['editar-corralero.component.scss'],
})
export class EditarCorraleroComponent {

  @Input() title: string;

  // Para agregar corralero
  @Input() corralero: Corralero;
  public formCorralero: FormUsuario = new FormUsuario();


  constructor(protected ref: NbDialogRef<EditarCorraleroComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {

  }

  // confirmo para editar matarife
  editar() {

    if (typeof this.corralero.usuario.nombre === 'undefined'
        || this.corralero.usuario.nombre === null
        || typeof this.corralero.usuario.apellido === 'undefined'
        || this.corralero.usuario.apellido === null
        || typeof this.corralero.usuario.username === 'undefined'
        || this.corralero.usuario.username === null
        || typeof this.corralero.usuario.password === 'undefined'
        || this.corralero.usuario.password === null) {

          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });

    } else {
      console.log(this.corralero);
      this.service.updateCorralero(this.corralero).subscribe(datas => {
            console.log('se edito el corralero', datas);
            this.ref.close('name');
          });
    }

  }

  dismiss() {
    this.ref.close('name');
  }
}
