import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { NbDialogService } from '@nebular/theme';

import { Corralero } from './corralero';
import { AgregarCorraleroComponent } from './agregar-corralero/agregar-corralero.component';
import { EditarCorraleroComponent } from './editar-corralero/editar-corralero.component';

@Component({
  selector: 'ngx-corraleros',
  templateUrl: './corraleros.component.html',
  styleUrls: ['./corraleros.component.scss'],
})
export class CorralerosComponent {

  // Add tipo-animal
  public corralero: Corralero = new Corralero();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: false,
    },
    mode: 'external',
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: {
      add: false,
      delete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      usuario: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.apellido;
          },
      },
      username: {
        title: 'Usuario',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.username;
          },
      },
      password: {
        title: 'Contraseña',
        type: 'string',
        // TENGO QUE desencriptar
        // valuePrepareFunction: (cell, row) => {
        //     return row.usuario.password;
        //   },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.mail;
          },
      },
    },
  };

  constructor(public service: SmartTableService,
              private dialogService: NbDialogService,
              ) {

                  // Obtengo los corraleros
                this.service.getCorraleros().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de corraleros');
                    },
                  );

  }

  // Agregar corralero
  agregar() {

      this.dialogService.open(AgregarCorraleroComponent, {
        context: {
          title: 'Agregar Corralero',
        },
        closeOnEsc: false,
        closeOnBackdropClick: false,
      }).onClose.subscribe(data =>  this.refreshTable() );
  }

  // Borrar corralero
  onDeleteConfirm(event) {
    console.log(event.data.id);
    if (window.confirm('Estás seguro que quieres borrar el corralero?')) {
      const idCorralero: number = event.data.id;
      this.service.deleteCorralero(idCorralero).subscribe(datas => {
        console.log('se borro el corralero');
        this.refreshTable();
        return;
      });
    } else {
      return;
    }
  }

  // Editar corralero
  editRow(event) {
    this.corralero = {'id': event.data.id,
                  'usuario': event.data.usuario,
                  'fechaBaja': null ,
                  };

    console.log(this.corralero);
    this.dialogService.open(EditarCorraleroComponent, {
      context: {
        title: 'Editar Corralero',
        corralero: this.corralero,
      },
      closeOnEsc: false,
      closeOnBackdropClick: false,
    }).onClose.subscribe(data =>  this.refreshTable() );
  }

  // funcion para actulizar cambios
  refreshTable() {
    // Obtengo los corralero
  this.service.getCorraleros().subscribe(
    data => {

        this.source = data;
      },
    );
    return   console.log('Se hizo el refresh de corraleros');
  }

}
