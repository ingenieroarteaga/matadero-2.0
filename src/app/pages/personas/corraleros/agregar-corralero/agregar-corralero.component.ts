import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Usuario } from '../../usuario';
import { Corralero } from '../corralero';
import { FormUsuario} from './form-usuario';

@Component({
  selector: 'ngx-agregar-corralero',
  templateUrl: 'agregar-corralero.component.html',
  styleUrls: ['agregar-corralero.component.scss'],
})
export class AgregarCorraleroComponent {

  @Input() title: string;

  // Para agregar corralero
  public corralero: Corralero = new Corralero();
  public formCorralero: FormUsuario = new FormUsuario();


  constructor(protected ref: NbDialogRef<AgregarCorraleroComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {

  }


  // submit del form agrego el matarife
  agregar() {

    this.corralero = {'id': null,
                  'fechaBaja' : null,
                  'usuario': this.corralero.usuario = new Usuario(),
                  };
    this.corralero.usuario = {'id': null,
                 'nombre': this.formCorralero.nombre,
                 'apellido' : this.formCorralero.apellido,
                 'username' : this.formCorralero.username,
                 'password' : this.formCorralero.password,
                 'mail': this.formCorralero.email,
                 'imagen': 'Pw==',
                 'langKey': null,
                 'creadoPor': 'admin',
                 'fechaAlta': new Date(),
                 'modificadoPor': null,
                 'activado': null,
                 'fechaBaja': null,
                 'fechaModificacion': null,
                 'telefono': this.formCorralero.telefono,
     };

    if (typeof this.corralero.usuario.nombre === 'undefined'
        || this.corralero.usuario.nombre === null
        || typeof this.corralero.usuario.apellido === 'undefined'
        || this.corralero.usuario.apellido === null
        || typeof this.corralero.usuario.username === 'undefined'
        || this.corralero.usuario.username === null
        || typeof this.corralero.usuario.password === 'undefined'
        || this.corralero.usuario.password === null) {

          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });
    } else {
      console.log(this.corralero);
      this.service.createCorralero(this.corralero).subscribe(datas => {
            this.ref.close();
            console.log('se guardo un corralero', datas);
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
