import { Component } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { AgregarTransportistaComponent } from './agregar-transportista/agregar-transportista.component';
import { EditarTransportistaComponent } from './editar-transportista/editar-transportista.component';
import { Transportista } from './transportista';
import { Persona } from '../persona';

@Component({
  selector: 'ngx-transportistas',
  templateUrl: './transportistas.component.html',
  styleUrls: ['./transportistas.component.scss'],
})
export class TransportistasComponent {

  // Add tranportista
  public transportista: Transportista = new Transportista();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: false,
    },
    mode: 'external',
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: {
      add: false,
      delete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      persona: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.apellido;
          },
      },
      cuit: {
        title: 'Cuit',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.cuit;
          },
      },
      telefono: {
        title: 'Telefono',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.telefono;
          },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.email;
          },
      },
    },
  };

  constructor(public service: SmartTableService,
              private dialogService: NbDialogService,
              ) {

                  // Obtengo los transportistas
                this.service.getTransportistas().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de transportistas');
                    },
                  );

  }
  // Agregar transportistas
  agregar() {

      this.dialogService.open(AgregarTransportistaComponent, {
        context: {
          title: 'Agregar Transportista',
        },
        closeOnEsc: false,
        closeOnBackdropClick: false,
      }).onClose.subscribe(data =>  this.refreshTable() );
  }



  // Editar transportista
  editRow(event) {
    this.transportista = {'id': event.data.id,
                  'persona': event.data.persona,
                  'fechaBaja': event.data.fechaBaja,
                  };

    console.log(this.transportista);
    this.dialogService.open(EditarTransportistaComponent, {
      context: {
        title: 'Editar transportista',
        transportista: this.transportista,
      },
      closeOnEsc: false,
      closeOnBackdropClick: false,
    }).onClose.subscribe(data =>  this.refreshTable() );
  }

  // Borrar transportista
  onDeleteConfirm(event) {
    console.log(event.data.id);
    if (window.confirm('Estás seguro que quieres borrar el transportista?')) {
      const idTransportista: number = event.data.id;
      this.service.deleteTransportista(idTransportista).subscribe(datas => {
        console.log('se borro el transportista');
        this.refreshTable();
        return;
      });
    } else {
      return;
    }
  }

  // funcion para actulizar cambios
  refreshTable() {
    // Obtengo los transportistas
  this.service.getTransportistas().subscribe(
    data => {

        this.source = data;
      },
    );
    return   console.log('Se hizo el refresh de transportistas');
  }


}
