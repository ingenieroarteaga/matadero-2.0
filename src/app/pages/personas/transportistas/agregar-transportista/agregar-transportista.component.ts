import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Persona } from '../../persona';
import { Transportista } from '../transportista';
import { FormPersona } from './form-persona';
import { TipoDocumento } from '../../../clasificadores/tipos-documento/tipo-documento';
import { TipoPersona } from '../../../clasificadores/tipos-persona/tipo-persona';

@Component({
  selector: 'ngx-agregar-transportista',
  templateUrl: 'agregar-transportista.component.html',
  styleUrls: ['agregar-transportista.component.scss'],
})
export class AgregarTransportistaComponent {

  @Input() title: string;

  // Para agregar transportista
  public transportista: Transportista = new Transportista();
  public formTransportista: FormPersona = new FormPersona();
  // para mostrar los tipos
  tiposDocumento: any = [];
  tiposPersona: any = [];

  constructor(protected ref: NbDialogRef<AgregarTransportistaComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {
    // Obtengo los tipo de documento
    service.getTipoDocumentos().subscribe(
      data => {
          this.tiposDocumento = data;
          console.log(this.tiposDocumento);
        },
    );

    // Obtengo los tipo de person
    service.getTipoPersonas().subscribe(
      data => {
          this.tiposPersona = data;
          console.log(this.tiposPersona);
        },
    );
  }


  // submit del form agrego el transportista
  agregar() {

    this.transportista = {'id': null,
                  'fechaBaja' : null,
                  'persona': this.transportista.persona = new Persona(),
                  };
    this.transportista.persona = {'id': null,
                 'nombre': this.formTransportista.nombre,
                 'apellido' : this.formTransportista.apellido,
                 'cuit': this.formTransportista.cuit,
                 'dni': this.formTransportista.dni,
                 'cond_iva_id': null,
                 'fechaNac': this.formTransportista.fechaNac,
                 'email': this.formTransportista.email,
                 'telefono': this.formTransportista.telefono,
                 'tipoDocumento': this.transportista.persona.tipoDocumento = new TipoDocumento(),
                 'tipoPersona': this.transportista.persona.tipoPersona = new TipoPersona(),
                 'domicilioPersonaList': null,
                 'contactoPersonaList': null,
     };
    this.transportista.persona.tipoDocumento =  this.tiposDocumento.find
                                            (tipoDocumento => tipoDocumento.descripcion ==
                                              this.formTransportista.tipoDocumento);

    this.transportista.persona.tipoPersona =  this.tiposPersona.find
                                            (tipoPersona => tipoPersona.descripcion ==
                                            this.formTransportista.tipoPersona);


    if (typeof this.transportista.persona.nombre === 'undefined'
        || this.transportista.persona.nombre === null
        || typeof this.transportista.persona.apellido === 'undefined'
        || this.transportista.persona.apellido === null
        || this.transportista.persona.fechaNac === undefined
        || this.transportista.persona.tipoDocumento === undefined
        || this.transportista.persona.tipoPersona === undefined
        || this.transportista.persona.cuit === undefined
        || this.transportista.persona.dni === undefined) {

          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });
    } else {
      console.log(this.transportista);
      this.service.createTransportista(this.transportista).subscribe(datas => {
            this.ref.close();
            console.log('se guardo un transportista', datas);
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
