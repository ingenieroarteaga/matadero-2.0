import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Persona } from '../../persona';
import { Transportista } from '../transportista';
// uso la clase que ya cree en agregar
import { FormPersona } from './../agregar-transportista/form-persona';


@Component({
  selector: 'ngx-editar-transportista',
  templateUrl: 'editar-transportista.component.html',
  styleUrls: ['editar-transportista.component.scss'],
})
export class EditarTransportistaComponent {

  @Input() title: string;

  // Para agregar matarifes
  @Input() transportista: Transportista;
  // public matarife: Matarife = new Matarife();
  public formtransportista: FormPersona = new FormPersona();
  // para mostrar los tipos
  tiposDocumento: any = [];
  tiposPersona: any = [];
  // para asignar cambio de fecha
  nuevaFecha: Date;

  constructor(protected ref: NbDialogRef<EditarTransportistaComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {

    console.log(this.title);
    // Obtengo los tipo de documento
    service.getTipoDocumentos().subscribe(
      data => {
          this.tiposDocumento = data;
          console.log(this.tiposDocumento);
        },
    );

    // Obtengo los tipo de person
    service.getTipoPersonas().subscribe(
      data => {
          this.tiposPersona = data;
          console.log(this.tiposPersona);
        },
    );
  }


  // confirmo para editar transportista
  editar() {
    // Si cambio la fecha de nac asigno nuevo valor
    if (this.nuevaFecha == null ) {
      console.log('no cambio la fecha');
    } else {
      console.log('cambio la fecha');
      this.transportista.persona.fechaNac = this.nuevaFecha;
    }

    // Asigno el objeto tipo Documento y Tipo Persona
    this.transportista.persona.tipoDocumento =  this.tiposDocumento.find
                                            (tipoDocumento => tipoDocumento.descripcion ==
                                              this.transportista.persona.tipoDocumento.descripcion);

    this.transportista.persona.tipoPersona =  this.tiposPersona.find
                                            (tipoPersona => tipoPersona.descripcion ==
                                            this.transportista.persona.tipoPersona.descripcion);

    if ( this.transportista.persona.nombre === ''
        || this.transportista.persona.nombre == null
        || this.transportista.persona.apellido === ''
        || this.transportista.persona.apellido === null
        || this.transportista.persona.tipoDocumento === undefined
        || this.transportista.persona.tipoPersona === undefined
        || this.transportista.persona.dni === undefined) {


          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });

    } else {
      console.log(this.transportista);
      this.service.updateTransportista(this.transportista).subscribe(datas => {
            this.ref.close();
            console.log('se edito el transportista', datas);
            // window.location.reload();
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
