// import { Cliente } from "../../personas/cliente/cliente";
// import { ContactoPersona } from "../contacto-persona";
// import { TipoDocumento } from "../../clasificadores/tipo-documento/tipo-documento";
// import { TipoPersona } from "../tipo-persona/tipo-persona";
// import { Proveedor } from "../../personas/proveedor/proveedor";
// import { DomicilioPersona } from "../domicilio-persona";

export class Usuario {
    public username: string;
    public password: string;
    public nombre: string;
    public apellido: string;
    public mail: string;
    public imagen: any;
    public langKey: any;
    public creadoPor: string;
    public fechaAlta: Date;
    public modificadoPor: string;
    public id: number;
    public activado: any;
    public fechaBaja: Date;
    public fechaModificacion: Date;
    public telefono: number;

}
