import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';

import { ThemeModule } from '../../@theme/theme.module';
import { PersonasRoutingModule, routedComponents } from './personas-routing.module';

// Providers
import { SmartTableService } from '../../@core/data/smart-table.service';

// Components Matarifes
import { MatarifesComponent } from './matarifes//matarifes.component';
import { AgregarMatarifeComponent } from './matarifes/agregar-matarife/agregar-matarife.component';
import { EditarMatarifeComponent } from './matarifes/editar-matarife/editar-matarife.component';
import { InvalidFormComponent } from './matarifes/agregar-matarife/invalid-form/invalid-form.component';

// Components Productores
import { ProductoresComponent } from './productores//productores.component';
import { AgregarProductorComponent } from './productores/agregar-productor/agregar-productor.component';
import { EditarProductorComponent } from './productores/editar-productor/editar-productor.component';

// Components Corraleros
import { CorralerosComponent } from './corraleros//corraleros.component';
import { AgregarCorraleroComponent } from './corraleros/agregar-corralero/agregar-corralero.component';
import { EditarCorraleroComponent } from './corraleros/editar-corralero/editar-corralero.component';

// Components Operarios
import { OperariosComponent } from './operarios//operarios.component';
import { AgregarOperarioComponent } from './operarios/agregar-operario/agregar-operario.component';
import { EditarOperarioComponent } from './operarios/editar-operario/editar-operario.component';

// Components Administrativos
import { AdministrativosComponent } from './administrativos//administrativos.component';
// tslint:disable-next-line:max-line-length
import {AgregarAdministrativoComponent} from './administrativos/agregar-administrativo/agregar-administrativo.component';
import { EditarAdministrativoComponent } from './administrativos/editar-administrativo/editar-administrativo.component';

// Components Transportistas
import { TransportistasComponent } from './transportistas/transportistas.component';
import { AgregarTransportistaComponent } from './transportistas/agregar-transportista/agregar-transportista.component';
import { EditarTransportistaComponent } from './transportistas/editar-transportista/editar-transportista.component';


@NgModule({
  imports: [
    ThemeModule,
    PersonasRoutingModule,
    Ng2SmartTableModule,
    FormsModule,
  ],
  declarations: [
    AgregarMatarifeComponent,
    AgregarProductorComponent,
    AgregarCorraleroComponent,
    AgregarAdministrativoComponent,
    AgregarTransportistaComponent,
    AgregarOperarioComponent,
    EditarMatarifeComponent,
    EditarProductorComponent,
    EditarCorraleroComponent,
    EditarAdministrativoComponent,
    EditarTransportistaComponent,
    EditarOperarioComponent,
    InvalidFormComponent,
    MatarifesComponent,
    ProductoresComponent,
    CorralerosComponent,
    AdministrativosComponent,
    TransportistasComponent,
    OperariosComponent,
    ...routedComponents,
  ],
  providers: [
    SmartTableService,
  ],
  entryComponents: [
    AgregarMatarifeComponent,
    AgregarProductorComponent,
    AgregarCorraleroComponent,
    AgregarAdministrativoComponent,
    AgregarTransportistaComponent,
    AgregarOperarioComponent,
    EditarMatarifeComponent,
    EditarProductorComponent,
    EditarCorraleroComponent,
    EditarAdministrativoComponent,
    EditarTransportistaComponent,
    EditarOperarioComponent,
    InvalidFormComponent,
  ],
})
export class PersonasModule { }
