import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { NbDialogService } from '@nebular/theme';

import { Administrativo } from './administrativo';
import { Usuario } from '../usuario';
import { AgregarAdministrativoComponent } from './agregar-administrativo/agregar-administrativo.component';
import { EditarAdministrativoComponent } from './editar-administrativo/editar-administrativo.component';

@Component({
  selector: 'ngx-administrativos',
  templateUrl: './administrativos.component.html',
  styleUrls: ['./administrativos.component.scss'],
})
export class AdministrativosComponent {


  // Add tipo-animal
  public administrativo: Administrativo = new Administrativo();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: false,
    },
    mode: 'external',
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: {
      add: false,
      delete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      usuario: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.apellido;
          },
      },
      username: {
        title: 'Usename',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.username;
          },
      },
      password: {
        title: 'Contraseña',
        type: 'string',
        // tengo que desenciptar
        // valuePrepareFunction: (cell, row) => {
        //     return row.usuario.password;
        //   },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.mail;
          },
      },
      // telefono: {
      //   title: 'Teléfono',
      //   type: 'string',
      //   valuePrepareFunction: (cell, row) => {
      //       return row.usuario.telefono;
      //     },
      // },
    },
  };

  constructor(public service: SmartTableService,
              private dialogService: NbDialogService,
              ) {

                  // Obtengo los administrativos
                this.service.getAdministrativos().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de administrativos');
                    },
                  );

  }
  // Agregar corralero
  agregar() {

      this.dialogService.open(AgregarAdministrativoComponent, {
        context: {
          title: 'Agregar Administrativo',
        },
        closeOnEsc: false,
        closeOnBackdropClick: false,
      }).onClose.subscribe(data =>  this.refreshTable() );
  }

  // Borrar administrativo
  onDeleteConfirm(event) {
    console.log(event.data.id);
    if (window.confirm('Estás seguro que quieres borrar el administrativo?')) {
      const idAdmimnisrtativo: number = event.data.id;
      this.service.deleteAdministrativo(idAdmimnisrtativo).subscribe(datas => {
        console.log('se borro el adminisrativo');
        this.refreshTable();
        return;
      });
    } else {
      return;
    }
  }
  // Editar administrativo
  editRow(event) {
    this.administrativo = {'id': event.data.id,
                  'usuario': event.data.usuario,
                  'fechaBaja': null ,
                  };

    console.log(this.administrativo);
    this.dialogService.open(EditarAdministrativoComponent, {
      context: {
        title: 'Editar administrativo',
        administrativo: this.administrativo,
      },
      closeOnEsc: false,
      closeOnBackdropClick: false,
    }).onClose.subscribe(data =>  this.refreshTable() );
  }

  // funcion para actulizar cambios
  refreshTable() {
    // Obtengo los matarifes
  this.service.getAdministrativos().subscribe(
    data => {

        this.source = data;
      },
    );
    return   console.log('Se hizo el refresh de administrativos');
  }


}
