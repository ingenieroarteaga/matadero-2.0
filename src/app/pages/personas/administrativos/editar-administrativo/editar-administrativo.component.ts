import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Usuario } from '../../usuario';
import { Administrativo } from '../administrativo';
// uso la clase que ya cree en agregar
import { FormUsuario } from './../agregar-administrativo/form-usuario';

@Component({
  selector: 'ngx-editar-administrativo',
  templateUrl: 'editar-administrativo.component.html',
  styleUrls: ['editar-administrativo.component.scss'],
})
export class EditarAdministrativoComponent {

  @Input() title: string;

  // Para agregar corralero
  @Input() administrativo: Administrativo;
  public formAdministrativo: FormUsuario = new FormUsuario();


  constructor(protected ref: NbDialogRef<EditarAdministrativoComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {

  }

  // confirmo para editar administrativo
  editar() {

    if (typeof this.administrativo.usuario.nombre === 'undefined'
        || this.administrativo.usuario.nombre === null
        || typeof this.administrativo.usuario.apellido === 'undefined'
        || this.administrativo.usuario.apellido === null
        || typeof this.administrativo.usuario.username === 'undefined'
        || this.administrativo.usuario.username === null
        || typeof this.administrativo.usuario.password === 'undefined'
        || this.administrativo.usuario.password === null) {

          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });

    } else {
      console.log(this.administrativo);
      this.service.updateAdministrativo(this.administrativo).subscribe(datas => {
            this.ref.close();
            console.log('se edito el administrativo', datas);
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
