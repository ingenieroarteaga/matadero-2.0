

export class FormUsuario {
    public nombre: string;
    public apellido: string;
    public username: string;
    public password: string;
    public email: string;
    public telefono: number;
    public fechaBaja: Date;
    public fechaModificacion: Date;
    public fechaAlta: Date;
    public modificadoPor: string;
    public imagen: any;
    public langKey: any;
    public creadoPor: string;
    public activado: any;

}
