import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Usuario } from '../../usuario';
import { Administrativo } from '../administrativo';
import { FormUsuario} from './form-usuario';

@Component({
  selector: 'ngx-agregar-administrativo',
  templateUrl: 'agregar-administrativo.component.html',
  styleUrls: ['agregar-administrativo.component.scss'],
})
export class AgregarAdministrativoComponent {

  @Input() title: string;

  // Para agregar administrativo
  public administrativo: Administrativo = new Administrativo();
  public formAdministrativo: FormUsuario = new FormUsuario();


  constructor(protected ref: NbDialogRef<AgregarAdministrativoComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {

  }


  // submit del form agrego el administrativo
  agregar() {

    this.administrativo = {'id': null,
                  'fechaBaja' : null,
                  'usuario': this.administrativo.usuario = new Usuario(),
                  };
    this.administrativo.usuario = {'id': null,
                 'nombre': this.formAdministrativo.nombre,
                 'apellido' : this.formAdministrativo.apellido,
                 'username' : this.formAdministrativo.username,
                 'password' : this.formAdministrativo.password,
                 'telefono' : this.formAdministrativo.telefono,
                 'mail': this.formAdministrativo.email,
                 'imagen': 'Pw==',
                 'langKey': null,
                 'creadoPor': 'admin',
                 'fechaAlta': new Date(),
                 'modificadoPor': null,
                 'activado': null,
                 'fechaBaja': null,
                 'fechaModificacion': null,
     };

    if (typeof this.administrativo.usuario.nombre === 'undefined'
        || this.administrativo.usuario.nombre === null
        || typeof this.administrativo.usuario.apellido === 'undefined'
        || this.administrativo.usuario.apellido === null
        || typeof this.administrativo.usuario.username === 'undefined'
        || this.administrativo.usuario.username === null
        || typeof this.administrativo.usuario.password === 'undefined'
        || this.administrativo.usuario.password === null) {

          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });
    } else {
      console.log(this.administrativo);
      this.service.createAdministrativo(this.administrativo).subscribe(datas => {
            this.ref.close();
            console.log('se guardo un administrativo', datas);
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
