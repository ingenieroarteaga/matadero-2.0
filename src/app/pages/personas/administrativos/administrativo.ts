import { Usuario } from '../usuario';

export class Administrativo {
    public id: number;
    public usuario: Usuario;
    // public descripcion: string;
    public fechaBaja: Date;
}
