import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Persona } from '../../persona';
import { Productor } from '../productor';
// uso la clase que ya cree en agregar
import { FormPersona } from './../agregar-productor/form-persona';
import { TipoDocumento} from '../../../clasificadores/tipos-documento/tipo-documento';

@Component({
  selector: 'ngx-editar-productor',
  templateUrl: 'editar-productor.component.html',
  styleUrls: ['editar-productor.component.scss'],
})
export class EditarProductorComponent {

  @Input() title: string;

  // Para agregar productor
  @Input() productor: Productor;
  // public matarife: Matarife = new Matarife();
  public formProductor: FormPersona = new FormPersona();
  // para mostrar los tipos
  tiposDocumento: any = [];
  tiposPersona: any = [];
  // para asignar cambio de fecha
  nuevaFecha: Date;

  constructor(protected ref: NbDialogRef<EditarProductorComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {

    console.log(this.title);
    // Obtengo los tipo de documento
    service.getTipoDocumentos().subscribe(
      data => {
          this.tiposDocumento = data;
          console.log(this.tiposDocumento);
        },
    );

    // Obtengo los tipo de person
    service.getTipoPersonas().subscribe(
      data => {
          this.tiposPersona = data;
          console.log(this.tiposPersona);
        },
    );
  }


  // confirmo para editar Productor
  editar() {
    // Si cambio la fecha de nac asigno nuevo valor
    if (this.nuevaFecha == null ) {
      console.log('no cambio la fecha');
    } else {
      console.log('cambio la fecha');
      this.productor.persona.fechaNac = this.nuevaFecha;
    }

    // Asigno el objeto tipo Documento y Tipo Persona
    // La asignación falla cuando el campo descripción tiene más de una palabra
    this.productor.persona.tipoDocumento =  this.tiposDocumento.find
                                            (tipoDocumento => tipoDocumento.descripcion ==
                                              this.productor.persona.tipoDocumento.descripcion);

    this.productor.persona.tipoPersona =  this.tiposPersona.find
                                            (tipoPersona => tipoPersona.descripcion ==
                                            this.productor.persona.tipoPersona.descripcion);

    if ( this.productor.persona.nombre === ''
        || this.productor.persona.nombre == null
        || this.productor.persona.apellido === ''
        || this.productor.persona.apellido === null
        || this.productor.persona.tipoDocumento === undefined
        || this.productor.persona.tipoPersona === undefined
        || this.productor.persona.dni === undefined) {


          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });

    } else {
      console.log(this.productor);
      this.service.updateProductor(this.productor).subscribe(datas => {
            console.log('se edito el productor', datas);
            this.ref.close();

            // window.location.reload();
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
