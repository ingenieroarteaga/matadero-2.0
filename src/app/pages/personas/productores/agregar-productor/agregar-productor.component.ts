import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Persona } from '../../persona';
import { Productor } from '../productor';
import { FormPersona } from './form-persona';
import { TipoDocumento } from '../../../clasificadores/tipos-documento/tipo-documento';
import { TipoPersona } from '../../../clasificadores/tipos-persona/tipo-persona';

@Component({
  selector: 'ngx-agregar-productor',
  templateUrl: 'agregar-productor.component.html',
  styleUrls: ['agregar-productor.component.scss'],
})
export class AgregarProductorComponent {

  @Input() title: string;

  // Para agregar productor
  public productor: Productor = new Productor();
  public formProductor: FormPersona = new FormPersona();
  // para mostrar los tipos
  tiposDocumento: any = [];
  tiposPersona: any = [];

  constructor(protected ref: NbDialogRef<AgregarProductorComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {
    // Obtengo los tipo de documento
    service.getTipoDocumentos().subscribe(
      data => {
          this.tiposDocumento = data;
          console.log(this.tiposDocumento);
        },
    );

    // Obtengo los tipo de person
    service.getTipoPersonas().subscribe(
      data => {
          this.tiposPersona = data;
          console.log(this.tiposPersona);
        },
    );
  }


  // submit del form agrego el matarife
  agregar() {

    this.productor = {'id': null,
                  'fechaBaja' : null,
                  'persona': this.productor.persona = new Persona(),
                  };
    this.productor.persona = {'id': null,
                 'nombre': this.formProductor.nombre,
                 'apellido' : this.formProductor.apellido,
                 'cuit': this.formProductor.cuit,
                 'dni': this.formProductor.dni,
                 'cond_iva_id': null,
                 'fechaNac': this.formProductor.fechaNac,
                 'email': this.formProductor.email,
                 'telefono': this.formProductor.telefono,
                 'tipoDocumento': this.productor.persona.tipoDocumento = new TipoDocumento(),
                 'tipoPersona': this.productor.persona.tipoPersona = new TipoPersona(),
                 'domicilioPersonaList': null,
                 'contactoPersonaList': null,
     };
    this.productor.persona.tipoDocumento =  this.tiposDocumento.find
                                            (tipoDocumento => tipoDocumento.descripcion ==
                                              this.formProductor.tipoDocumento);

    this.productor.persona.tipoPersona =  this.tiposPersona.find
                                            (tipoPersona => tipoPersona.descripcion ==
                                            this.formProductor.tipoPersona);


    if (typeof this.productor.persona.nombre === 'undefined'
        || this.productor.persona.nombre === null
        || typeof this.productor.persona.apellido === 'undefined'
        || this.productor.persona.apellido === null
        || this.productor.persona.fechaNac === undefined
        || this.productor.persona.tipoDocumento === undefined
        || this.productor.persona.tipoPersona === undefined
        || this.productor.persona.cuit === undefined
        || this.productor.persona.dni === undefined) {

          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });
    } else {
      console.log(this.productor);
      this.service.createProductor(this.productor).subscribe(datas => {
            console.log('se guardo un productor', datas);
            this.ref.close();
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
