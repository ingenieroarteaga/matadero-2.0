

export class FormPersona {
    public nombre: string;
    public apellido: string;
    public cuit: number;
    public dni: number;
    public fechaNac: Date;
    public email: string;
    public telefono: string;
    public tipoDocumento: string;
    public tipoPersona: string;

}
