import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { NbDialogService } from '@nebular/theme';

import { Productor } from './productor';
import { AgregarProductorComponent } from './agregar-productor/agregar-productor.component';
import { EditarProductorComponent } from './editar-productor/editar-productor.component';


@Component({
  selector: 'ngx-productores',
  templateUrl: './productores.component.html',
  styleUrls: ['./productores.component.scss'],
})
export class ProductoresComponent {

  // Add productores
  public productor: Productor = new Productor();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: false,
    },
    mode: 'external',
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: {
      add: false,
      delete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      persona: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.apellido;
          },
      },
      cuit: {
        title: 'Cuit',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.cuit;
          },
      },
      telefono: {
        title: 'Telefono',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.telefono;
          },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.email;
          },
      },
    },
  };

  constructor(public service: SmartTableService,
              private dialogService: NbDialogService,
              ) {

                  // Obtengo los productores
                this.service.getProductores().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de productores');
                    },
                  );

  }
  // Agregar matarife
  agregar() {

      this.dialogService.open(AgregarProductorComponent, {
        context: {
          title: 'Agregar Productor',
        },
        closeOnEsc: false,
        closeOnBackdropClick: false,
      }).onClose.subscribe(data =>  this.refreshTable());
  }


  // Borrar productor
  onDeleteConfirm(event) {
    console.log(event.data.id);
    if (window.confirm('Estás seguro que quieres borrar el productor?')) {
      const idProductor: number = event.data.id;
      this.service.deleteProductor(idProductor).subscribe(datas => {
        console.log('se borro el productor');
        this.refreshTable();
        return;
      });
    } else {
      return;
    }
  }

  // Editar productor
  editRow(event) {
    this.productor = {'id': event.data.id,
                  'persona': event.data.persona,
                  'fechaBaja': event.data.fechaBaja,
                  };

    console.log(this.productor);
    this.dialogService.open(EditarProductorComponent, {
      context: {
        title: 'Editar Productor',
        productor: this.productor,
      },
      closeOnEsc: false,
      closeOnBackdropClick: false,
    }).onClose.subscribe(data =>  this.refreshTable() );
  }


  refreshTable() {
    // Obtengo los productores
  this.service.getProductores().subscribe(
    data => {

        this.source = data;
      },
    );
    return   console.log('Se hizo el refresh de productores');
  }

}
