import { Persona } from '../persona';

export class Productor {
        public id: number;
        public fechaBaja: Date;
        public persona: Persona;
}