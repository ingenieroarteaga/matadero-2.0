import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { NbDialogService } from '@nebular/theme';

import { Operario } from './operario';
import { AgregarOperarioComponent } from './agregar-operario/agregar-operario.component';
import { EditarOperarioComponent } from './editar-operario/editar-operario.component';

@Component({
  selector: 'ngx-operarios',
  templateUrl: './operarios.component.html',
  styleUrls: ['./operarios.component.scss'],
})
export class OperariosComponent {


  // Add tipo-animal
  public operario: Operario = new Operario();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: false,
    },
    mode: 'external',
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: {
      add: false,
      delete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      usuario: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.apellido;
          },
      },
      username: {
        title: 'Usename',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.username;
          },
      },
      password: {
        title: 'Contraseña',
        type: 'string',
        // TENGO QUE desencriptar
        // valuePrepareFunction: (cell, row) => {
        //     return row.usuario.password;
        //   },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.mail;
          },
      },
    },
  };

  constructor(public service: SmartTableService,
              private dialogService: NbDialogService,
              ) {

                  // Obtengo los operarios
                this.service.getOperarios().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de operarios');
                    },
                  );

  }
  // Editar operario
  editRow(event) {
    this.operario = {'id': event.data.id,
                  'usuario': event.data.usuario,
                  'fechaBaja': null ,
                  };

    console.log(this.operario);
    this.dialogService.open(EditarOperarioComponent, {
      context: {
        title: 'Editar operario',
        operario: this.operario,
      },
      closeOnEsc: false,
      closeOnBackdropClick: false,
    }).onClose.subscribe(data =>  this.refreshTable() );
  }

  // Agregar operario
  agregar() {

      this.dialogService.open(AgregarOperarioComponent, {
        context: {
          title: 'Agregar Operario',
        },
        closeOnEsc: false,
        closeOnBackdropClick: false,
      }).onClose.subscribe(data =>  this.refreshTable() );
  }

  // Borrar operario
  onDeleteConfirm(event) {
    console.log(event.data.id);
    if (window.confirm('Estás seguro que quieres borrar el operario?')) {
      const idOperario: number = event.data.id;
      this.service.deleteOperario(idOperario).subscribe(datas => {
        console.log('se borro el operario');
        this.refreshTable();
        return;
      });
    } else {
      return;
    }
  }

  // funcion para actulizar cambios
  refreshTable() {
    // Obtengo los opearios
  this.service.getOperarios().subscribe(
    data => {

        this.source = data;
      },
    );
    return   console.log('Se hizo el refresh de operarios');
  }

}
