import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Usuario } from '../../usuario';
import { Operario } from '../operario';
import { FormUsuario} from './form-usuario';

@Component({
  selector: 'ngx-agregar-operario',
  templateUrl: 'agregar-operario.component.html',
  styleUrls: ['agregar-operario.component.scss'],
})
export class AgregarOperarioComponent {

  @Input() title: string;

  // Para agregar operario
  public operario: Operario = new Operario();
  public formOperario: FormUsuario = new FormUsuario();


  constructor(protected ref: NbDialogRef<AgregarOperarioComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {

  }


  // submit del form agrego el matarife
  agregar() {

    this.operario = {'id': null,
                  'fechaBaja' : null,
                  'usuario': this.operario.usuario = new Usuario(),
                  };
    this.operario.usuario = {'id': null,
                 'nombre': this.formOperario.nombre,
                 'apellido' : this.formOperario.apellido,
                 'username' : this.formOperario.username,
                 'password' : this.formOperario.password,
                 'mail': this.formOperario.email,
                 'imagen': 'Pw==',
                 'langKey': null,
                 'creadoPor': 'admin',
                 'fechaAlta': new Date(),
                 'modificadoPor': null,
                 'activado': null,
                 'fechaBaja': null,
                 'fechaModificacion': null,
                 'telefono': this.formOperario.telefono,
     };

    if (typeof this.operario.usuario.nombre === 'undefined'
        || this.operario.usuario.nombre === null
        || typeof this.operario.usuario.apellido === 'undefined'
        || this.operario.usuario.apellido === null
        || typeof this.operario.usuario.username === 'undefined'
        || this.operario.usuario.username === null
        || typeof this.operario.usuario.password === 'undefined'
        || this.operario.usuario.password === null) {

          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });
    } else {
      console.log(this.operario);
      this.service.createOperario(this.operario).subscribe(datas => {
            this.ref.close();
            console.log('se guardo un operario', datas);
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
