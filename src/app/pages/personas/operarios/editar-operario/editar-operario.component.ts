import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../../matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { Usuario } from '../../usuario';
import { Operario } from '../operario';
// uso la clase que ya cree en agregar
import { FormUsuario } from './../agregar-operario/form-usuario';

@Component({
  selector: 'ngx-editar-operario',
  templateUrl: 'editar-operario.component.html',
  styleUrls: ['editar-operario.component.scss'],
})
export class EditarOperarioComponent {

  @Input() title: string;

  // Para agregar operario
  @Input() operario: Operario;
  public formoperario: FormUsuario = new FormUsuario();


  constructor(protected ref: NbDialogRef<EditarOperarioComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {

  }

  // confirmo para editar matarife
  editar() {

    if (typeof this.operario.usuario.nombre === 'undefined'
        || this.operario.usuario.nombre === null
        || typeof this.operario.usuario.apellido === 'undefined'
        || this.operario.usuario.apellido === null
        || typeof this.operario.usuario.username === 'undefined'
        || this.operario.usuario.username === null
        || typeof this.operario.usuario.password === 'undefined'
        || this.operario.usuario.password === null) {

          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });

    } else {
      console.log(this.operario);
      this.service.updateOperario(this.operario).subscribe(datas => {
            this.ref.close();
            console.log('se edito el operario', datas);
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
