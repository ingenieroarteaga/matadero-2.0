// import { Cliente } from "../../personas/cliente/cliente";
// import { ContactoPersona } from "../contacto-persona";
import { TipoDocumento } from '../clasificadores/tipos-documento/tipo-documento';
import { TipoPersona } from '../clasificadores/tipos-persona/tipo-persona';
// import { Proveedor } from "../../personas/proveedor/proveedor";
// import { DomicilioPersona } from "../domicilio-persona";

export class Persona {
    public id: number;
    public nombre: string;
    public apellido: string;
    public cuit: number;
    public dni: number;
    public cond_iva_id: any;
    public fechaNac: Date;
    public email: string;
    public telefono: string;
    public tipoDocumento: TipoDocumento;
    public tipoPersona: TipoPersona;
    public contactoPersonaList: any;
    public domicilioPersonaList: any;

}
