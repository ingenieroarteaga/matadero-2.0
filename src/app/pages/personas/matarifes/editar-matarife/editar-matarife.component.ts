import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from '../agregar-matarife/invalid-form/invalid-form.component';
import { Persona } from '../../persona';
import { Matarife } from '../matarife';
import { EstadoCliente } from '../estado-cliente';
// uso la clase que ya cree en agregar
import { FormPersona } from './../agregar-matarife/form-persona';
import { TipoDocumento } from '../../../clasificadores/tipos-documento/tipo-documento';
import { TipoPersona } from '../../../clasificadores/tipos-persona/tipo-persona';

@Component({
  selector: 'ngx-editar-matarife',
  templateUrl: 'editar-matarife.component.html',
  styleUrls: ['editar-matarife.component.scss'],
})
export class EditarMatarifeComponent {

  @Input() title: string;

  // Para agregar matarifes
  @Input() matarife: Matarife;
  // public matarife: Matarife = new Matarife();
  public formMatarife: FormPersona = new FormPersona();
  // para mostrar los tipos
  tiposDocumento: any = [];
  tiposPersona: any = [];
  // para asignar cambio de fecha
  nuevaFecha: Date;

  constructor(protected ref: NbDialogRef<EditarMatarifeComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {

    console.log(this.title);
    // Obtengo los tipo de documento
    service.getTipoDocumentos().subscribe(
      data => {
          this.tiposDocumento = data;
          console.log(this.tiposDocumento);
        },
    );

    // Obtengo los tipo de person
    service.getTipoPersonas().subscribe(
      data => {
          this.tiposPersona = data;
          console.log(this.tiposPersona);
        },
    );
  }


  // confirmo para editar matarife
  editar() {
    // Si cambio la fecha de nac asigno nuevo valor
    if (this.nuevaFecha == null ) {
      console.log('no cambio la fecha');
    } else {
      console.log('cambio la fecha');
      this.matarife.persona.fechaNac = this.nuevaFecha;
    }

    // Asigno el objeto tipo Documento y Tipo Persona
    this.matarife.persona.tipoDocumento =  this.tiposDocumento.find
                                            (tipoDocumento => tipoDocumento.descripcion ==
                                              this.matarife.persona.tipoDocumento.descripcion);

    this.matarife.persona.tipoPersona =  this.tiposPersona.find
                                            (tipoPersona => tipoPersona.descripcion ==
                                            this.matarife.persona.tipoPersona.descripcion);

    if ( this.matarife.persona.nombre === ''
        || this.matarife.persona.nombre == null
        || this.matarife.persona.apellido === ''
        || this.matarife.persona.apellido === null
        || this.matarife.persona.tipoDocumento === undefined
        || this.matarife.persona.tipoPersona === undefined
        || this.matarife.persona.dni === undefined) {


          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });

    } else {
      console.log(this.matarife);
      this.service.updateMatarife(this.matarife).subscribe(datas => {
            console.log('se edito el matarife', datas);
            this.ref.close();
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
