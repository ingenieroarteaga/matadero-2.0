
export class EstadoCliente {
  public descripcion: string;
  public id: number;
  public bloqueado: number;
  public fechaBaja: Date;
}
