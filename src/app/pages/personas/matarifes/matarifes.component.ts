import { Component, TemplateRef } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { InvalidFormComponent } from '../../personas/matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { AgregarMatarifeComponent } from './agregar-matarife/agregar-matarife.component';
import { EditarMatarifeComponent } from './editar-matarife/editar-matarife.component';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Matarife } from './matarife';
import { EstadoCliente } from './estado-cliente';

@Component({
  selector: 'ngx-matarifes',
  templateUrl: './matarifes.component.html',
  styleUrls: ['./matarifes.component.scss'],
})
export class MatarifesComponent {

  // Add matarifes
  public matarife: Matarife = new Matarife();

  // Cargar matarifes en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: false,
    },
    mode: 'external',
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: {
      add: false,
      delete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      persona: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.apellido;
          },
      },
      cuit: {
        title: 'Cuit',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.cuit;
          },
      },
      telefono: {
        title: 'Telefono',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.telefono;
          },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.email;
          },
      },
      ruca: {
        title: 'Ruca',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.ruca;
          },
      },
    },
  };

  constructor(public service: SmartTableService,
              private dialogService: NbDialogService,
              ) {
                  // Obtengo los Matarifes
                this.service.getMatarifes().subscribe(
                  data => {
                      this.source = data;
                      console.log('Se hizo el traspaso de matarifes');
                      console.log(this.source);
                    },
                  );

  }
  // Agregar matarife
  agregar() {

      this.dialogService.open(AgregarMatarifeComponent, {
        context: {
          title: 'Agregar Matarife',
        },
        closeOnEsc: false,
        closeOnBackdropClick: false,
      }).onClose.subscribe(data =>  this.refreshTable() );
  }



  // Editar matarife
  editRow(event) {
    this.matarife = {'id': event.data.id,
                  'persona': event.data.persona,
                  'descripcion' : event.data.descripcion,
                  'fechaBaja': event.data.fechaBaja,
                  'estadoCliente' : this.matarife.estadoCliente = new EstadoCliente(),
                  'ruca' : event.data.ruca,
                  };

    this.matarife.estadoCliente = event.data.estadoCliente;
    console.log(this.matarife);
    this.dialogService.open(EditarMatarifeComponent, {
      context: {
        title: 'Editar Matarife',
        matarife: this.matarife,
      },
      closeOnEsc: false,
      closeOnBackdropClick: false,
    }).onClose.subscribe(data =>  this.refreshTable() );
  }


  // Borrar matarife
  onDeleteConfirm(event) {
    console.log(event.data.id);
    if (window.confirm('Estás seguro que quieres borrar el matarife?')) {
      this.dialogService.open(InvalidFormComponent, {
        context: {
          title: 'No está autorizado a borrar Matarifes',
        },
        closeOnEsc: false,
        closeOnBackdropClick: true,
      });
      // tengo que validar que el matarife que quiere borrar no tenga ingresos asignados
      // antes de enviar al servicio para borrar
      // const idMatarife: number = event.data.id;
      // this.service.deleteMatarife(idMatarife).subscribe(datas => {
      //   console.log('se borro el matarife');
      //   this.refreshTable();
      //   return;
      // });
    } else {
      return;
    }
  }

  // funcion para actulizar cambios
  refreshTable() {
    // Obtengo los matarifes
  this.service.getMatarifes().subscribe(
    data => {

        this.source = data;
      },
    );
    return   console.log('Se hizo el refresh de matarifes');
  }

}
