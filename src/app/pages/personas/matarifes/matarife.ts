import { Persona } from '../persona';
import { EstadoCliente } from './estado-cliente';

export class Matarife {
        public id: number;
        public persona: Persona;
        public descripcion: string;
        public fechaBaja: Date; //
        public estadoCliente: EstadoCliente;
        public ruca: string;

}
