import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'ngx-invalid-form',
  templateUrl: 'invalid-form.component.html',
  styleUrls: ['invalid-form.component.scss'],
})
export class InvalidFormComponent {

  @Input() title: string;


  constructor(protected ref: NbDialogRef<InvalidFormComponent>,
    ) {

  }
  dismiss() {
    this.ref.close();
  }
}
