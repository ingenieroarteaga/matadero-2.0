import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { SmartTableService } from '../../../../@core/data/smart-table.service';
import { InvalidFormComponent } from './invalid-form/invalid-form.component';
import { Persona } from '../../persona';
import { Matarife } from '../matarife';
import { EstadoCliente } from '../estado-cliente';
import { FormPersona } from './form-persona';
import { TipoDocumento } from '../../../clasificadores/tipos-documento/tipo-documento';
import { TipoPersona } from '../../../clasificadores/tipos-persona/tipo-persona';

@Component({
  selector: 'ngx-agregar-matarife',
  templateUrl: 'agregar-matarife.component.html',
  styleUrls: ['agregar-matarife.component.scss'],
})
export class AgregarMatarifeComponent {

  @Input() title: string;

  // Para agregar matarifes
  public matarife: Matarife = new Matarife();
  public formMatarife: FormPersona = new FormPersona();
  // para mostrar los tipos
  tiposDocumento: any = [];
  tiposPersona: any = [];

  constructor(protected ref: NbDialogRef<AgregarMatarifeComponent>,
              public service: SmartTableService,
              private dialogService: NbDialogService ) {
    // Obtengo los tipo de documento
    service.getTipoDocumentos().subscribe(
      data => {
          this.tiposDocumento = data;
          console.log(this.tiposDocumento);
        },
    );

    // Obtengo los tipo de person
    service.getTipoPersonas().subscribe(
      data => {
          this.tiposPersona = data;
          console.log(this.tiposPersona);
        },
    );
  }


  // submit del form agrego el matarife
  agregar() {

    this.matarife = {'id': null,
                  'descripcion': null,
                  'fechaBaja' : null,
                  'persona': this.matarife.persona = new Persona(),
                  'estadoCliente': this.matarife.estadoCliente = new EstadoCliente(),
                  'ruca': this.formMatarife.ruca,
                  };
    this.matarife.persona = {'id': null,
                 'nombre': this.formMatarife.nombre,
                 'apellido' : this.formMatarife.apellido,
                 'cuit': this.formMatarife.cuit,
                 'dni': this.formMatarife.dni,
                 'cond_iva_id': null,
                 'fechaNac': this.formMatarife.fechaNac,
                 'email': this.formMatarife.email,
                 'telefono': this.formMatarife.telefono,
                 'tipoDocumento': this.matarife.persona.tipoDocumento = new TipoDocumento(),
                 'tipoPersona': this.matarife.persona.tipoPersona = new TipoPersona(),
                 'domicilioPersonaList': null,
                 'contactoPersonaList': null,
     };
    this.matarife.persona.tipoDocumento =  this.tiposDocumento.find
                                            (tipoDocumento => tipoDocumento.descripcion ==
                                              this.formMatarife.tipoDocumento);

    this.matarife.persona.tipoPersona =  this.tiposPersona.find
                                            (tipoPersona => tipoPersona.descripcion ==
                                            this.formMatarife.tipoPersona);

    this.matarife.estadoCliente = {'id': 1,
                  'descripcion': null,
                  'bloqueado' : null,
                  'fechaBaja': null,
                  };

    if (typeof this.matarife.persona.nombre === 'undefined'
        || this.matarife.persona.nombre === null
        || typeof this.matarife.persona.apellido === 'undefined'
        || this.matarife.persona.apellido === null
        || this.matarife.persona.fechaNac === undefined
        || this.matarife.persona.tipoDocumento === undefined
        || this.matarife.persona.tipoPersona === undefined
        || this.matarife.persona.cuit === undefined
        || this.matarife.persona.dni === undefined) {

          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe rellenar todos los campos marcados con *',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });
    } else {
      console.log(this.matarife);
      this.service.createMatarife(this.matarife).subscribe(datas => {
            this.ref.close();
            console.log('se guardo un matarife', datas);
          });
    }

  }

  dismiss() {
    this.ref.close();
  }
}
