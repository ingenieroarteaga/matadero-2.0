  // tslint:disable-next-line:max-line-length
import { Component, OnDestroy } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { NbDialogService } from '@nebular/theme';
import { InvalidFormComponent } from '../../personas/matarifes/agregar-matarife/invalid-form/invalid-form.component';
import { FormsModule } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
// Necesarios par alas notificaciones
// Necesarioas para el mensaje toaster
import { ToasterConfig } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import { NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
// Datos para el ingreso
import {FormIngreso} from '../../operaciones/ingresos/form-ingreso';
import {RangoTropa} from '../../operaciones/rangos-tropas/rango-tropa';
import {Matarife} from '../../personas/matarifes/matarife';
import {Productor} from '../../personas/productores/productor';
import {Transportista} from '../../personas/transportistas/transportista';
import {Corralero} from '../../personas/corraleros/corralero';
import {TipoAnimal} from '../../clasificadores/tipos-animales/tipo-animal';
import {SubTipoAnimal} from '../../clasificadores/subtipos-animales/subtipo-animal';
import {TipoProcedencia} from '../../clasificadores/tipos-procedencias/tipo-procedencia';
import {TipoDestino} from '../../clasificadores/tipos-destinos/tipo-destino';
import {Tropa} from '../../operaciones/ingresos/tropa';
import {TropaDetalle} from '../../operaciones/ingresos/tropa-detalle';
import {Ingreso} from '../../operaciones/ingresos/ingreso';
import {Transporte} from '../../herramientas/transportes/transporte';
import {Corral} from '../../herramientas/corrales/corral';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ngx-info-matarifes',
  templateUrl: './info-matarifes.component.html',
  styleUrls: ['./info-matarifes.component.scss'],
})
export class InfoMatarifesComponent implements OnDestroy {

  // para listar  los ingresos ya cargados
  ingresos: any = [];

  tropa: Tropa = new Tropa();
  ingreso: Ingreso = new Ingreso();

  formIngreso: FormIngreso = new FormIngreso();

  matarifes: any = [];
  productores: any = [];
  loadingProductores = false;
  transportistas: any = [];
  transportes: any = [];
  corraleros: any = [];
  corrales: any = [];
  tiposAnimal: any = [];
  subTiposAnimales: any = [];
  distritos: any = [];
  destinos: any = [];
  procedencias: any = [];
  editIngresos: any = [];

  // para obtener el número de tropa
  rangoTropa: RangoTropa = new RangoTropa();
  numeroTropaDevuelto: number;
  numeroTropaAsignado: number;

  buttonAgregar: boolean = true;
  tituloFormulario: string = 'Nuevo Ingreso';

  // Para listar los subtipos
  subTiposAnimalesList: any = [];
  // Guardar los datos de subtipos cargados
  subTiposElegidos = {
    checkbox: [],
    cantvivo: [],
    pesovivo: [],
    cantmuerto: [],
    pesomuerto: [],
    raza: [],
  };
 // para armar la lista de TropaDetalle

  tropaDetalleCargar: TropaDetalle;

  // Datos para mostrar notificaciones
  config: ToasterConfig;
  index = 1;
  destroyByClick = true;
  duration = 3500;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbToastStatus;
  titleToast: string;
  contentToat: string;
  // para cortar el servicio cuando cambio da pantalla
  getIngresosSubscribe: Subscription;
  getProductoresSubscribe: Subscription;
  /////////// Paginacion
  ///////// Buscar
 search: any = {};
 totalPages: Array<number>;

 page = 0;
 size = 10;
 order = 'id';
 asc = false;

 isFirst = false;
 isLast = false;

  constructor(public service: SmartTableService,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService) {

         // Obtengo los ingresos
        this.getIngresosSubscribe = service.getIngresosPaginado(this.search, this.page, this.size,
          this.order, this.asc ).subscribe(
           data => {

             this.ingresos = data.content;
             this.isFirst = data.first;
             this.isLast = data.last;
             this.totalPages = new Array(data.totalPages);
             console.log(data);
            },
          );
        // Obtengo los matarifes
        service.getMatarifes().subscribe(
          data => {

              this.matarifes = data;
              console.log(this.matarifes);
            },
        );
        // Obtengo los productores
        // Como son mucho los muestro paginados
        this.buscarProductoresLista(null);

        // Obtengo los transportistas
        service.getTransportistas().subscribe(
          data => {

              this.transportistas = data;
              console.log(this.transportistas);
            },
        );
        // Obtengo los transportes
        service.getTransportes().subscribe(
          data => {

              this.transportes = data;
              console.log(this.transportes);
            },
        );
        // Obtengo los corraleros
        service.getCorraleros().subscribe(
          data => {

              this.corraleros = data;
              console.log(this.corraleros);
            },
        );
        // Obtengo los corrales
        service.getCorrales().subscribe(
          data => {

              this.corrales = data;
              console.log(this.corrales);
            },
        );

        // Obtengo los tipo de animales
        service.getTiposAnimales().subscribe(
          data => {
              this.tiposAnimal = data;
              console.log(this.tiposAnimal);
            },
        );

        // Obtengo los subtippo de animales
        service.getSubTiposAnimales().subscribe(
          data => {
              this.subTiposAnimales = data;
              console.log(this.subTiposAnimales);
            },
        );
        // Obtengo los destinos
        service.getTiposDestinos().subscribe(
          data => {
              this.destinos = data;
              console.log(this.destinos);
            },
        );

        // Obtengo las procedencias
        service.getTiposProcedencias().subscribe(
          data => {
              this.procedencias = data;
              console.log(this.procedencias);
            },
        );

  }

  /////// Corto la carga de registros
   ngOnDestroy() {
     if (typeof this.getProductoresSubscribe !== 'undefined') {
       this.getProductoresSubscribe.unsubscribe();
     }
     if (typeof this.getIngresosSubscribe !== 'undefined') {
       this.getIngresosSubscribe.unsubscribe();
     }
   }


  //  Obtengo el número de tropa
  obtenerNumeroDeTropa() {

    if (this.formIngreso.matarife != null && this.formIngreso.tipoAnimal != null) {

        this.rangoTropa = { id: null,
        desde: new Date(),
        hasta: null,
        inicio: 1,
        fin: 1,
        numero: 0,
        tipoAnimal: this.rangoTropa.tipoAnimal = new TipoAnimal,
        cliente: this.rangoTropa.cliente = new Matarife,
        };

      this.rangoTropa.tipoAnimal = this.tiposAnimal.find(
                                   tipoAnimal => tipoAnimal.id === this.formIngreso.tipoAnimal );
      this.rangoTropa.cliente = this.matarifes.find(matarife =>
                                matarife.id === this.formIngreso.matarife );

      console.log(this.rangoTropa);
      console.log(this.formIngreso);
      this.service.getNumeroRangoTropa(this.rangoTropa).subscribe(data => {
        console.log(data);
        this.numeroTropaDevuelto = data;
        if (this.numeroTropaDevuelto === 0) {
          // si devuelve cero el cliente seleccionado no tiene rango de tropa asignado
          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'El cliente no tiene Rango de Tropa asignado',
            },
            closeOnEsc: false,
            closeOnBackdropClick: false,
          });
          this.buttonAgregar = true;
          this.formIngreso.numeroTropa = null;
        } else if (this.numeroTropaDevuelto === -1) {
            // si devuelve menos uno el cliente seleccionado ocupo todo el rango
            this.dialogService.open(InvalidFormComponent, {
              context: {
                title: 'El cliente ocupo el total del rango de tropa',
              },
              closeOnEsc: false,
              closeOnBackdropClick: true,
            });
            this.formIngreso.numeroTropa = null;
            this.buttonAgregar = true;
        } else {
            this.formIngreso.numeroTropa = data;
            this.buttonAgregar = false;
            console.log(this.formIngreso.numeroTropa);
        }
      });
    } else {
      // si no elige matarife y tipo de animal rechazo
      this.dialogService.open(InvalidFormComponent, {
        context: {
          title: 'Debe seleccionar Matarife y Tipo de Animal',
        },
        closeOnEsc: false,
        closeOnBackdropClick: true,
      });
    }
  }

  // Listar los subTipoAnimal del tipoAnimal seleccionado
  listaSubTiposAnimal (event) {
    this.subTiposAnimalesList = [];
    this.subTiposAnimales.forEach(subtipo => {
      if (subtipo.tipoAnimal.id === this.formIngreso.tipoAnimal) {
        this.subTiposAnimalesList.push(subtipo);
      }
    });
    // limpio los valores ngModel de la lista de subtipos
    this.subTiposElegidos = {
      checkbox: [],
      cantvivo: [],
      pesovivo: [],
      cantmuerto: [],
      pesomuerto: [],
      raza: [],
    };
    // le asigno valores iniciales a la lista de subtipos
    for (let i = 0; i < this.subTiposAnimalesList.length; i++) {
      console.log(i);
      this.subTiposElegidos.checkbox[i] = false;
      this.subTiposElegidos.cantvivo[i] = 0;
      this.subTiposElegidos.pesovivo[i] = 0;
      this.subTiposElegidos.cantmuerto[i] = 0;
      this.subTiposElegidos.pesomuerto[i] = 0;
    }
    let h = 0;
    this.subTiposAnimalesList.forEach(sub => {
      this.subTiposElegidos.raza[h] = sub.id;
      h = h + 1;
    });
  }


  agregar() {
    // Valido que esten todos los campos de ingreso completos
    if (this.formIngreso.numeroDte !== undefined && this.formIngreso.fechaDte !== undefined &&
        this.formIngreso.matarife !== undefined && this.formIngreso.transportista !== undefined &&
        this.formIngreso.transporte !== undefined && this.formIngreso.productor !== undefined &&
        this.formIngreso.corral !== undefined && this.formIngreso.corralero !== undefined &&
        this.formIngreso.destino !== undefined && this.formIngreso.procedencia !== undefined) {

          // Agrego tropa, en caso de exito agrego ingreso
          this.tropa = {'id': null,
                        'numero': this.formIngreso.numeroTropa,
                        'dte': this.formIngreso.numeroDte,
                        'fechaDte': this.formIngreso.fechaDte,
                        'guia': this.formIngreso.numeroDte, // uso el mismo valor q DTE
                        'fechaGuia': this.formIngreso.fechaDte, // uso el mismo que fecha DTE
                        'cantidad': 0,
                        'peso': 0,
                        'cantidadMuerto': 0,
                        'pesoMuerto': 0,
                        'libroFolio': '1',
                        'libroNumero': '1',
                        'cliente': this.tropa.cliente = new Matarife(),
                        'chofer': this.tropa.chofer = new Transportista(),
                        'transporte': this.tropa.transporte = new Transporte(),
                        'productor': this.tropa.productor = new Productor(),
                        'destino': this.tropa.destino = new TipoDestino(),
                        'procedencia': this.tropa.procedencia = new TipoProcedencia(),
                        'tropaDetalleList': [], // Cambiar valor
          },
          this.tropa.cliente = this.matarifes.find(matarife => matarife.id === this.formIngreso.matarife);
          this.tropa.chofer = this.transportistas.find(transportista =>
                                                  transportista.id === this.formIngreso.transportista);
          this.tropa.destino = this.destinos.find(destino =>
                                             destino.id === this.formIngreso.destino);
          this.tropa.procedencia = this.procedencias.find(procedencia =>
                                                     procedencia.id === this.formIngreso.procedencia);
          this.tropa.transporte = this.transportes.find(transporte => transporte.id === this.formIngreso.transporte);
          this.tropa.productor = this.productores.find(productor => productor.id === this.formIngreso.productor);
          console.log(this.tropa);
          // cargo los detalles de tropa
          for (let i = 0; i < this.subTiposAnimalesList.length; i++) {
            if (this.subTiposElegidos.checkbox[i]) {
              this.tropaDetalleCargar = new TropaDetalle ();
              this.tropaDetalleCargar.fecha = new Date ();
              this.tropaDetalleCargar.cantidad = this.subTiposElegidos.cantvivo[i];
              this.tropaDetalleCargar.cantidadMuerto = this.subTiposElegidos.cantmuerto[i];
              this.tropaDetalleCargar.cantidadDecomizado = 0;
              this.tropaDetalleCargar.estadoAnimalId = 0;
              this.tropaDetalleCargar.tropa = null;
              this.tropaDetalleCargar.subtipoAnimal = new SubTipoAnimal ();
              this.tropaDetalleCargar.subtipoAnimal = this.subTiposAnimalesList
                                                      .find(subtipo => subtipo.id === this.subTiposElegidos.raza[i]);
              this.tropa.tropaDetalleList.push(this.tropaDetalleCargar);
            }
          }
          // Suma la cantida de animales vivos/muertos para asiganar totales
          this.tropa.tropaDetalleList.forEach (tropaDetalle => {
              this.tropa.cantidad = this.tropa.cantidad + tropaDetalle.cantidad;
              this.tropa.cantidadMuerto = this.tropa.cantidadMuerto + tropaDetalle.cantidadMuerto;
            },
          );
          for (let i = 0; i < this.subTiposAnimalesList.length; i++) {
            this.tropa.peso = this.tropa.peso + this.subTiposElegidos.pesovivo[i];
            this.tropa.pesoMuerto = this.tropa.pesoMuerto + this.subTiposElegidos.pesomuerto[i];
          }
          console.log(this.tropa);
          // valido que haya cargado al menos un subtipp
          if (this.tropa.cantidad > 0 ) {
            this.service.createTropa(this.tropa).subscribe(datas => {
              console.log(datas);
              // Le asisgno el id de tropa a cada TropaDetalle
              // datas.tropaDetalleList.forEach( tropaDetalle => {
              //   tropaDetalle.tropa = datas;
              //   this.service.updateTropaDetalleList(tropaDetalle).subscribe(
              //   info => {
              //     console.log('id tropa asignado en tropa detalla', info );
              //   });
              // });
              // Si sale todo bien despues de cargar la tropa cargo el ingreso
              this.agregarIngreso(datas);
            });
          } else {
            this.dialogService.open(InvalidFormComponent, {
              context: {
                title: 'No ha seleccionado ninguna raza',
              },
              closeOnEsc: false,
              closeOnBackdropClick: true,
            });
          }
        } else {
          this.dialogService.open(InvalidFormComponent, {
            context: {
              title: 'Debe completar todos los campos del Ingreso',
            },
            closeOnEsc: false,
            closeOnBackdropClick: true,
          });
        }
  }
  // Agrego ingreso
  agregarIngreso(tropa) {

    this.ingreso = {'id': null,
                  'fecha': this.formIngreso.fechaIngreso,
                  'numero': null,
                  'fechaEgreso': null,
                  'cantidad': this.tropa.cantidad,
                  'corral': this.ingreso.corral = new Corral(),
                  'corralero': this.ingreso.corralero = new Corralero(),
                  'matanza': null,
                  'tropa': tropa,
                  'romaneoList': null,
                  // agregar en el formulario el campo observarciones
                  'observaciones': null,

    },
    this.ingreso.corral = this.corrales.find(corral => corral.id === this.formIngreso.corral);
    this.ingreso.corralero = this.corraleros.find(corralero => corralero.id === this.formIngreso.corralero);
    console.log(this.ingreso);
    this.service.createIngreso(this.ingreso).subscribe(datas => {
      //
      console.log('se cargo el ingreso' , datas);
      this.refreshIngresos();
      this.status = NbToastStatus.SUCCESS;
      this.titleToast = 'El Ingreso fue registrado';
      // cuando falle
      // this.status =  NbToastStatus.WARNING;
      // this.titleToast =  'No se pudo cargar Lista de Matanza'
      this.showToast(this.status, this.titleToast, this.contentToat);
      // limpio el formulario y deshabilito el boton
      this.formIngreso = new FormIngreso();
      this.buttonAgregar = true;
    });
  }

  // Borra Ingreso de la listar
  borrarIngreso(ingreso) {
    //
    // this.dialogService.open(InvalidFormComponent, {
    //   context: {
    //     title: 'La función borrar está deshabilitada para este usuario',
    //   },
    //   closeOnEsc: false,
    //   closeOnBackdropClick: true,
    // });
    console.log(ingreso);
    if (window.confirm('Estás seguro que quieres borrar el ingreso?')) {
      if (ingreso.matanza === null) {
        const idIngreso: number = ingreso.id;
        this.service.deleteIngreso(idIngreso).subscribe(datas => {
          console.log('se borro el ingreso');
          console.log(datas);
          this.refreshIngresos ();
        });
      } else {
        this.dialogService.open(InvalidFormComponent, {
          context: {
            title: 'El ingreso tiene lista de matanza asiganada. No se puede borrar.',
          },
          closeOnEsc: false,
          closeOnBackdropClick: true,
        });
      }
    } else {
      return;
    }
  }

  // Editar ingreso
  editIngreso(ingresoEdit) {

    this.dialogService.open(InvalidFormComponent, {
      context: {
        title: 'La función editar está deshabilitada para este usuario',
      },
      closeOnEsc: false,
      closeOnBackdropClick: true,
    });
    // REEMPLAZAR DATOS DE ARMADO DEL INGRESO POR LOS
    // QUE INGRESE EL USUARIO EN EL FORMULARIO DE EDIT
    // this.editIngresos = {
    //   'id': ingresoEdit.id,
    //   'fecha': '2019-11-25T03:00:00.000+0000',
    //   'numero':  ingresoEdit.numero,
    //   'cantidad': ingresoEdit.cantidad,
    //   'fechaEgreso':  ingresoEdit.fechaEgreso,
    //   'corral':  ingresoEdit.corral,
    //   'corralero':  ingresoEdit.corralero,
    //   'matanza':  ingresoEdit.matanza,
    //   'tropa':  ingresoEdit.tropa,
    //   'observaciones': ingresoEdit.observaciones,
    // };
    // console.log(ingresoEdit);
    // console.log(this.editIngresos);
    // this.service.updateIngreso(this.editIngresos).subscribe(data => {
    //   console.log('se actualizo el ingreso', data.id );
    // });
  }

  // Actualiza la lista de ingresos
  refreshIngresos () {
    this.service.getIngresosPaginado(this.search, this.page, this.size,
      this.order, this.asc ).subscribe(
       data => {
         this.ingresos = data.content;
         this.isFirst = data.first;
         this.isLast = data.last;
         this.totalPages = new Array(data.totalPages);
         console.log(data);
        },
      );
  }

  // Resetear numero de tropa cuando cambio matarife o especie
  resetTropaN() {
    this.formIngreso.numeroTropa = null;
    this.buttonAgregar = true;
  }
  // Funcion para mostrar Toastear
  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  /////////////////////////////////////
 buscar() {
   this.ingresos = [];

   const v_search: any = {};
   v_search.numero = this.search.numero;

   if (typeof this.search.desde !== 'undefined' && this.search.desde !== null) {
     v_search.desde = this.search.desde;
   } else {
     v_search.desde = null;
   }
   if (typeof this.search.hasta !== 'undefined' && this.search.hasta !== null) {
     v_search.hasta = this.search.hasta;
   } else {
     v_search.hasta = null;
   }

   if (typeof this.search.cliente !== 'undefined' && this.search.cliente !== null) {
     v_search.cliente = this.search.cliente.id;
   } else {
     v_search.cliente = null;
   }

   if (typeof this.search.sucursal !== 'undefined' && this.search.sucursal !== null) {
     v_search.sucursal = this.search.sucursal.id;
   } else {
     v_search.sucursal = null;
   }

   if (typeof this.search.puntoVenta !== 'undefined' && this.search.puntoVenta !== null) {
     v_search.puntoVenta = this.search.puntoVenta.id;
   } else {
     v_search.puntoVenta = null;
   }

   if (typeof this.search.tipoFactura !== 'undefined' && this.search.tipoFactura !== null) {
     v_search.tipoFactura = this.search.tipoFactura.id;
   } else {
     v_search.tipoFactura = null;
   }

   this.service.getIngresosPaginado(
     v_search, this.page, this.size, this.order, this.asc).subscribe(
       data => {
         this.ingresos = data.content;

         this.isFirst = data.first;
         this.isLast = data.last;
         this.totalPages = new Array(data.totalPages);

       },
       err => {
         console.log(err.error);
       },
     );
 }
 ////////////////////////////////////

  /////////////// Paginacion Ingresos //////////////////////////
  sort(): void {
    this.asc = !this.asc;
    this.buscar();
  }

  rewind(): void {
    if (!this.isFirst) {
      this.page--;
      this.buscar();
    }
  }

  forward(): void {
    if (!this.isLast) {
      this.page++;
      this.buscar();
    }
  }

  setPage(page: number): void {
    this.page = page;
    this.buscar();
  }

  setOrder(order: string): void {
    this.order = order;
    this.buscar();
  }
  //////////////////// Fin Paginacion Ingresos ////////////////////////////

  ///////////////////////////////////// Paginacion productores
  buscarProductoresLista(p_search) {
    this.productores = [];
    const v_search: any = {};
    v_search.descripcion = p_search;

    this.getProductoresSubscribe = this.service.getProductoresPaginado(v_search).subscribe(data => {
      this.productores = data.content;
    },
      err => {
        console.log(err.error);
      },
    );
  }
  ////////////////////////////////////
}
