import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformesComponent } from './informes.component';

import { InfoMatarifesComponent } from './info-matarifes/info-matarifes.component';

const routes: Routes = [{
  path: '',
  component: InformesComponent,
  children: [ {
      path: 'info-matarifes',
      component: InfoMatarifesComponent,
      },
    ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformesRoutingModule { }
export const routedComponents = [
    InformesComponent,
    InfoMatarifesComponent,
];
