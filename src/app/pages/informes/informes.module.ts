import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { InformesRoutingModule, routedComponents } from './informes-routing.module';
import { SmartTableService } from '../../@core/data/smart-table.service';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { InfoMatarifesComponent } from './info-matarifes/info-matarifes.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    InformesRoutingModule,
    Ng2SmartTableModule,
    AutocompleteModule.forRoot(),
    Ng2AutoCompleteModule,
    NgSelectModule,
    FormsModule,

  ],
  declarations: [
    ...routedComponents,
    InfoMatarifesComponent,
  ],
  providers: [
    SmartTableService,
  ],
})
export class InformesModule { }
