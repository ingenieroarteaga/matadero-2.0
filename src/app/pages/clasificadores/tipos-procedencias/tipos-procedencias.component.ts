import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { TipoProcedencia} from './tipo-procedencia' ;

@Component({
  selector: 'ngx-tipos-procedencias',
  templateUrl: './tipos-procedencias.component.html',
  styleUrls: ['./tipos-procedencias.component.scss'],
})
export class TiposProcedenciasComponent {


  // Add tipo-procedencia
  public tipoProcedencia: TipoProcedencia = new TipoProcedencia();

  // Cargar procedencias en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
      codigo: {
        title: 'Codigo',
        type: 'string',
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los tipo de procedencias
                this.service.getTiposProcedencias().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de tipo de procedencias');
                    },
                  );

  }
  // Agregar tipo procedencia
  onCreateConfirm(event) {
    this.tipoProcedencia = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'codigo': event.newData.codigo,
                  };
      this.service.createTipoProcedencia(this.tipoProcedencia).subscribe(datas => {
        console.log('se guardo un tipo de procedencia', datas);
        // Hago el refresh de la tabla
        this.service.getTiposProcedencias().subscribe(
          data => {
            this.source = data;
          },
        );
        event.confirm.resolve();
      });
  }

  // Borrar tipo de procedencia
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar el tipo de procedencia?')) {
      const idTipoProcedencia: number = event.data.id;
      this.service.deleteTipoProcedencia(idTipoProcedencia).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el tipo de procedencia', datas);
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar tipo procedencia
  onSaveConfirm(event) {
    this.tipoProcedencia = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'codigo': event.newData.codigo,
                  };
      this.service.updateTipoProcedencia(this.tipoProcedencia).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el tipo de procedencia', datas);
    });
  }


}
