import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import {TipoPersona} from './tipo-persona';

@Component({
  selector: 'ngx-tipos-persona',
  templateUrl: './tipos-persona.component.html',
  styleUrls: ['./tipos-persona.component.scss'],
})
export class TiposPersonaComponent {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Trabajar con tipo-domicilio
  public tipoPersona: TipoPersona = new TipoPersona();

  // Cargar tipo-domicilios en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Tipo de Persona',
        type: 'string',
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {
                // Obtengo los tipo de persona
                this.service.getTipoPersonas().subscribe(
                  data => {

                      this.source = data;
                      console.log(this.source);
                    },
                  );
  }

  // Agregar tipo persona
  onCreateConfirm(event) {
    this.tipoPersona = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  };
      this.service.createTipoPersona(this.tipoPersona).subscribe(datas => {
        console.log('se guardo un tipo de persona', datas);
        // Hago el refresh de la tabla
        this.service.getTipoPersonas().subscribe(
          data => {
            this.source = data;
          },
        );
        event.confirm.resolve();
      });
  }

  // Borrar tipo de persona
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar el tipo de persona?')) {
      const idTipoPersona: number = event.data.id;
      this.service.deleteTipoPersona(idTipoPersona).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el tipo de persona');
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar tipo persona
  onSaveConfirm(event) {
    this.tipoPersona = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  };
      this.service.updateTipoPersona(this.tipoPersona).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el tipo de documento', datas);
    });
  }


}
