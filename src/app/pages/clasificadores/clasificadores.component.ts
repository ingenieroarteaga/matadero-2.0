import { Component } from '@angular/core';

@Component({
  selector: 'ngx-clasificadores',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ClasificadoresComponent {
}
