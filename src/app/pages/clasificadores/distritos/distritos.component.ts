import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import {Localidad} from '../localidades/localidad';
import {Distrito} from './distrito';
// import {Provincia} from '../provincias/provincia';
// import {Pais} from '../paises/pais';

@Component({
  selector: 'ngx-distritos',
  templateUrl: './distritos.component.html',
  styleUrls: ['./distritos.component.scss'],
})
export class DistritosComponent {
  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Add distrito
  public distrito: Distrito = new Distrito();

  // Cargo las localidades disponibles
  localidadesCargador;
  localidades: Localidad[];
  localidadCargador: Localidad = new Localidad();

  // Cargar distritos en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
      localidad: {
        title: 'Localidad',
        valuePrepareFunction: (data) => {
                                   return data['descripcion'];
                               },
        editor: {
        type: 'list',
        config: {
          list: this.localidades,
          },
        },
      },
      // provincia: {
      //   title: 'provincia',
      //   valuePrepareFunction: (cell, row) => {
      //                           return row.localidad.provincia.descripcion;
      //                         },
      //   editable: false,
      //   addable: false,
      // },
      // pais: {
      //   title: 'pais',
      //   valuePrepareFunction: (cell, row) => {
      //                           return row.localidad.provincia.pais.descripcion;
      //                         },
      //   editable: false,
      //   addable: false,
      // },
      codigoPostal: {
        title: 'Codigo Postal',
        type: 'string',
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {
                // Obtengo las localidades que muestro en editar
                this.service.getLocalidades().subscribe(
                  data => {
                    this.localidades = data;
                    this.localidadesCargador = data.map( item => {
                    return { title: item.descripcion , value : item.id };
                    });
                    // asigno a la variable setting los paises que debe mostrar en editor
                    this.settings.columns.localidad.editor.config.list = this.localidadesCargador;
                    this.settings = Object.assign({}, this.settings);
                    console.log(this.localidadesCargador);
                    console.log(this.localidades);
                    },
                  );
                  // Obtengo las distritos
                this.service.getDistritos().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de localidades');
                      console.log(this.source);
                    },
                  );
  }

  // Agregar Distrito
  onCreateConfirm(event) {
    this.distrito = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja' : event.newData.fechaBaja,
                  'localidad': this.distrito.localidad = new Localidad(),
                  'codigoPostal' : event.newData.codigoPostal,
                  };
      this.distrito.localidad =  this.localidades.find(localidad => localidad.id == event.newData.localidad);
      console.log(this.distrito);
      this.service.createDistrito(this.distrito).subscribe(datas => {

        console.log('se guardo un Distrito', datas);
        // Hago refresh de la tabla
        this.service.getDistritos().subscribe(
          data => {
            this.source = data;
          },
        );
        event.confirm.resolve();
      });
  }

  // Borrar disrtito
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar el distrito?')) {
      const idDistrito: number = event.data.id;
      this.service.deleteDistrito(idDistrito).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el distrito');
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar distrito
  onSaveConfirm(event) {
    this.distrito = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja' : event.newData.fechaBaja,
                  'localidad': this.distrito.localidad = new Localidad(),
                  'codigoPostal' : event.newData.codigoPostal,
                  };
    this.distrito.localidad =  this.localidades.find(localidad => localidad.id == event.newData.localidad);
    console.log(this.distrito);
    this.service.updateDistrito(this.distrito).subscribe(datas => {
      console.log('se edito el distrito', datas);
      // Hago el refresh de la tabla
      this.service.getDistritos().subscribe(
        data => {
          this.source = data;
        },
      );
      event.confirm.resolve();
    });
  }



}
