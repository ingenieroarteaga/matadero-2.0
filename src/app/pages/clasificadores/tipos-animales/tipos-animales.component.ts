import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { TipoAnimal } from './tipo-animal' ;

@Component({
  selector: 'ngx-tipos-animales',
  templateUrl: './tipos-animales.component.html',
  styleUrls: ['./tipos-animales.component.scss'],
})
export class TiposAnimalesComponent {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Add tipo-animal
  public tipoAnimal: TipoAnimal = new TipoAnimal();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los tipo de animales
                this.service.getTiposAnimales().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de tipo de animales');
                    },
                  );

  }
  // Agregar tipo animal
  onCreateConfirm(event) {
    this.tipoAnimal = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  };
      this.service.createTipoAnimal(this.tipoAnimal).subscribe(datas => {
        console.log('se guardo un tipo de animal', datas);
        // Hago el refresh de la tabla
        this.service.getTiposAnimales().subscribe(
          data => {
            this.source = data;
          },
        );
        event.confirm.resolve();
      });
  }

  // Borrar tipo de animal
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar el tipo de animal?')) {
      const idTipoAnimal: number = event.data.id;
      this.service.deleteTipoAnimal(idTipoAnimal).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el tipo de animal');
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar tipo animal
  onSaveConfirm(event) {
    this.tipoAnimal = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  };
      this.service.updateTipoAnimal(this.tipoAnimal).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el tipo de animal', datas);
    });
  }


}
