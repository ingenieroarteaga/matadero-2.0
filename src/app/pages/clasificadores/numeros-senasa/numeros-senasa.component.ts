import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import { TipoAnimal } from '../tipos-animales/tipo-animal';
import { TipoCamara } from '../tipos-camaras/tipo-camara';
import {NumeroSenasa} from './numero-senasa';

@Component({
  selector: 'ngx-numeros-senasa',
  templateUrl: './numeros-senasa.component.html',
  styleUrls: ['./numeros-senasa.component.scss'],
})
export class NumeroSenasaComponent {
  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Add numeroSenasa
  public numeroSenasa: NumeroSenasa = new NumeroSenasa();

  // Cargo los tipo animal  disponibles
  tipoAnimalesCargador;
  tipoAnimales: TipoAnimal[];
  tipoAnimalCargador: TipoAnimal = new TipoAnimal();

  // Cargo los tipo animal  disponibles
  tipoCamarasCargador;
  tipoCamaras: TipoCamara[];
  tipoCamaraCargador: TipoCamara = new TipoCamara();

  // Cargar numeros en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Número',
        type: 'string',
      },
      tipoAnimal: {
        title: 'Tipo de Animal',
        valuePrepareFunction: (data) => {
                                   return data['descripcion'];
                               },
        editor: {
        type: 'list',
        config: {
          list: this.tipoAnimales,
          },
        },
      },
      tipoCamara: {
        title: 'Tipo de Camara',
        valuePrepareFunction: (data) => {
                                   return data['descripcion'];
                               },
        editor: {
        type: 'list',
        config: {
          list: this.tipoCamaras,
          },
        },
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {
                // Obtengo los tipo de animal que muestro en editar
                this.service.getTiposAnimales().subscribe(
                  data => {
                    this.tipoAnimales = data;
                    this.tipoAnimalesCargador = data.map( item => {
                    return { title: item.descripcion , value : item.id };
                    });
                    // asigno a la variable setting los tipo de animal que debe mostrar en editor
                    this.settings.columns.tipoAnimal.editor.config.list = this.tipoAnimalesCargador;
                    this.settings = Object.assign({}, this.settings);
                    console.log(this.tipoAnimalesCargador);
                    console.log(this.tipoAnimales);
                    },
                  );

                  // Obtengo los tipo de camara que muestro en editar
                  this.service.getTiposCamaras().subscribe(
                    data => {
                      this.tipoCamaras = data;
                      this.tipoCamarasCargador = data.map( item => {
                      return { title: item.descripcion , value : item.id };
                      });
                      // asigno a la variable setting los tipo de animal que debe mostrar en editor
                      this.settings.columns.tipoCamara.editor.config.list = this.tipoCamarasCargador;
                      this.settings = Object.assign({}, this.settings);
                      console.log(this.tipoCamarasCargador);
                      console.log(this.tipoCamaras);
                      },
                    );
                  // Obtengo las distritos
                this.service.getNumerosSenasa().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de numeros');
                      console.log(this.source);
                    },
                  );
  }

  // Agregar Numero
  onCreateConfirm(event) {
    this.numeroSenasa = {'id': event.newData.id,
                  'tipoAnimal': this.numeroSenasa.tipoAnimal = new TipoAnimal(),
                  'tipoCamara' : this.numeroSenasa.tipoCamara = new TipoCamara(),
                  'descripcion': event.newData.descripcion,
                  };
      this.numeroSenasa.tipoAnimal =  this.tipoAnimales.find(tipoAnimal => tipoAnimal.id == event.newData.tipoAnimal);
      this.numeroSenasa.tipoCamara =  this.tipoCamaras.find(tipoCamara => tipoCamara.id == event.newData.tipoCamara);
      console.log(this.numeroSenasa);
      this.service.createNumeroSenasa(this.numeroSenasa).subscribe(datas => {

        console.log('se guardo un número de Senasa', datas);
        // Hago refresh de la tabla
        this.service.getNumerosSenasa().subscribe(
          data => {
            this.source = data;
          },
        );
        event.confirm.resolve();
      });
  }

  // Borrar disrtito
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar el número de senasa?')) {
      const idNumero: number = event.data.id;
      this.service.deleteNumeroSenasa(idNumero).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el número de Senasa');
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar distrito
  onSaveConfirm(event) {
    this.numeroSenasa = {'id': event.newData.id,
                  'tipoAnimal': this.numeroSenasa.tipoAnimal = new TipoAnimal(),
                  'tipoCamara' : this.numeroSenasa.tipoCamara = new TipoCamara(),
                  'descripcion': event.newData.descripcion,
                  };
    this.numeroSenasa.tipoAnimal =  this.tipoAnimales.find(tipoAnimal => tipoAnimal.id == event.newData.tipoAnimal);
    this.numeroSenasa.tipoCamara =  this.tipoCamaras.find(tipoCamara => tipoCamara.id == event.newData.tipoCamara);
    console.log(this.numeroSenasa);
    this.service.updateNumeroSenasa(this.numeroSenasa).subscribe(datas => {
      console.log('se edito el numero de senas', datas);
      // Hago el refresh de la tabla
      this.service.getNumerosSenasa().subscribe(
        data => {
          this.source = data;
        },
      );
      event.confirm.resolve();
    });
  }
}
