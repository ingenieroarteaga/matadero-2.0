import { TipoAnimal } from '../tipos-animales/tipo-animal';
import { TipoCamara } from '../tipos-camaras/tipo-camara';

export class NumeroSenasa {
    public id: number;
    public descripcion: string;
    public tipoAnimal: TipoAnimal;
    public tipoCamara: TipoCamara;
}
