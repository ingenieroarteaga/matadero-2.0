import { Pais } from '../paises/pais';

export class Provincia {

    public id: number;
    public descripcion: string;
    public fechaBaja: Date;
    public pais: Pais;
    public localidadList: [];

}
