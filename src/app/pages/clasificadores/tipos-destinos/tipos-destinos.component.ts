import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { TipoDestino} from './tipo-destino' ;

@Component({
  selector: 'ngx-tipos-destinos',
  templateUrl: './tipos-destinos.component.html',
  styleUrls: ['./tipos-destinos.component.scss'],
})
export class TiposDestinosComponent {


  // Add tipo-destino
  public tipoDestino: TipoDestino = new TipoDestino();

  // Cargar destinos en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los tipo de destinos
                this.service.getTiposDestinos().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de tipo de destinos');
                    },
                  );

  }
  // Agregar tipo destino
  onCreateConfirm(event) {
    this.tipoDestino = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  };
      this.service.createTipoDestino(this.tipoDestino).subscribe(datas => {
        console.log('se guardo un tipo de desdtino', datas);
        // Hago el refresh de la tabla
        this.service.getTiposDestinos().subscribe(
          data => {
            this.source = data;
          },
        );
        event.confirm.resolve();
      });
  }

  // Borrar tipo de destino
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar el tipo de destino?')) {
      const idTipoDestino: number = event.data.id;
      this.service.deleteTipoDestino(idTipoDestino).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el tipo de destino', datas);
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar tipo destino
  onSaveConfirm(event) {
    this.tipoDestino = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  };
      this.service.updateTipoDestino(this.tipoDestino).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el tipo de destino', datas);
    });
  }


}
