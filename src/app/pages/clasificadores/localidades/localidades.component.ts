import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import {Localidad} from './localidad';
import {Provincia} from '../provincias/provincia';
import {Pais} from '../paises/pais';


@Component({
  selector: 'ngx-localidades',
  templateUrl: './localidades.component.html',
  styleUrls: ['./localidades.component.scss'],
})
export class LocalidadesComponent  {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Add localidad
  public localidad: Localidad = new Localidad();

  // Cargo las provincias disponibles
  provinciasCargador;
  provincias: Provincia[];
  provinciaCargador: Provincia = new Provincia();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
      // fechaBaja: {
      //   title: 'Fecha Baja',
      //   type: 'string',
      //   editable: false,
      //   addable: false,
      // },
      provincia: {
        title: 'provincia',
        valuePrepareFunction: (data) => {
                                   return data['descripcion'];
                               },
        editor: {
        type: 'list',
        config: {
          list: this.provincias,
          },
        },
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {
                  // Obtengo las localidades
                this.service.getLocalidades().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de localidades');
                    },
                  );
                  // Obtengo las provincias que muestro en editar
                  this.service.getProvincias().subscribe(
                    data => {
                      this.provincias = data;
                      this.provinciasCargador = data.map( item => {
                      return { title: item.descripcion , value : item.id };
                      });
                      // asigno a la variable setting los paises que debe mostrar en editor
                      this.settings.columns.provincia.editor.config.list = this.provinciasCargador;
                      this.settings = Object.assign({}, this.settings);
                      console.log(this.provinciasCargador);
                      console.log(this.provincias);
                      },
                    );

  }
  // Agregar Localidad
  onCreateConfirm(event) {
    this.localidad = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja' : event.newData.fechaBaja,
                  'provincia': this.localidad.provincia = new Provincia(),
                  'zona': event.newData.zona,
                  'distritoList' : event.newData.distritoList,
                  };
      this.localidad.provincia =  this.provincias.find(provincia => provincia.id == event.newData.provincia);
      console.log(this.localidad);
      this.service.createLocalidad(this.localidad).subscribe(datas => {

        console.log('se guardo una localidad', datas);
        // Hago el refresh de la tabla
        this.service.getLocalidades().subscribe(
          data => {
            this.source = data;
          },
        );
        event.confirm.resolve();
      });
  }

  // Borrar localidad
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar la localidad?')) {
      const idLocalidad: number = event.data.id;
      this.service.deleteLocalidad(idLocalidad).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro la localidad');
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar localidades
  onSaveConfirm(event) {
    this.localidad = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja' : event.newData.fechaBaja,
                  'provincia': this.localidad.provincia = new Provincia(),
                  'zona': event.newData.zona,
                  'distritoList' : event.newData.distritoList,
                  };
      this.localidad.provincia =  this.provincias.find(provincia => provincia.id == event.newData.provincia);
      console.log(this.localidad);
      this.service.updateLocalidad(this.localidad).subscribe(datas => {
        console.log('se edito la localidad', datas);
        // Hago el refresh de la tabla
        this.service.getLocalidades().subscribe(
          data => {
            this.source = data;
          },
        );
        event.confirm.resolve();
    });
  }
}
