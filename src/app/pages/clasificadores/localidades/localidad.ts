import { Provincia } from '../provincias/provincia';
// import { Distrito } from "../distrito/distrito";

export class Localidad {
    public id: number;
    public descripcion: string;
    public fechaBaja: Date;
    public distritoList: any[];
    public provincia: Provincia;
    public zona: any;

}

