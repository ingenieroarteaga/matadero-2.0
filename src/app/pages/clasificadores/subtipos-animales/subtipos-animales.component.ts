import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { SubTipoAnimal } from './subtipo-animal';
import {TipoAnimal} from '../tipos-animales/tipo-animal';

@Component({
  selector: 'ngx-subtipos-animales',
  templateUrl: './subtipos-animales.component.html',
  styleUrls: ['./subtipos-animales.component.scss'],
})
export class SubTiposAnimalesComponent {


  // Add tipo-animal
  public subtipoAnimal: SubTipoAnimal = new SubTipoAnimal();

  // Obtengo los tipo de Animales
  tipoDeAnimalesCargador;
  tipoDeAnimales: TipoAnimal[];
  tipoDeAnimalCargador: TipoAnimal = new TipoAnimal();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
      tipoAnimal: {
        title: 'Tipo Animal',
        valuePrepareFunction: (data) => {
                                   return data['descripcion'];
                               },
        editor: {
          type: 'list',
          config: {
            list: this.tipoDeAnimales,
          },
        },
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los subtipo de animales
                this.service.getSubTiposAnimales().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de subtipo de animales');
                      console.log(this.source);
                    },
                  );
                // Obtengo los tipo de animales que muestro en editar
                this.service.getTiposAnimales().subscribe(
                  data => {
                    this.tipoDeAnimales = data;
                    this.tipoDeAnimalesCargador = data.map( item => {
                    return { title: item.descripcion , value : item.id };
                    });
                    // asigno a la variable setting los tipos de animales que debe mostrar en editor
                    this.settings.columns.tipoAnimal.editor.config.list = this.tipoDeAnimalesCargador;
                    this.settings = Object.assign({}, this.settings);
                    console.log(this.tipoDeAnimalesCargador);
                    console.log(this.tipoDeAnimales);
                    },
                  );

  }
  // Agregar subtipo
  onCreateConfirm(event) {
    this.subtipoAnimal = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'tipoAnimal': this.subtipoAnimal.tipoAnimal = new TipoAnimal(),
                  };
    this.subtipoAnimal.tipoAnimal = this.tipoDeAnimales.find(
                      tipoAnimal => tipoAnimal.id == event.newData.tipoAnimal);
    console.log(this.subtipoAnimal);
    this.service.createSubTipoAnimal(this.subtipoAnimal).subscribe(datas => {
      console.log('se guardo un subtipo de animal', datas);
      // Hago el refresh de la tabla
      this.service.getSubTiposAnimales().subscribe(
        data => {
          this.source = data;
        },
      );
      event.confirm.resolve();
    });
  }

  // Borrar subtipo de animal
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar el subtipo de animal?')) {
      const idSubTipoAnimal: number = event.data.id;
      this.service.deleteSubTipoAnimal(idSubTipoAnimal).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el subtipo de animal');
      });
    } else {
      event.confirm.reject();
    }
  }


  // Editar subtipo
  onSaveConfirm(event) {
    this.subtipoAnimal = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'tipoAnimal': this.subtipoAnimal.tipoAnimal = new TipoAnimal(),
                  };
    this.subtipoAnimal.tipoAnimal = this.tipoDeAnimales.find(
                      tipoAnimal => tipoAnimal.id == event.newData.tipoAnimal);
    console.log(this.subtipoAnimal);
    this.service.updateSubTipoAnimal(this.subtipoAnimal).subscribe(datas => {
      console.log('se edito el tipo de subtipo de animal', datas);
      // Hago el refresh de la tabla
      this.service.getSubTiposAnimales().subscribe(
        data => {
          this.source = data;
        },
      );
      event.confirm.resolve();
    });
  }


}
