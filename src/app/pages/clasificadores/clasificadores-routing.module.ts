import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClasificadoresComponent } from './clasificadores.component';
import { PaisesComponent } from './paises/paises.component';
import { ProvinciasComponent } from './provincias/provincias.component';
import { LocalidadesComponent } from './localidades/localidades.component';
import { DistritosComponent } from './distritos/distritos.component';
import { TiposDomicilioComponent } from './tipos-domicilio/tipos-domicilio.component';
import { TiposDocumentoComponent } from './tipos-documento/tipos-documento.component';
import { TiposPersonaComponent } from './tipos-persona/tipos-persona.component';
import { TiposAnimalesComponent } from './tipos-animales/tipos-animales.component';
import { SubTiposAnimalesComponent } from './subtipos-animales/subtipos-animales.component';
import { TiposCamarasComponent } from './tipos-camaras/tipos-camaras.component';
import { TiposDestinosComponent } from './tipos-destinos/tipos-destinos.component';
import { TiposProcedenciasComponent } from './tipos-procedencias/tipos-procedencias.component';
import { NumeroSenasaComponent } from './numeros-senasa/numeros-senasa.component';
// import { IconsComponent } from './icons/icons.component';
// import { TypographyComponent } from './typography/typography.component';
// import { SearchComponent } from './search-fields/search-fields.component';

const routes: Routes = [{
  path: '',
  component: ClasificadoresComponent,
  children: [ {
      path: 'paises',
      component: PaisesComponent,
      }, {
      path: 'provincias',
      component: ProvinciasComponent,
      }, {
      path: 'localidades',
      component: LocalidadesComponent,
      }, {
      path: 'distritos',
      component: DistritosComponent,
      }, {
      path: 'tipos-domicilio',
      component: TiposDomicilioComponent,
      }, {
      path: 'tipos-documento',
      component: TiposDocumentoComponent,
      }, {
      path: 'tipos-animales',
      component: TiposAnimalesComponent,
       }, {
       path: 'subtipos-animales',
       component: SubTiposAnimalesComponent,
        }, {
       path: 'tipos-camaras',
       component: TiposCamarasComponent,
        }, {
       path: 'tipos-destinos',
       component: TiposDestinosComponent,
        }, {
       path: 'tipos-persona',
       component: TiposPersonaComponent,
        },  {
       path: 'tipos-procedencias',
       component: TiposProcedenciasComponent,
        },  {
       path: 'numeros-senasa',
       component: NumeroSenasaComponent,
        },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClasificadoresRoutingModule { }
export const routedComponents = [
  ClasificadoresComponent,
  PaisesComponent,
  ProvinciasComponent,
  LocalidadesComponent,
  DistritosComponent,
  TiposDomicilioComponent,
  TiposDocumentoComponent,
  TiposAnimalesComponent,
  SubTiposAnimalesComponent,
  TiposCamarasComponent,
  TiposPersonaComponent,
  TiposDestinosComponent,
  TiposProcedenciasComponent,
  NumeroSenasaComponent,
];
