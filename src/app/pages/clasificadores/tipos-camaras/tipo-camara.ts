

export class TipoCamara {
        public id: number;
        public descripcion: string;
        public tempDesde: number;
        public tempHasta: number;
        public precio: number;
        public fechaBaja: any;
}
