import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import {TipoDomicilio} from './tipo-domicilio';

@Component({
  selector: 'ngx-tipos-domicilio',
  templateUrl: './tipos-domicilio.component.html',
  styleUrls: ['./tipos-domicilio.component.scss'],
})
export class TiposDomicilioComponent  {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Trabajar con tipo-domicilio
  public tipoDomicilio: TipoDomicilio = new TipoDomicilio();

  // Cargar tipo-domicilios en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Tipo de Domicilio',
        type: 'string',
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {
                // Obtengo los tipo de domicilios
                this.service.getTipoDomicilios().subscribe(
                  data => {

                      this.source = data;
                      console.log(this.source);
                    },
                  );

  }

  // Agregar provincia
  onCreateConfirm(event) {
    this.tipoDomicilio = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja': event.newData.fechaBaja,
                  'domicilioPersonaList': event.newData.domicilioPersonaList,
                  'domicilioSucursalList': event.newData.domicilioSucursalList,
                  };
      this.service.createTipoDomicilio(this.tipoDomicilio).subscribe(datas => {
        console.log('se guardo un tipo de domicilio', datas);
        // Hago el refresh de la tabla
        this.service.getTipoDomicilios().subscribe(
          data => {
            this.source = data;
          },
        );
        event.confirm.resolve();
      });
  }

  // Borrar tipo de domicilio
  onDeleteConfirm(event) {
    if (window.confirm('Estás seguro que quieres borrar el tipo de domicilio?')) {
      const idTipoDomicilio: number = event.data.id;
      this.service.deleteTipoDomicilio(idTipoDomicilio).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el tipo de domicilio');
      });
    } else {
      event.confirm.reject();
    }
  }

  // Editar tipo de domicilio
  onSaveConfirm(event) {
    this.tipoDomicilio = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja': event.newData.fechaBaja,
                  'domicilioPersonaList': event.newData.domicilioPersonaList,
                  'domicilioSucursalList': event.newData.domicilioSucursalList,
                  };
      this.service.updateTipoDomicilio(this.tipoDomicilio).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el tipo de domicilio', datas);
    });
  }


}
