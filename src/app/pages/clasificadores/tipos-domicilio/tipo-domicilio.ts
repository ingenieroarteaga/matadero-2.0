// import { DomicilioPersona } from "../../model/domicilio-persona";
// import { DomicilioSucursal } from "../../model/domicilio-sucursal";

export class TipoDomicilio {
    public id: number;
    public descripcion: string;
    public fechaBaja: Date;
    public domicilioPersonaList: any[];
    public domicilioSucursalList: any[];
}
