export class Usuario {
    public id: number;
    public username: string;
    public password: string;
    public nombre: string;
    public apellido: string;
    public mail: string;
    public image: any;
    public langKey: any;
    public creadoPor: any;
    public fechaAlta: Date;
    public modificadoPor: any;
    public activado: any;
    public fechaBaja: Date;
    public fechaModificacion: Date;
}

