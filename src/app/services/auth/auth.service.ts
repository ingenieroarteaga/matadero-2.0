import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global} from '../../global';
import { map } from 'rxjs/operators';


@Injectable()
export class AuthService {
  public url: string;


  // baseUrl: 'http://192.168.0.110:8080/email2sms/';
  constructor(private http: HttpClient) {

    this.url = Global.urlBase;
  }


  attemptAuth(username: string, password: string): Observable<any> {
    const credentials = { username: username, password: password };
    console.log('attempAuth ::');
    return this.http.post<any>(this.url + '/token/generate-token', credentials);
  }

}
